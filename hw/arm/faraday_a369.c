/*
 * Faraday A369 SoC emulation
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2+.
 */

#include "net/net.h"
#include "hw/boards.h"
#include "sysemu/sysemu.h"
#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/arm/faraday.h"
#include "hw/loader.h"
#include "hw/char/serial.h"
#include "exec/address-spaces.h"

/* AHB controller */

static void ahbc_init(void *opaque, uint32_t *regs)
{
    int i;

    /* AHB slave base & size configuration */
    regs[0] = 0x94050000;
    regs[1] = 0x96040000;
    regs[2] = 0x90f00000;
    regs[3] = 0x92050000; /* APB: base=0x92000000, size=32MB */
    regs[5] = 0xc0080000;
    regs[4] = 0x00080000; /* ROM: base=0x00000000, size=256MB */
    regs[6] = 0x10090000; /* RAM: base=0x10000000, size=512MB */
    for (i = 0; i < 15; ++i) {
        regs[7 + i] = 0x90000000 + (i << 20);
    }
    regs[22] = 0x40080000;
    regs[23] = 0x60080000;
    regs[24] = 0xa0000000; /* SRAM: base=0xA0000000, size=1MB */
}

static void ahbc_remap(void *opaque, bool on)
{
    FaradayA369State *s = FARADAY_A369(opaque);

    memory_region_del_subregion(s->as, s->rom);
    memory_region_del_subregion(s->as, s->ram);

    if (on) {
        memory_region_add_subregion(s->as, 0x40000000, s->rom);
        memory_region_add_subregion(s->as, 0x00000000, s->ram);
    } else {
        memory_region_add_subregion(s->as, 0x00000000, s->rom);
        memory_region_add_subregion(s->as, 0x10000000, s->ram);
    }
}

static void soc_reset(void *opaque)
{
    FaradayA369State *s = FARADAY_A369(opaque);

    if (s->cpu) {
        cpu_reset(CPU(s->cpu));
    }
}

FaradayA369State *faraday_a369_init(const char *cpu, uint64_t ram_size)
{
    FaradayA369State *s = g_new0(FaradayA369State, 1);
    qemu_irq *cpu_pic;
    qemu_irq ack, req;
    DeviceState *ds, *ahb;
    DeviceState *hdma[2];
    DeviceState *pdma[1];
    int i;

    /* Initialize QOM object */
    object_initialize(s, TYPE_FARADAY_A369);
    OBJECT(s)->free = g_free;
    object_property_add_child(container_get(qdev_get_machine(), "/unattached"),
        "soc", OBJECT(s), NULL);

    /* The 'bios_name' is supplied from the '-bios' command line option */
    s->romboot = !!bios_name;

    /* System Address Space */
    s->as = get_system_memory();

    /* CPU */
    s->cpu = cpu_arm_init(!cpu ? "fa626te" : cpu);
    if (!s->cpu) {
        fprintf(stderr, "faraday_a369: Unable to find CPU definition\n");
        abort();
    }

    /* Interrupt Controller */
    cpu_pic = arm_pic_init_cpu(s->cpu);
    ds = sysbus_create_varargs("ftintc020", 0x90100000,
                               cpu_pic[ARM_PIC_CPU_IRQ],
                               cpu_pic[ARM_PIC_CPU_FIQ], NULL);
    for (i = 0; i < ARRAY_SIZE(s->pic); ++i) {
        s->pic[i] = qdev_get_gpio_in(ds, i);
    }

    /* AHB bus controller */
    ahb = sysbus_create_simple("ftahbc020", 0x94000000, NULL);

    /* Embedded ROM Init */
    s->rom = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->rom, "faraday_a369.rom", 16 << 10);
    memory_region_set_readonly(s->rom, true);
    vmstate_register_ram_global(s->rom);

    /* Embedded RAM Init */
    s->sram = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->sram, "faraday_a369.sram", 16 << 10);
    vmstate_register_ram_global(s->sram);

    /* SDRAM(DDR) Init */
    s->ram = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->ram, "faraday_a369.ram", ram_size);
    vmstate_register_ram_global(s->ram);

    /*
     * ROM/RAM address binding
     */

    /* Embedded RAM is always binded to 0xA0000000 */
    memory_region_add_subregion(s->as, 0xA0000000, s->sram);

    if (s->romboot) {
        Error *errp = NULL;

        s->common.ahbc_init = ahbc_init;
        s->common.ahbc_remap = ahbc_remap;
        object_property_set_link(OBJECT(ahb), OBJECT(s), "soc", &errp);
        if (errp) {
            fprintf(stderr, "faraday.a369: Unable to set soc link\n");
            abort();
        }

        /* Bind SDRAM(DDR) to 0x10000000 */
        memory_region_add_subregion(s->as, 0x10000000, s->ram);

        /* Bind ROM to 0x00000000 */
        memory_region_add_subregion(s->as, 0x00000000, s->rom);

        /* Load the ROM image file */
        if (rom_add_file_fixed(bios_name, 0x00000000, 0)) {
            printf("faraday_a369: Unable to find [%s]\n", bios_name);
            abort();
        }
    } else {
        /* Bind SDRAM(DDR) to 0x00000000 */
        memory_region_add_subregion(s->as, 0x00000000, s->ram);

        /* Bind ROM to 0x40000000 */
        memory_region_add_subregion(s->as, 0x40000000, s->rom);
    }

    /* Serial (FTUART010 which is 16550A compatible) */
    if (serial_hds[0]) {
        serial_mm_init(s->as,
                       0x92b00000,
                       2,
                       s->pic[53],
                       18432000,
                       serial_hds[0],
                       DEVICE_LITTLE_ENDIAN);
    }
    if (serial_hds[1]) {
        serial_mm_init(s->as,
                       0x92c00000,
                       2,
                       s->pic[54],
                       18432000,
                       serial_hds[1],
                       DEVICE_LITTLE_ENDIAN);
    }

    /* ftscu010 */
    sysbus_create_simple("a369.scu", 0x92000000, NULL);

    /* ftpwmtmr010 */
    ds = qdev_create(NULL, "ftpwmtmr010");
    qdev_prop_set_uint32(ds, "freq", 66 * 1000000);
    qdev_init_nofail(ds);
    sysbus_mmio_map(SYS_BUS_DEVICE(ds), 0, 0x92300000);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 0, s->pic[8]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 1, s->pic[9]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 2, s->pic[10]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 3, s->pic[11]);

    /* ftwdt010 */
    qemu_register_reset(soc_reset, s);
    sysbus_create_simple("ftwdt010", 0x92200000, s->pic[46]);

    /* fti2c010 */
    ds = sysbus_create_simple("fti2c010", 0x92900000, s->pic[51]);
    s->i2c[0] = ds;
    ds = sysbus_create_simple("fti2c010", 0x92A00000, s->pic[52]);
    s->i2c[1] = ds;

    /* ftgmac100 */
    if (nb_nics > 0) {
        ftgmac100_init(&nd_table[0], 0x90c00000, s->pic[32]);
    }

    /* ftdmac020 */
    hdma[0] = sysbus_create_varargs("ftdmac020",
                                    0x90300000,
                                    s->pic[0],  /* ALL (NC in A369) */
                                    s->pic[15], /* TC */
                                    s->pic[16], /* ERR */
                                    NULL);
    hdma[1] = sysbus_create_varargs("ftdmac020",
                                    0x96100000,
                                    s->pic[0],  /* ALL (NC in A369) */
                                    s->pic[17], /* TC */
                                    s->pic[18], /* ERR */
                                    NULL);

    /* ftnandc021 */
    ds = sysbus_create_simple("ftnandc021", 0x90200000, s->pic[30]);
    s->nandc = ds;
    ack = qdev_get_gpio_in(ds, 0);
    req = qdev_get_gpio_in(hdma[0], 15);
    qdev_connect_gpio_out(hdma[0], 15, ack);
    qdev_connect_gpio_out(ds, 0, req);

    /* ftsdc010 */
    ds = sysbus_create_simple("ftsdc010", 0x90600000, s->pic[39]);
    ack = qdev_get_gpio_in(ds, 0);
    req = qdev_get_gpio_in(hdma[0], 13);
    qdev_connect_gpio_out(hdma[0], 13, ack);
    qdev_connect_gpio_out(ds, 0, req);

    /* fusbh200 */
    sysbus_create_simple("faraday-ehci-usb", 0x90800000, s->pic[36]);
    sysbus_create_simple("faraday-ehci-usb", 0x90900000, s->pic[37]);

    /* ftapbbrg020 */
    pdma[0] = sysbus_create_simple("ftapbbrg020", 0x90f00000, s->pic[14]);

    /* ftssp010 */
    ds = sysbus_create_simple("ftssp010", 0x92700000, s->pic[49]);
    s->spi[0] = ds;
    s->i2s[0] = ds;

    /* ftssp010 - DMA (Tx) */
    ack = qdev_get_gpio_in(ds, 0);
    req = qdev_get_gpio_in(pdma[0], 7);
    qdev_connect_gpio_out(pdma[0], 7, ack);
    qdev_connect_gpio_out(ds, 0, req);

    /* ftssp010 - DMA (Rx) */
    ack = qdev_get_gpio_in(ds, 1);
    req = qdev_get_gpio_in(pdma[0], 8);
    qdev_connect_gpio_out(pdma[0], 8, ack);
    qdev_connect_gpio_out(ds, 1, req);

    /* ftlcdc200 */
    sysbus_create_varargs("ftlcdc200",
                          0x94a00000,
                          NULL,       /* COMBINED (NC in A369) */
                          s->pic[25], /* VSTATUS */
                          s->pic[24], /* Frame Base Update */
                          s->pic[23], /* FIFO Under-Run */
                          s->pic[22], /* AHB Bus Error */
                          NULL);

    /* fttsc010 */
    sysbus_create_simple("fttsc010", 0x92400000, s->pic[19]);

    /* ftrtc011 */
    sysbus_create_varargs("ftrtc011",
                          0x92100000,
                          NULL,       /* Alarm: Level (NC in A369) */
                          s->pic[42], /* Alarm: Edge */
                          s->pic[43], /* Second: Edge */
                          s->pic[44], /* Minute: Edge */
                          s->pic[45], /* Hour: Edge */
                          NULL);

    return s;
}

static const VMStateDescription a369_vmst = {
    .name = TYPE_FARADAY_A369,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_END_OF_LIST(),
    }
};

static void a369_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc = TYPE_FARADAY_A369;
    dc->vmsd = &a369_vmst;
    dc->no_user = 1;
}

static const TypeInfo a369_info = {
    .name = TYPE_FARADAY_A369,
    .parent = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(FaradayA369State),
    .class_init = a369_class_init,
};

static void a369_register_types(void)
{
    type_register_static(&a369_info);
}

type_init(a369_register_types)
