/*
 * MOXA ART SoC based boards
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * Code based on faraday_boards.c by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2+
 */

#include "sysemu/sysemu.h"
#include "hw/sysbus.h"
#include "net/net.h"
#include "hw/arm/arm.h"
#include "hw/arm/faraday.h"
#include "hw/boards.h"
#include "hw/block/flash.h"

static void em1220_gpio(void *opaque, int line, int level)
{
    switch (line) {
    case 0:
        fprintf(stderr, "Debug LED 0 %s\n", level ? "on" : "off");
        break;
    case 1:
        fprintf(stderr, "Debug LED 1 %s\n", level ? "on" : "off");
        break;
    case 2:
        fprintf(stderr, "Debug LED 2 %s\n", level ? "on" : "off");
        break;
    case 3:
        fprintf(stderr, "Debug LED 3 %s\n", level ? "on" : "off");
        break;
    case 4:
        fprintf(stderr, "Ready LED %s\n", level ? "on" : "off");
        break;
    default:
        break;
    }
}

static void em1220_init(QEMUMachineInitArgs *args)
{
    MoxaArtState *s;
    struct arm_boot_info *abi;
    DriveInfo *dinfo;
    pflash_t *nor;
    qemu_irq *led;
    qemu_irq buzzer;
    DeviceState *ds;

    if (!args->ram_size) {
        args->ram_size = 64 << 20;
    }
    s = moxa_art_init(args->cpu_model, args->ram_size);

    dinfo = drive_get(IF_PFLASH, 0, 0);
    /* Intel */
    nor = pflash_cfi01_register(0x80000000, NULL, "em1220.nor", 16 << 20,
                                dinfo ? dinfo->bdrv : NULL, 128 << 10,
                                128, 2, 0x00, 0x89, 0x00, 0x18, 0);

    if (!bios_name) {
        /* Embedded ROM as pointer to NOR */
        memory_region_del_subregion(s->as, s->rom);
        memory_region_destroy(s->rom);
        memory_region_init_ram_ptr(s->rom, "moxa_art.rom", 256 << 10,
            memory_region_get_ram_ptr(pflash_cfi01_get_memory(nor)));
        memory_region_set_readonly(s->rom, true);
        memory_region_add_subregion(s->as, 0x00000000, s->rom);
    }

    /* Unknown (Float high) */
    qemu_irq_raise(qdev_get_gpio_in(s->gpio, 30));
    /* Reset Button (Float high) */
    qemu_irq_raise(qdev_get_gpio_in(s->gpio, 25));
    /* JP1 - Boot menu */
    qdev_get_gpio_in(s->gpio, 23);
    /* JP2 - Disable cache (Float high) */
    qemu_irq_raise(qdev_get_gpio_in(s->gpio, 22));

    led = qemu_allocate_irqs(em1220_gpio, s, 5);

    /* Debug LED */
    qdev_connect_gpio_out(s->gpio, 0, qemu_irq_invert(led[0]));
    qdev_connect_gpio_out(s->gpio, 1, qemu_irq_invert(led[1]));
    qdev_connect_gpio_out(s->gpio, 2, qemu_irq_invert(led[2]));
    qdev_connect_gpio_out(s->gpio, 3, qemu_irq_invert(led[3]));
    /* Ready LED */
    qdev_connect_gpio_out(s->gpio, 27, qemu_irq_invert(led[4]));

    /* Buzzer */
    ds = qdev_create(NULL, "buzzer");
    qdev_init_nofail(ds);

    buzzer = qdev_get_gpio_in(ds, 0);
    qdev_connect_gpio_out(s->gpio, 24, buzzer);

    /* System boot */
    if (args->kernel_filename) {
        abi = g_new0(struct arm_boot_info, 1);
        abi->ram_size = args->ram_size;
        abi->kernel_filename = args->kernel_filename;
        abi->kernel_cmdline = args->kernel_cmdline;
        abi->initrd_filename = args->initrd_filename;
        /* FIXME: Add MOXA ART machine id */
        abi->board_id = 0;
        arm_load_kernel(s->cpu, abi);
    }
}

static QEMUMachine moxa_machines[] = {
    {
        .name = "em1220",
        .desc = "MOXA EM-1220",
        .init = em1220_init,
        DEFAULT_MACHINE_OPTIONS,
    },
};

static void moxa_machine_init(void)
{
    int i;

    for (i = 0; i < ARRAY_SIZE(moxa_machines); ++i) {
        qemu_register_machine(&moxa_machines[i]);
    }
}

machine_init(moxa_machine_init);
