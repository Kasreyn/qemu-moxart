/*
 * Faraday A360 SoC emulation
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2+
 */

#include "net/net.h"
#include "hw/boards.h"
#include "sysemu/sysemu.h"
#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/arm/faraday.h"
#include "hw/loader.h"
#include "hw/char/serial.h"
#include "exec/address-spaces.h"

FaradayA360State *faraday_a360_init(const char *cpu, uint64_t ram_size)
{
    FaradayA360State *s = g_new0(FaradayA360State, 1);
    qemu_irq *cpu_pic;
    DeviceState *ds;
    int i;

    /* Initialize QOM object */
    object_initialize(s, TYPE_FARADAY_A360);
    OBJECT(s)->free = g_free;
    object_property_add_child(container_get(qdev_get_machine(), "/unattached"),
        "soc", OBJECT(s), NULL);

    /* System Address Space */
    s->as = get_system_memory();

    /* CPU */
    s->cpu = cpu_arm_init(!cpu ? "fa626te" : cpu);
    if (!s->cpu) {
        fprintf(stderr, "faraday_a360: Unable to find CPU definition\n");
        abort();
    }

    /* Interrupt Controller */
    cpu_pic = arm_pic_init_cpu(s->cpu);
    ds = sysbus_create_varargs("ftintc020", 0x98800000,
                               cpu_pic[ARM_PIC_CPU_IRQ],
                               cpu_pic[ARM_PIC_CPU_FIQ], NULL);
    for (i = 0; i < ARRAY_SIZE(s->pic); ++i) {
        s->pic[i] = qdev_get_gpio_in(ds, i);
    }

    /* SDRAM(DDR) Init */
    s->ram = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->ram, "faraday_a360.ram", ram_size);
    vmstate_register_ram_global(s->ram);
    memory_region_add_subregion(s->as, 0x00000000, s->ram);

    /* Serial (FTUART010 which is 16550A compatible) */
    if (serial_hds[0]) {
        serial_mm_init(s->as,
                       0x98200000,
                       2,
                       s->pic[10],
                       18432000,
                       serial_hds[0],
                       DEVICE_LITTLE_ENDIAN);
    }
    if (serial_hds[1]) {
        serial_mm_init(s->as,
                       0x98300000,
                       2,
                       s->pic[11],
                       18432000,
                       serial_hds[1],
                       DEVICE_LITTLE_ENDIAN);
    }

    /* pmu */
    sysbus_create_simple("a360.pmu", 0x98100000, NULL);

    /* scu */
    sysbus_create_simple("a360.scu", 0x99900000, NULL);

    /* fttmr010 */
    ds = qdev_create(NULL, "fttmr010");
    qdev_prop_set_uint32(ds, "freq", 67500000);
    qdev_init_nofail(ds);
    sysbus_mmio_map(SYS_BUS_DEVICE(ds), 0, 0x98400000);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 0, s->pic[19]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 1, s->pic[14]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 2, s->pic[15]);

    /* ftwdt010 */
    ds = qdev_create(NULL, "ftwdt010");
    qdev_prop_set_uint32(ds, "freq", 67500000);
    qdev_init_nofail(ds);
    sysbus_mmio_map(SYS_BUS_DEVICE(ds), 0, 0x98500000);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 0, s->pic[16]);

    /* ftmac110 */
    if (nb_nics > 0) {
        ftmac110_init(&nd_table[0], 0x90900000, s->pic[25]);
    }

    return s;
}

static const VMStateDescription a360_vmst = {
    .name = TYPE_FARADAY_A360,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_END_OF_LIST(),
    }
};

static void a360_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc = TYPE_FARADAY_A360;
    dc->vmsd = &a360_vmst;
    dc->no_user = 1;
}

static const TypeInfo a360_info = {
    .name = TYPE_FARADAY_A360,
    .parent = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(FaradayA360State),
    .class_init = a360_class_init,
};

static void a360_register_types(void)
{
    type_register_static(&a360_info);
}

type_init(a360_register_types)
