/*
 * Faraday SoC based boards emulation
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2+
 */

#include "sysemu/sysemu.h"
#include "hw/sysbus.h"
#include "net/net.h"
#include "hw/arm/arm.h"
#include "hw/arm/faraday.h"
#include "hw/boards.h"
#include "hw/block/flash.h"
#include "hw/i2c/i2c.h"
#include "hw/ssi.h"

static void a360evb_init(QEMUMachineInitArgs *args)
{
    FaradayA360State *s;
    struct arm_boot_info *abi;

    if (!args->ram_size) {
        args->ram_size = 256 << 20;
    }
    s = faraday_a360_init(args->cpu_model, args->ram_size);

    abi = g_new0(struct arm_boot_info, 1);
    abi->ram_size = args->ram_size;
    abi->kernel_filename = args->kernel_filename;
    abi->kernel_cmdline = args->kernel_cmdline;
    abi->initrd_filename = args->initrd_filename;
    abi->board_id = 0x3360;
    arm_load_kernel(s->cpu, abi);
}

static void a369evb_init(QEMUMachineInitArgs *args)
{
    DeviceState *ds;
    FaradayA369State *s;
    DriveInfo *dinfo;
    Error *errp = NULL;
    i2c_bus *i2c;
    SSIBus *ssi;

    if (!args->ram_size) {
        args->ram_size = 512 << 20;
    }
    s = faraday_a369_init(args->cpu_model, args->ram_size);

    /* Attach the nand flash to ftnandc021 */
    dinfo = drive_get_next(IF_MTD);
    ds = nand_init(dinfo ? dinfo->bdrv : NULL, NAND_MFR_SAMSUNG, 0xda);
    object_property_set_link(OBJECT(s->nandc), OBJECT(ds), "flash", &errp);
    if (errp) {
        fprintf(stderr, "a369evb: Unable to set nand flash link\n");
        abort();
    }

    /* Attach the spi flash to ftssp010.0 */
    ssi = (SSIBus *)qdev_get_child_bus(s->spi[0], "spi");
    ds = ssi_create_slave(ssi, "w25q64");
    sysbus_connect_irq(SYS_BUS_DEVICE(s->spi[0]), 1, qdev_get_gpio_in(ds, 0));

    /* Attach the wm8731 to fti2c010.0 & ftssp010.0 */
    i2c = (i2c_bus *)qdev_get_child_bus(s->i2c[0], "i2c");
    ds = i2c_create_slave(i2c, "wm8731", 0x1B);
    object_property_set_link(OBJECT(s->i2s[0]), OBJECT(ds),
        "codec", &errp);
    if (errp) {
        fprintf(stderr, "a369evb: Unable to set codec link\n");
        abort();
    }
    wm8731_data_req_set(ds, ftssp010_i2s_data_req, s->i2s[0]);

    /* System boot */
    if (args->kernel_filename) {
        struct arm_boot_info *abi;

        abi = g_new0(struct arm_boot_info, 1);
        abi->ram_size = args->ram_size;
        abi->kernel_filename = args->kernel_filename;
        abi->kernel_cmdline = args->kernel_cmdline;
        abi->initrd_filename = args->initrd_filename;
        abi->board_id = 0x3369;
        arm_load_kernel(s->cpu, abi);
    } else if (!bios_name) {
        fprintf(stderr,
            "a369evb: please specify boot image by either -kernel or -bios\n");
        abort();
    }
}

static QEMUMachine faraday_machines[] = {
    {
        .name = "a360",
        .desc = "Faraday A360 evalution board (FIE3360)",
        .init = a360evb_init,
        DEFAULT_MACHINE_OPTIONS,
    },
    {
        .name = "a360evb",
        .desc = "Faraday A360 evalution board (FIE3360)",
        .init = a360evb_init,
        DEFAULT_MACHINE_OPTIONS,
    },
    {
        .name = "fie3360",
        .desc = "Faraday A360 evalution board (FIE3360)",
        .init = a360evb_init,
        DEFAULT_MACHINE_OPTIONS,
    },
    {
        .name = "a369",
        .desc = "Faraday A369 evalution board (FIE3369)",
        .init = a369evb_init,
        DEFAULT_MACHINE_OPTIONS,
    },
    {
        .name = "a369evb",
        .desc = "Faraday A369 evalution board (FIE3369)",
        .init = a369evb_init,
        DEFAULT_MACHINE_OPTIONS,
    },
    {
        .name = "fie3369",
        .desc = "Faraday A369 evalution board (FIE3369)",
        .init = a369evb_init,
        DEFAULT_MACHINE_OPTIONS,
    },
};

static void faraday_machine_init(void)
{
    int i;

    for (i = 0; i < ARRAY_SIZE(faraday_machines); ++i) {
        qemu_register_machine(&faraday_machines[i]);
    }
}

machine_init(faraday_machine_init);
