/*
 * MOXA ART SoC emulation
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * Code based on faraday_a369.c by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2+
 */

#include "net/net.h"
#include "hw/boards.h"
#include "sysemu/sysemu.h"
#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/arm/faraday.h"
#include "hw/loader.h"
#include "hw/char/serial.h"
#include "exec/address-spaces.h"

/* AHB controller */
static void ahb_reset(void *opaque, uint32_t *regs)
{
    /* AHB slave base & size configuration */
    regs[0] = 0x90100000; /* AHBC */
    regs[1] = 0x90500000; /* APB to AHB bridge */
    regs[2] = 0x98700000; /* GPIOC */
    regs[3] = 0x90200000; /* SMC */
    regs[4] = 0x00080000; /* ROM: base=0x00000000, size=256MB */
    regs[5] = 0x90300000; /* SDRAMC */
    regs[6] = 0x100b0000; /* RAM: base=0x10000000, size=2GB */
    regs[7] = 0x90400000; /* DMAC */
/*
    regs[8] = 0x00000000;
    regs[9] = 0x00000000;
    regs[10] = 0x00000000;
    regs[11] = 0x00000000;
*/
    regs[12] = 0x90900000; /* MAC */
    regs[13] = 0x90a00000; /* EHCI */
    regs[14] = 0x90b00000;
    regs[15] = 0x90c00000; /* PCI to AHB bridge */
/*
    regs[16] = 0x00000000;
    regs[17] = 0x00000000;
*/
    regs[18] = 0x90f00000; /* AES/DES */
    regs[19] = 0x92000000; /* MAC */
    regs[20] = 0x92300000; /* EBI */
    regs[21] = 0xa0080000; /* SRAM: base=0xa0000000, size=256MB */
/*
    regs[22] = 0x00000000;
*/
    regs[23] = 0x92100000;
    regs[24] = 0x92200000;
    regs[25] = 0x92400000;
    regs[26] = 0x92500000;
/*
    regs[27] = 0x00000000;
    regs[28] = 0x00000000;
    regs[29] = 0x00000000;
    regs[30] = 0x00000000;
*/
}

static void soc_reset(void *opaque)
{
    MoxaArtState *s = MOXA_ART(opaque);

    if (s->cpu) {
        cpu_reset(CPU(s->cpu));
    }
}

MoxaArtState *moxa_art_init(const char *cpu, uint64_t ram_size)
{
    MoxaArtState *s = g_new0(MoxaArtState, 1);
    qemu_irq *cpu_pic;
    qemu_irq ack, req;
    DeviceState *ds, *ahb, *pdma;
    Error *errp = NULL;
    int i;

    /* Initialize QOM object */
    object_initialize(s, TYPE_MOXA_ART);
    OBJECT(s)->free = g_free;
    object_property_add_child(container_get(qdev_get_machine(), "/unattached"),
        "soc", OBJECT(s), NULL);

    /* System Address Space */
    s->as = get_system_memory();

    /* CPU */
    s->cpu = cpu_arm_init(!cpu ? "fa526" : cpu);
    if (!s->cpu) {
        fprintf(stderr, "moxa_art: Unable to find CPU definition\n");
        abort();
    }

    /* Interrupt Controller */
    cpu_pic = arm_pic_init_cpu(s->cpu);
    ds = sysbus_create_varargs("ftintc020", 0x98800000,
                               cpu_pic[ARM_PIC_CPU_IRQ],
                               cpu_pic[ARM_PIC_CPU_FIQ], NULL);
    for (i = 0; i < ARRAY_SIZE(s->pic); ++i) {
        s->pic[i] = qdev_get_gpio_in(ds, i);
    }

    /* AHB bus controller */
    ahb = sysbus_create_simple("ftahbc020s", 0x90100000, NULL);
    /* FIXME: adapt to qemu_register_reset? */
    s->common.ahbc_init = ahb_reset;

    object_property_set_link(OBJECT(ahb), OBJECT(s), "soc", &errp);
    if (errp) {
        fprintf(stderr, "moxa_art: Unable to set soc link\n");
        abort();
    }

    /* Embedded ROM Init */
    s->rom = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->rom, "moxa_art.rom", 256 << 10);
    vmstate_register_ram_global(s->rom);
    /* Bind ROM to 0x00000000 */
    memory_region_add_subregion(s->as, 0x00000000, s->rom);

    /* Embedded RAM Init */
    s->sram = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->sram, "moxa_art.sram", 256 << 10);
    vmstate_register_ram_global(s->sram);
    /* Embedded RAM is always binded to 0xa0000000 */
    memory_region_add_subregion(s->as, 0xa0000000, s->sram);

    /* SDRAM(DDR) Init */
    s->ram = g_new0(MemoryRegion, 1);
    memory_region_init_ram(s->ram, "moxa_art.ram", ram_size);
    vmstate_register_ram_global(s->ram);
    /* Bind SDRAM(DDR) to 0x10000000 */
    memory_region_add_subregion(s->as, 0x10000000, s->ram);

    if (!!bios_name) {
        /* Load the ROM image file */
        if (rom_add_file_fixed(bios_name, 0x00000000, 0)) {
            printf("moxa_art: Unable to find [%s]\n", bios_name);
            abort();
        }
    }

    /* Serial (FTUART010 which is 16550A compatible) */
    if (serial_hds[0]) {
        serial_mm_init(s->as, 0x98200000, 2, s->pic[31], 14745600,
                       serial_hds[0], DEVICE_LITTLE_ENDIAN);
    }

    if (serial_hds[1]) {
        serial_mm_init(s->as, 0x98200020, 2, s->pic[31], 14745600,
                       serial_hds[1], DEVICE_LITTLE_ENDIAN);
    }

    /* Power management unit */
    sysbus_create_simple("moxa_art.pmu", 0x98100000, NULL);

    /* SDRAM controller */
    sysbus_create_simple("moxa_art.sdramc", 0x90300000, NULL);

    /* Timer */
    ds = qdev_create(NULL, "fttmr010");
    qdev_prop_set_uint32(ds, "freq", 48000000);
    qdev_init_nofail(ds);
    sysbus_mmio_map(SYS_BUS_DEVICE(ds), 0, 0x98400000);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 0, s->pic[19]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 1, s->pic[14]);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 2, s->pic[15]);

    /* Watchdog timer */
    qemu_register_reset(soc_reset, s);
    ds = qdev_create(NULL, "ftwdt010");
    qdev_prop_set_uint32(ds, "freq", 48000000);
    qdev_init_nofail(ds);
    sysbus_mmio_map(SYS_BUS_DEVICE(ds), 0, 0x98500000);
    sysbus_connect_irq(SYS_BUS_DEVICE(ds), 0, s->pic[16]);

    /* GPIO controller */
    s->gpio = sysbus_create_simple("ftgpio010", 0x98700000, s->pic[13]);

    /* 10/100Mbps ethernet controller */
    if (nb_nics > 0) {
        if (nd_table[0].used) {
            ftmac100_init(&nd_table[0], 0x92000000, s->pic[27]);
        }

        if (nd_table[1].used) {
            ftmac100_init(&nd_table[1], 0x90900000, s->pic[25]);
        }
    }

    /* DMA controller */
    sysbus_create_varargs("ftdmac020",
                          0x90400000,
                          s->pic[21], /* ALL */
                          s->pic[22], /* TC */
                          s->pic[23], /* ERR */
                          NULL);

    /* fusbh200 */
    sysbus_create_simple("faraday-ehci-usb", 0x90a00000, s->pic[28]);

    /* APB to AHB bridge */
    pdma = sysbus_create_simple("ftapbbrg020", 0x90500000, s->pic[24]);

    /* SD controller */
    ds = sysbus_create_simple("ftsdc010", 0x98e00000, s->pic[5]);

    ack = qdev_get_gpio_in(ds, 0);
    req = qdev_get_gpio_in(pdma, 5);
    qdev_connect_gpio_out(pdma, 5, ack);
    qdev_connect_gpio_out(ds, 0, req);

    return s;
}

static const VMStateDescription vmstate_moxa_art = {
    .name = TYPE_MOXA_ART,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_END_OF_LIST(),
    }
};

static void moxa_art_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc = TYPE_MOXA_ART;
    dc->vmsd = &vmstate_moxa_art;
    dc->no_user = 1;
}

static const TypeInfo moxa_art_info = {
    .name = TYPE_MOXA_ART,
    .parent = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(MoxaArtState),
    .class_init = moxa_art_class_init,
};

static void moxa_art_register_types(void)
{
    type_register_static(&moxa_art_info);
}

type_init(moxa_art_register_types)
