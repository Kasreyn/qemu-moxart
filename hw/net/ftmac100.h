/*
 * Faraday FTMAC100 10/100Mbps Ethernet Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#ifndef HW_NET_FTMAC100_H
#define HW_NET_FTMAC100_H

typedef struct Ftmac100Regs {
    uint32_t isr;    /* 0x00: Interrups Status Register */
    uint32_t ier;    /* 0x04: Interrupt Enable Register */
    uint32_t mac[2]; /* 0x08: MAC Address */
    uint32_t mht[2]; /* 0x10: Multicast Hash Table Register */
    uint32_t txpd;   /* 0x18: Tx Poll Demand Register */
    uint32_t rxpd;   /* 0x1c: Rx Poll Demand Register */
    uint32_t txbar;  /* 0x20: Tx Ring Base Address Register */
    uint32_t rxbar;  /* 0x24: Rx Ring Base Address Register */
    uint32_t itc;    /* 0x28: Interrupt Timer Control Register */
    uint32_t aptc;   /* 0x2C: Automatic Polling Timer Control Register */
    uint32_t dblac;  /* 0x30: DMA Burst Length&Arbitration Control */
    uint32_t revr;   /* 0x34: Revision Register */
    uint32_t fear;   /* 0x38: Feature Register */
    uint32_t rsvd0[19];
    uint32_t maccr;  /* 0x88: MAC Control Register */
    uint32_t macsr;  /* 0x8C: MAC Status Register */
    uint32_t phycr;  /* 0x90: PHY Control Register */
    uint32_t phydr;  /* 0x94: PHY Data Register */
    uint32_t fcr;    /* 0x98: Flow Control Register */
    uint32_t bpr;    /* 0x9C: Back Pressure Register */

    uint32_t rsvd1[16];/* 0xA0 ~ 0xDF */
    uint32_t rxerr[3];
    uint32_t rxbcst;
    uint32_t rxmcst;
    uint32_t rxpkt;
    uint32_t txpkt;
} Ftmac100Regs;

#define FTMAC100_REG_SIZE       sizeof(Ftmac100Regs)

/*
 * Interrupt status register
 */
#define FTMAC100_ISR_PHYST      (1 << 9) /* phy status change */
#define FTMAC100_ISR_BUS        (1 << 8) /* bus error */
#define FTMAC100_ISR_RXLOST     (1 << 7) /* rx lost */
#define FTMAC100_ISR_RXFIFO     (1 << 6) /* rx to fifo */
#define FTMAC100_ISR_TXLOST     (1 << 5) /* tx lost */
#define FTMAC100_ISR_TX         (1 << 4) /* tx to ethernet */
#define FTMAC100_ISR_NTB        (1 << 3) /* out of tx buffer */
#define FTMAC100_ISR_TXFIFO     (1 << 2) /* tx to fifo */
#define FTMAC100_ISR_NRB        (1 << 1) /* out of rx buffer */
#define FTMAC100_ISR_RX         (1 << 0) /* rx to buffer */

/*
 * Feature register
 */
#define FTMAC100_FEAR_HD        (1 << 2) /* support half-duplex */
#define FTMAC100_FEAR_FD        (1 << 1) /* support full-duplex */
#define FTMAC100_FEAR_WOL       (1 << 0) /* support Wake-On-LAN */

/*
 * MAC control register
 */
#define FTMAC100_MACCR_100M     (1 << 18) /* 100Mbps mode */
#define FTMAC100_MACCR_RXBCST   (1 << 17) /* rx broadcast packet */
#define FTMAC100_MACCR_RXMCST   (1 << 16) /* rx multicast packet */
#define FTMAC100_MACCR_FD       (1 << 15) /* full duplex */
#define FTMAC100_MACCR_CRCAPD   (1 << 14) /* tx crc append */
#define FTMAC100_MACCR_RXALL    (1 << 12) /* rx all packets */
#define FTMAC100_MACCR_RXFTL    (1 << 11) /* rx packet even it's > 1518 byte */
#define FTMAC100_MACCR_RXRUNT   (1 << 10) /* rx packet even it's < 64 byte */
#define FTMAC100_MACCR_RXMH     (1 << 9)  /* rx multicast hash table */
#define FTMAC100_MACCR_RX       (1 << 8)  /* rx enable */
#define FTMAC100_MACCR_RXINHDTX (1 << 6)  /* rx in half duplex tx */
#define FTMAC100_MACCR_TX       (1 << 5)  /* tx enable */
#define FTMAC100_MACCR_CRCDIS   (1 << 4)  /* discard crc error */
#define FTMAC100_MACCR_LOOP     (1 << 3)  /* loop-back */
#define FTMAC100_MACCR_RESET    (1 << 2)  /* reset */
#define FTMAC100_MACCR_RXDMA    (1 << 1)  /* rx dma enable */
#define FTMAC100_MACCR_TXDMA    (1 << 0)  /* tx dma enable */

/*
 * PHY control register
 */
#define FTMAC100_PHYCR_READ     (1 << 26)
#define FTMAC100_PHYCR_WRITE    (1 << 27)
#define FTMAC100_PHYCR_REG(x)   extract32((uint32_t)(x), 21, 5)
#define FTMAC100_PHYCR_DEV(x)   extract32((uint32_t)(x), 16, 5)

typedef struct Ftmac100TxDesc {
    uint32_t txdes0;
    uint32_t txdes1;
    uint32_t txdes2; /* RXBUF_BADR */
    uint32_t txdes3; /* not used by HW */
} Ftmac100TxDesc;

#define	FTMAC100_TXDES0_TXPKT_LATECOL   (1 << 0)
#define	FTMAC100_TXDES0_TXPKT_EXSCOL    (1 << 1)
#define	FTMAC100_TXDES0_TXDMA_OWN       (1 << 31)

#define	FTMAC100_TXDES1_TXBUF_SIZE(x)	((x) & 0x7ff)
#define	FTMAC100_TXDES1_LTS             (1 << 27)
#define	FTMAC100_TXDES1_FTS             (1 << 28)
#define	FTMAC100_TXDES1_TX2FIC          (1 << 29)
#define	FTMAC100_TXDES1_TXIC            (1 << 30)
#define	FTMAC100_TXDES1_EDOTR           (1 << 31)

typedef struct Ftmac100RxDesc {
    uint32_t rxdes0;
    uint32_t rxdes1;
    uint32_t rxdes2; /* RXBUF_BADR */
    uint32_t rxdes3; /* not used by HW */
} Ftmac100RxDesc;

#define	FTMAC100_RXDES0_RFL         0x7ff
#define	FTMAC100_RXDES0_MULTICAST   (1 << 16)
#define	FTMAC100_RXDES0_BROADCAST   (1 << 17)
#define	FTMAC100_RXDES0_RX_ERR      (1 << 18)
#define	FTMAC100_RXDES0_CRC_ERR     (1 << 19)
#define	FTMAC100_RXDES0_FTL         (1 << 20)
#define	FTMAC100_RXDES0_RUNT        (1 << 21)
#define	FTMAC100_RXDES0_RX_ODD_NB   (1 << 22)
#define	FTMAC100_RXDES0_LRS         (1 << 28)
#define	FTMAC100_RXDES0_FRS         (1 << 29)
#define	FTMAC100_RXDES0_RXDMA_OWN   (1 << 31)

#define	FTMAC100_RXDES1_RXBUF_SIZE(x)   ((x) & 0x7ff)
#define	FTMAC100_RXDES1_EDORR           (1 << 31)

/*
 * descriptor structure
 */
typedef struct Ftmac100Desc { 
    uint64_t ctrl;
    uint32_t buff;
    uint32_t rsvd;
} Ftmac100Desc;

#define FTMAC100_RXD_END        ((uint64_t)1 << 63)
#define FTMAC100_RXD_BUFSZ(x)   (((uint64_t)(x) >> 32) & 0x7ff)

#define FTMAC100_RXD_OWNER      ((uint64_t)1 << 31) /* owner: 1=HW, 0=SW */
#define FTMAC100_RXD_FRS        ((uint64_t)1 << 29) /* first pkt desc */
#define FTMAC100_RXD_LRS        ((uint64_t)1 << 28) /* last pkt desc */
#define FTMAC100_RXD_ODDNB      ((uint64_t)1 << 22) /* odd nibble */
#define FTMAC100_RXD_RUNT       ((uint64_t)1 << 21) /* runt pkt */
#define FTMAC100_RXD_FTL        ((uint64_t)1 << 20) /* frame too long */
#define FTMAC100_RXD_CRC        ((uint64_t)1 << 19) /* pkt crc error */
#define FTMAC100_RXD_ERR        ((uint64_t)1 << 18) /* bus error */
#define FTMAC100_RXD_BCST       ((uint64_t)1 << 17) /* Bcst pkt */
#define FTMAC100_RXD_MCST       ((uint64_t)1 << 16) /* Mcst pkt */
#define FTMAC100_RXD_LEN(x)     ((uint64_t)((x) & 0x7ff))

#define FTMAC100_RXD_CLRMASK    ((uint64_t)0xffffffff00000000)

#define FTMAC100_TXD_END        ((uint64_t)1 << 63) /* end of ring */
#define FTMAC100_TXD_TXIC       ((uint64_t)1 << 62) /* tx done interrupt */
#define FTMAC100_TXD_TX2FIC     ((uint64_t)1 << 61) /* tx fifo interrupt */
#define FTMAC100_TXD_FTS        ((uint64_t)1 << 60) /* first pkt desc */
#define FTMAC100_TXD_LTS        ((uint64_t)1 << 59) /* last pkt desc */
#define FTMAC100_TXD_LEN(x)     (((uint64_t)(x) >> 32) & 0x7ff)

#define FTMAC100_TXD_OWNER      ((uint64_t)1 << 31) /* owner: 1=HW, 0=SW */
#define FTMAC100_TXD_COL        ((uint64_t)3)       /* collision */

#define FTMAC100_TXD_CLRMASK    (FTMAC100_TXD_END)

#define BMCR_LOOPBACK 0x4000

#endif  /* FTMAC100_H */
