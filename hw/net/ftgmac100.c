/*
 * Faraday FTGMAC100 Gigabit Ethernet Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"
#include "sysemu/sysemu.h"
#include "sysemu/dma.h"
#include "net/net.h"
#include "ftgmac100.h"

#define FTGMAC100_REVISION  FARADAY_REVISION(0, 6, 0)
#define FTGMAC100_FRMAX     9216        /* Max. frame size */

#ifndef DEBUG
#define DEBUG   0
#endif

#define DPRINTF(fmt, ...) \
    do { \
        if (DEBUG) { \
            fprintf(stderr, "[qemu]" fmt , ## __VA_ARGS__); \
        } \
    } while (0)

#define TYPE_FTGMAC100  "ftgmac100"

typedef struct Ftgmac100State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    QEMUBH *bh;
    qemu_irq irq;
    NICState *nic;
    NICConf conf;
    DMAContext *dma;
    QEMUTimer *qtimer;

    uint32_t txi;
    uint32_t rxi;

    struct {
        uint8_t dat[FTGMAC100_FRMAX];
        uint32_t len;
    } buf;

    struct {
        uint32_t tx_pkts;
        uint32_t rx_pkts;
        uint32_t rx_bcst;
        uint32_t rx_mcst;
    } stats;

    /* hw registers */
    uint32_t regs[FTGMAC100_REG_SIZE >> 2];
} Ftgmac100State;

#define FTGMAC100(obj) \
    OBJECT_CHECK(Ftgmac100State, obj, TYPE_FTGMAC100)

#define FRM_PROTO(frm) \
    ((((uint8_t *)(frm))[12] << 8) | ((uint8_t *)(frm))[13])

#define FRM_BCST(frm) \
    ((((uint8_t *)(frm))[0] == 0xff) \
    && (((uint8_t *)(frm))[1] == 0xff) \
    && (((uint8_t *)(frm))[2] == 0xff) \
    && (((uint8_t *)(frm))[3] == 0xff) \
    && (((uint8_t *)(frm))[4] == 0xff) \
    && (((uint8_t *)(frm))[5] == 0xff))

#define FRM_MCST(frm) \
    ((((uint8_t *)(frm))[0] == 0x01) \
    && (((uint8_t *)(frm))[1] == 0x00) \
    && (((uint8_t *)(frm))[2] == 0x5e) \
    && (((uint8_t *)(frm))[3] <= 0x7f))

/* Emulating a Davicom PHY with 100Mbps link state */
static const uint16_t miiphy_regs[] = {
    0x3100, 0x786d, 0x0181, 0xb8a0, 0x01e1, 0x45e1,
};

static int ftgmac100_mcast_hash(Ftgmac100Regs *regs, const uint8_t *data)
{
#define CRCPOLY_BE  0x04c11db7
    int i, len;
    uint32_t crc = 0xFFFFFFFF;

    len = (regs->maccr & FTGMAC100_MACCR_GIGA) ? 5 : 6;

    while (len--) {
        uint32_t c = *(data++);

        for (i = 0; i < 8; ++i) {
            crc = (crc << 1) ^ ((((crc >> 31) ^ c) & 0x01) ? CRCPOLY_BE : 0);
            c >>= 1;
        }
    }
    crc = ~crc;

    /* Reverse CRC32 and return MSB 6 bits only */
    return bitrev8(crc >> 24) >> 2;
}

static void ftgmac100_get_descriptor(Ftgmac100State *s, Ftgmac100Desc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftgmac100: descriptor is not 16-byte aligned!\n");
    }

    dma_memory_read(s->dma, addr, d, sizeof(Ftgmac100Desc));

    d->ctrl = le64_to_cpu(d->ctrl);
    d->rsvd = le32_to_cpu(d->rsvd);
    d->buff = le32_to_cpu(d->buff);
}

static void ftgmac100_put_descriptor(Ftgmac100State *s, Ftgmac100Desc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftgmac100: descriptor is not 16-byte aligned!\n");
    }

    d->ctrl = cpu_to_le64(d->ctrl);
    d->rsvd = cpu_to_le32(d->rsvd);
    d->buff = cpu_to_le32(d->buff);

    dma_memory_write(s->dma, addr, d, sizeof(Ftgmac100Desc));
}

static void ftgmac100_update_irq(Ftgmac100State *s)
{
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;

    qemu_set_irq(s->irq, !!(regs->isr & regs->imr));
}

static int ftgmac100_can_receive(NetClientState *nc)
{
    int ret = 0;
    Ftgmac100Desc rxd;
    Ftgmac100State *s = qemu_get_nic_opaque(nc);
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;
    hwaddr addr = regs->rxbar + s->rxi * sizeof(Ftgmac100Desc);
    uint32_t maccr = regs->maccr;

    if ((maccr & FTGMAC100_MACCR_RX) && (maccr & FTGMAC100_MACCR_RXDMA)) {
        ftgmac100_get_descriptor(s, (void *)&rxd, addr);
        ret = !(rxd.ctrl & FTGMAC100_RXD_OWNER_SW);
        if (!ret) {
            qemu_mod_timer(s->qtimer, qemu_get_clock_ms(vm_clock) + 10);
        }
    }

    return ret;
}

static ssize_t ftgmac100_receive(NetClientState *nc, const uint8_t *buf,
        size_t size)
{
    Ftgmac100State *s = qemu_get_nic_opaque(nc);
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;
    uint32_t maccr = regs->maccr;
    uint16_t proto = FRM_PROTO(buf);
    bool bcst, mcst, ftl;
    const uint8_t *ptr;

    s->stats.rx_pkts++;

    /* Check if it's a long frame. (Frame checksum is excluded) */
    ftl = false;
    if (proto == 0x8100) { /* 802.1Q VLAN */
        ftl = !!(size > 1518);
    } else {
        ftl = !!(size > 1514);
    }
    if (ftl) {
        DPRINTF("ftgmac100: frame too long, drop it\n");
        return -1;
    }

    /* Check if it's a broadcast frame */
    if (FRM_BCST(buf)) {
        bcst = true;
        s->stats.rx_bcst++;
        if (!(maccr & FTGMAC100_MACCR_RXALL)
            && !(maccr & FTGMAC100_MACCR_RXBCST)) {
            DPRINTF("ftgmac100: bcst filtered\n");
            return -1;
        }
    } else {
        bcst = false;
    }

    /* Check if it's a multicast frame */
    if (FRM_MCST(buf)) {
        mcst = true;
        s->stats.rx_mcst++;
        if (!(maccr & FTGMAC100_MACCR_RXALL)
            && !(maccr & FTGMAC100_MACCR_RXMCST)) {
            int hash;

            if (!(maccr & FTGMAC100_MACCR_RXMH)) {
                DPRINTF("ftgmac100: mcst filtered\n");
                return -1;
            }
            hash = ftgmac100_mcast_hash(regs, buf);
            if (!(regs->mht[hash >> 5] & BIT(hash & 0x1f))) {
                DPRINTF("ftgmac100: mcst filtered\n");
                return -1;
            }
        }
    } else {
        mcst = false;
    }

    /* Check if its DA matches NIC mac address */
    if (!(maccr & FTGMAC100_MACCR_RXALL) && !bcst && !mcst) {
        if (memcmp(s->conf.macaddr.a, buf, 6)) {
            DPRINTF("ftgmac100: DA!=NIC filtered\n");
            return -1;
        }
    }

    ptr = buf;
    while (size > 0) {
        Ftgmac100Desc rxd;
        hwaddr addr;
        size_t len;

        addr = regs->rxbar + s->rxi * sizeof(Ftgmac100Desc);
        ftgmac100_get_descriptor(s, &rxd, addr);
        if (rxd.ctrl & FTGMAC100_RXD_OWNER_SW) {
            regs->isr |= FTGMAC100_ISR_NRB;
            DPRINTF("ftgmac100: out of rxd!?\n");
            return -1;
        }

        len = MIN(size, regs->rbsr);

        /* update rxd.ctrl */
        rxd.ctrl &= FTGMAC100_RXD_CLRMASK;
        rxd.ctrl |= FTGMAC100_RXD_OWNER_SW;
        rxd.ctrl |= FTGMAC100_RXD_LEN(len);
        if (bcst) {
            rxd.ctrl |= FTGMAC100_RXD_BCST;
        }
        if (mcst) {
            rxd.ctrl |= FTGMAC100_RXD_MCST;
        }
        if (ptr == buf) {
            rxd.ctrl |= FTGMAC100_RXD_FRS;
        }
        if (size == len) {
            rxd.ctrl |= FTGMAC100_RXD_LRS;
        }

        /* copy frame into rxd.buf */
        dma_memory_write(s->dma, rxd.buff, ptr, len);
        ptr  += len;
        size -= len;

        /* write-back the rx descriptor */
        ftgmac100_put_descriptor(s, &rxd, addr);

        /* advance the descriptor index */
        if (rxd.ctrl & FTGMAC100_RXD_END) {
            s->rxi = 0;
        } else {
            s->rxi += 1;
        }
    }

    /* update interrupt signal */
    regs->isr |= FTGMAC100_ISR_RXFIFO | FTGMAC100_ISR_RX;
    ftgmac100_update_irq(s);

    return (ssize_t)((ptrdiff_t)ptr - (ptrdiff_t)buf);
}

static uint32_t ftgmac100_transmit(Ftgmac100State *s, uint32_t bar,
        uint32_t idx)
{
    hwaddr addr;
    uint8_t *buf;
    Ftgmac100Desc txd;
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;
    uint32_t len, maccr = regs->maccr;

    if (!(maccr & FTGMAC100_MACCR_TX) || !(maccr & FTGMAC100_MACCR_TXDMA)) {
        return idx;
    }

    do {
        addr = bar + (idx * sizeof(Ftgmac100Desc));

        ftgmac100_get_descriptor(s, &txd, addr);
        if (!(txd.ctrl & FTGMAC100_TXD_OWNER_HW)) {
            regs->isr |= FTGMAC100_ISR_NTB;
            break;
        }

        len = FTGMAC100_TXD_LEN(txd.ctrl);

        if (txd.ctrl & FTGMAC100_TXD_FTS) {
            s->buf.len = 0;
        }

        if (len + s->buf.len > FTGMAC100_FRMAX) {
            fprintf(stderr, "ftgmac100: tx buffer overflow!\n");
            abort();
        }

        buf = s->buf.dat + s->buf.len;
        dma_memory_read(s->dma, txd.buff, buf, len);
        s->buf.len += len;

        if (txd.ctrl & FTGMAC100_TXD_LTS) {
            s->stats.tx_pkts++;
            if (maccr & FTGMAC100_MACCR_LOOP) {
                ftgmac100_receive(qemu_get_queue(s->nic),
                    s->buf.dat, s->buf.len);
            } else {
                qemu_send_packet(qemu_get_queue(s->nic),
                    s->buf.dat, s->buf.len);
            }
        }

        if (txd.ctrl & FTGMAC100_TXD_END) {
            idx = 0;
        } else {
            idx++;
        }

        if (txd.ctrl & FTGMAC100_TXD_TX2FIC) {
            regs->isr |= FTGMAC100_ISR_TXFIFO;
        }

        if (txd.ctrl & FTGMAC100_TXD_TXIC) {
            regs->isr |= FTGMAC100_ISR_TX;
        }

        /* update txd */
        txd.ctrl &= FTGMAC100_TXD_CLRMASK;
        ftgmac100_put_descriptor(s, (void *)&txd, addr);
    } while (1);

    return idx;
}

static void ftgmac100_bh(void *opaque)
{
    Ftgmac100State *s = FTGMAC100(opaque);
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;

    /* process tx ring */
    if (regs->txbar) {
        s->txi = ftgmac100_transmit(s, regs->txbar, s->txi);
    }

    /* update interrupt signal */
    ftgmac100_update_irq(s);
}

static void ftgmac100_chip_reset(Ftgmac100State *s)
{
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;

    s->txi = 0;
    s->rxi = 0;

    memset(s->regs, 0, sizeof(s->regs));

    /* rx buffer size (max. frame size) */
    regs->rbsr = FTGMAC100_FRMAX;

    if (s->bh) {
        qemu_bh_cancel(s->bh);
    }

    if (s->qtimer) {
        qemu_del_timer(s->qtimer);
    }

    ftgmac100_update_irq(s);
}

static uint64_t ftgmac100_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    uint32_t ret = 0;
    Ftgmac100State *s = FTGMAC100(opaque);

    if (addr >= FTGMAC100_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftgmac100: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(Ftgmac100Regs, mac):
        ret = s->conf.macaddr.a[1] | (s->conf.macaddr.a[0] << 8);
        break;
    case offsetof(Ftgmac100Regs, mac) + 4:
        ret = s->conf.macaddr.a[5] | (s->conf.macaddr.a[4] << 8)
            | (s->conf.macaddr.a[3] << 16) | (s->conf.macaddr.a[2] << 24);
        break;
    case offsetof(Ftgmac100Regs, rxptr):
        ret = s->rxi;
        break;
    case offsetof(Ftgmac100Regs, txptr):
        ret = s->txi;
        break;
    case offsetof(Ftgmac100Regs, txpkt):
        ret = s->stats.tx_pkts;
        break;
    case offsetof(Ftgmac100Regs, rxpkt):
        ret = s->stats.rx_pkts;
        break;
    case offsetof(Ftgmac100Regs, rxbcst):
        ret = s->stats.rx_bcst;
        break;
    case offsetof(Ftgmac100Regs, rxmcst):
        ret = s->stats.rx_mcst;
        break;
    case offsetof(Ftgmac100Regs, revr):
        ret = FTGMAC100_REVISION;
        break;
    case offsetof(Ftgmac100Regs, fear):
        ret = FTGMAC100_FEAR_TFSZ(16384) | FTGMAC100_FEAR_RFSZ(16384);
        break;
    case offsetof(Ftgmac100Regs, dmasr):
        ret = 0x0c000000; /* both tx & rx fifo are empty */
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftgmac100_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftgmac100State *s = FTGMAC100(opaque);
    Ftgmac100Regs *regs = (Ftgmac100Regs *)s->regs;

    if (addr >= FTGMAC100_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftgmac100: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftgmac100Regs, isr):
        regs->isr &= ~((uint32_t)val);
        ftgmac100_update_irq(s);
        break;
    case offsetof(Ftgmac100Regs, imr):
        regs->imr = (uint32_t)val;
        ftgmac100_update_irq(s);
        break;
    case offsetof(Ftgmac100Regs, mac):
        s->conf.macaddr.a[1] = extract32((uint32_t)val, 0, 8);
        s->conf.macaddr.a[0] = extract32((uint32_t)val, 8, 8);
        break;
    case offsetof(Ftgmac100Regs, mac) + 4:
        s->conf.macaddr.a[5] = extract32((uint32_t)val, 0, 8);
        s->conf.macaddr.a[4] = extract32((uint32_t)val, 8, 8);
        s->conf.macaddr.a[3] = extract32((uint32_t)val, 16, 8);
        s->conf.macaddr.a[2] = extract32((uint32_t)val, 24, 8);
        break;
    case offsetof(Ftgmac100Regs, maccr):
        if (val & FTGMAC100_MACCR_RESET) {
            val = 0;
            ftgmac100_chip_reset(s);
        }
        regs->maccr = (uint32_t)val;
        if ((val & FTGMAC100_MACCR_RX) && (val & FTGMAC100_MACCR_RXDMA)) {
            if (ftgmac100_can_receive(qemu_get_queue(s->nic))) {
                qemu_flush_queued_packets(qemu_get_queue(s->nic));
            }
        } else {
            qemu_del_timer(s->qtimer);
        }
        break;
    case offsetof(Ftgmac100Regs, macsr):
        regs->macsr &= ~((uint32_t)val);
        break;
    case offsetof(Ftgmac100Regs, phycr):
        /* Drop it, if PHY ID != 0 */
        if (!FTGMAC100_PHYCR_DEV(val) && (val & FTGMAC100_PHYCR_MDIORD)) {
            regs->phydr &= 0x0000ffff;
            regs->phydr |= miiphy_regs[FTGMAC100_PHYCR_REG(val)] << 16;
        }
        val &= ~(FTGMAC100_PHYCR_MDIOWR | FTGMAC100_PHYCR_MDIORD);
        regs->phycr = (uint32_t)val;
        break;
    case offsetof(Ftgmac100Regs, txpd):
    case offsetof(Ftgmac100Regs, hptxpd):
        qemu_bh_schedule(s->bh);
        break;
    case offsetof(Ftgmac100Regs, txpkt) ... FTGMAC100_REG_SIZE - 4:
        /* tx/rx statistics (read-only) */
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftgmac100_mmio_read,
    .write = ftgmac100_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftgmac100_cleanup(NetClientState *nc)
{
    Ftgmac100State *s = qemu_get_nic_opaque(nc);

    s->nic = NULL;
}

static NetClientInfo net_ftgmac100_info = {
    .type = NET_CLIENT_OPTIONS_KIND_NIC,
    .size = sizeof(NICState),
    .can_receive = ftgmac100_can_receive,
    .receive = ftgmac100_receive,
    .cleanup = ftgmac100_cleanup,
};

static void ftgmac100_reset(DeviceState *dev)
{
    ftgmac100_chip_reset(FTGMAC100(dev));
}

static void ftgmac100_timer(void *opaque)
{
    Ftgmac100State *s = FTGMAC100(opaque);

    if (ftgmac100_can_receive(qemu_get_queue(s->nic))) {
        qemu_flush_queued_packets(qemu_get_queue(s->nic));
    }
}

static void ftgmac100_realize(DeviceState *dev, Error **errp)
{
    Ftgmac100State *s = FTGMAC100(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->mmio,
                          &mmio_ops,
                          s,
                          TYPE_FTGMAC100,
                          FTGMAC100_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);

    qemu_macaddr_default_if_unset(&s->conf.macaddr);
    s->nic = qemu_new_nic(&net_ftgmac100_info,
                          &s->conf,
                          object_get_typename(OBJECT(dev)),
                          sbd->qdev.id,
                          s);
    qemu_format_nic_info_str(qemu_get_queue(s->nic), s->conf.macaddr.a);

    s->qtimer = qemu_new_timer_ms(vm_clock, ftgmac100_timer, s);
    s->dma = &dma_context_memory;
    s->bh = qemu_bh_new(ftgmac100_bh, s);

    ftgmac100_chip_reset(s);
}

static const VMStateDescription vmstate_ftgmac100 = {
    .name = TYPE_FTGMAC100,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftgmac100State, FTGMAC100_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static Property ftgmac100_properties[] = {
    DEFINE_NIC_PROPERTIES(Ftgmac100State, conf),
    DEFINE_PROP_END_OF_LIST(),
};

static void ftgmac100_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd  = &vmstate_ftgmac100;
    dc->props = ftgmac100_properties;
    dc->reset = ftgmac100_reset;
    dc->realize = ftgmac100_realize;
    dc->no_user = 1;
}

static const TypeInfo ftgmac100_info = {
    .name           = TYPE_FTGMAC100,
    .parent         = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(Ftgmac100State),
    .class_init     = ftgmac100_class_init,
};

static void ftgmac100_register_types(void)
{
    type_register_static(&ftgmac100_info);
}

/* Legacy helper function.  Should go away when machine config files are
   implemented.  */
void ftgmac100_init(NICInfo *nd, uint32_t base, qemu_irq irq)
{
    DeviceState *dev;
    SysBusDevice *s;

    qemu_check_nic_model(nd, TYPE_FTGMAC100);
    dev = qdev_create(NULL, TYPE_FTGMAC100);
    qdev_set_nic_properties(dev, nd);
    qdev_init_nofail(dev);
    s = SYS_BUS_DEVICE(dev);
    sysbus_mmio_map(s, 0, base);
    sysbus_connect_irq(s, 0, irq);
}

type_init(ftgmac100_register_types)
