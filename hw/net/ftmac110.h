/*
 * Faraday FTMAC110 10/100Mbps Ethernet Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#ifndef HW_NET_FTMAC110_H
#define HW_NET_FTMAC110_H

typedef struct Ftmac110Regs {
    uint32_t isr;    /* 0x00: Interrups Status Register */
    uint32_t ier;    /* 0x04: Interrupt Enable Register */
    uint32_t mac[2]; /* 0x08: MAC Address */
    uint32_t mht[2]; /* 0x10: Multicast Hash Table Register */
    uint32_t txpd;   /* 0x18: Tx Poll Demand Register */
    uint32_t rxpd;   /* 0x1c: Rx Poll Demand Register */
    uint32_t txbar;  /* 0x20: Tx Ring Base Address Register */
    uint32_t rxbar;  /* 0x24: Rx Ring Base Address Register */
    uint32_t itc;    /* 0x28: Interrupt Timer Control Register */
    uint32_t aptc;   /* 0x2C: Automatic Polling Timer Control Register */
    uint32_t dblac;  /* 0x30: DMA Burst Length&Arbitration Control */
    uint32_t revr;   /* 0x34: Revision Register */
    uint32_t fear;   /* 0x38: Feature Register */
    uint32_t rsvd0[19];
    uint32_t maccr;  /* 0x88: MAC Control Register */
    uint32_t macsr;  /* 0x8C: MAC Status Register */
    uint32_t phycr;  /* 0x90: PHY Control Register */
    uint32_t phydr;  /* 0x94: PHY Data Register */
    uint32_t fcr;    /* 0x98: Flow Control Register */
    uint32_t bpr;    /* 0x9C: Back Pressure Register */

    uint32_t rsvd1[16];/* 0xA0 ~ 0xDF */
    uint32_t rxerr[3];
    uint32_t rxbcst;
    uint32_t rxmcst;
    uint32_t rxpkt;
    uint32_t txpkt;
} Ftmac110Regs;

#define FTMAC110_REG_SIZE       sizeof(Ftmac110Regs)

/*
 * Interrupt status register
 */
#define FTMAC110_ISR_PHYST      (1 << 9) /* phy status change */
#define FTMAC110_ISR_BUS        (1 << 8) /* bus error */
#define FTMAC110_ISR_RXLOST     (1 << 7) /* rx lost */
#define FTMAC110_ISR_RXFIFO     (1 << 6) /* rx to fifo */
#define FTMAC110_ISR_TXLOST     (1 << 5) /* tx lost */
#define FTMAC110_ISR_TX         (1 << 4) /* tx to ethernet */
#define FTMAC110_ISR_NTB        (1 << 3) /* out of tx buffer */
#define FTMAC110_ISR_TXFIFO     (1 << 2) /* tx to fifo */
#define FTMAC110_ISR_NRB        (1 << 1) /* out of rx buffer */
#define FTMAC110_ISR_RX         (1 << 0) /* rx to buffer */

/*
 * Feature register
 */
#define FTMAC110_FEAR_HD        (1 << 2) /* support half-duplex */
#define FTMAC110_FEAR_FD        (1 << 1) /* support full-duplex */
#define FTMAC110_FEAR_WOL       (1 << 0) /* support Wake-On-LAN */

/*
 * MAC control register
 */
#define FTMAC110_MACCR_100M     (1 << 18) /* 100Mbps mode */
#define FTMAC110_MACCR_RXBCST   (1 << 17) /* rx broadcast packet */
#define FTMAC110_MACCR_RXMCST   (1 << 16) /* rx multicast packet */
#define FTMAC110_MACCR_FD       (1 << 15) /* full duplex */
#define FTMAC110_MACCR_CRCAPD   (1 << 14) /* tx crc append */
#define FTMAC110_MACCR_RXALL    (1 << 12) /* rx all packets */
#define FTMAC110_MACCR_RXFTL    (1 << 11) /* rx packet even it's > 1518 byte */
#define FTMAC110_MACCR_RXRUNT   (1 << 10) /* rx packet even it's < 64 byte */
#define FTMAC110_MACCR_RXMH     (1 << 9)  /* rx multicast hash table */
#define FTMAC110_MACCR_RX       (1 << 8)  /* rx enable */
#define FTMAC110_MACCR_RXINHDTX (1 << 6)  /* rx in half duplex tx */
#define FTMAC110_MACCR_TX       (1 << 5)  /* tx enable */
#define FTMAC110_MACCR_CRCDIS   (1 << 4)  /* discard crc error */
#define FTMAC110_MACCR_LOOP     (1 << 3)  /* loop-back */
#define FTMAC110_MACCR_RESET    (1 << 2)  /* reset */
#define FTMAC110_MACCR_RXDMA    (1 << 1)  /* rx dma enable */
#define FTMAC110_MACCR_TXDMA    (1 << 0)  /* tx dma enable */

/*
 * PHY control register
 */
#define FTMAC110_PHYCR_READ     (1 << 26)
#define FTMAC110_PHYCR_WRITE    (1 << 27)
#define FTMAC110_PHYCR_REG(x)   extract32((uint32_t)(x), 21, 5)
#define FTMAC110_PHYCR_DEV(x)   extract32((uint32_t)(x), 16, 5)

/*
 * descriptor structure
 */
typedef struct Ftmac110Desc {
    uint64_t ctrl;
    uint32_t buff;
    uint32_t rsvd;
} Ftmac110Desc;

#define FTMAC110_RXD_END        ((uint64_t)1 << 63)
#define FTMAC110_RXD_BUFSZ(x)   (((uint64_t)(x) >> 32) & 0x7ff)

#define FTMAC110_RXD_OWNER      ((uint64_t)1 << 31) /* owner: 1=HW, 0=SW */
#define FTMAC110_RXD_FRS        ((uint64_t)1 << 29) /* first pkt desc */
#define FTMAC110_RXD_LRS        ((uint64_t)1 << 28) /* last pkt desc */
#define FTMAC110_RXD_ODDNB      ((uint64_t)1 << 22) /* odd nibble */
#define FTMAC110_RXD_RUNT       ((uint64_t)1 << 21) /* runt pkt */
#define FTMAC110_RXD_FTL        ((uint64_t)1 << 20) /* frame too long */
#define FTMAC110_RXD_CRC        ((uint64_t)1 << 19) /* pkt crc error */
#define FTMAC110_RXD_ERR        ((uint64_t)1 << 18) /* bus error */
#define FTMAC110_RXD_BCST       ((uint64_t)1 << 17) /* Bcst pkt */
#define FTMAC110_RXD_MCST       ((uint64_t)1 << 16) /* Mcst pkt */
#define FTMAC110_RXD_LEN(x)     ((uint64_t)((x) & 0x7ff))

#define FTMAC110_RXD_CLRMASK    ((uint64_t)0xffffffff00000000)

#define FTMAC110_TXD_END        ((uint64_t)1 << 63) /* end of ring */
#define FTMAC110_TXD_TXIC       ((uint64_t)1 << 62) /* tx done interrupt */
#define FTMAC110_TXD_TX2FIC     ((uint64_t)1 << 61) /* tx fifo interrupt */
#define FTMAC110_TXD_FTS        ((uint64_t)1 << 60) /* first pkt desc */
#define FTMAC110_TXD_LTS        ((uint64_t)1 << 59) /* last pkt desc */
#define FTMAC110_TXD_LEN(x)     (((uint64_t)(x) >> 32) & 0x7ff)

#define FTMAC110_TXD_OWNER      ((uint64_t)1 << 31) /* owner: 1=HW, 0=SW */
#define FTMAC110_TXD_COL        ((uint64_t)3)       /* collision */

#define FTMAC110_TXD_CLRMASK    (FTMAC110_TXD_END)

#endif  /* FTMAC110_H */
