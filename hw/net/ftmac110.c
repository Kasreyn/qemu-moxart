/*
 * Faraday FTMAC110 10/100Mbps Ethernet Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"
#include "sysemu/sysemu.h"
#include "sysemu/dma.h"
#include "net/net.h"
#include "ftmac110.h"

#define FTMAC110_REVISION   FARADAY_REVISION(0, 6, 0)

#ifdef CONFIG_MOXART
#undef FTMAC110_REVISION
#define FTMAC110_REVISION   FARADAY_REVISION(1, 4, 0)
#endif

#define FTMAC110_FRMAX      1536 /* Max. frame size */

#ifndef DEBUG
#define DEBUG   0
#endif

#define DPRINTF(fmt, ...) \
    do { \
        if (DEBUG) { \
            fprintf(stderr, "[qemu]" fmt , ## __VA_ARGS__); \
        } \
    } while (0)

#define TYPE_FTMAC110  "ftmac110"

typedef struct Ftmac110State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    QEMUBH *bh;
    qemu_irq irq;
    NICState *nic;
    NICConf conf;
    DMAContext *dma;
    QEMUTimer *qtimer;

    uint32_t txi;
    uint32_t rxi;

    struct {
        uint8_t dat[FTMAC110_FRMAX];
        uint32_t len;
    } buf;

    struct {
        uint32_t tx_pkts;
        uint32_t rx_pkts;
        uint32_t rx_bcst;
        uint32_t rx_mcst;
    } stats;

    /* hw registers */
    uint32_t regs[FTMAC110_REG_SIZE >> 2];
} Ftmac110State;

#define FTMAC110(obj) \
    OBJECT_CHECK(Ftmac110State, obj, TYPE_FTMAC110)

#define FRM_PROTO(frm) \
    ((((uint8_t *)(frm))[12] << 8) | ((uint8_t *)(frm))[13])

#define FRM_BCST(frm) \
    ((((uint8_t *)(frm))[0] == 0xff) \
    && (((uint8_t *)(frm))[1] == 0xff) \
    && (((uint8_t *)(frm))[2] == 0xff) \
    && (((uint8_t *)(frm))[3] == 0xff) \
    && (((uint8_t *)(frm))[4] == 0xff) \
    && (((uint8_t *)(frm))[5] == 0xff))

#define FRM_MCST(frm) \
    ((((uint8_t *)(frm))[0] == 0x01) \
    && (((uint8_t *)(frm))[1] == 0x00) \
    && (((uint8_t *)(frm))[2] == 0x5e) \
    && (((uint8_t *)(frm))[3] <= 0x7f))

#ifdef CONFIG_MOXART
/* Emulating a RTL8201 PHY with 100Mbps link state */
static const uint16_t miiphy_regs[] = {
    0x2100, 0x782d, 0x0000, 0x8201, 0x01e1, 0x45e0,
};
#else
/* Emulating a Davicom PHY with 100Mbps link state */
static const uint16_t miiphy_regs[] = {
    0x3100, 0x786d, 0x0181, 0xb8a0, 0x01e1, 0x45e1,
};
#endif

static int ftmac110_mcast_hash(Ftmac110Regs *regs, const uint8_t *data)
{
#define CRCPOLY_LE 0xedb88320
    int i, len = 6;
    uint32_t crc = 0xFFFFFFFF;

    while (len--) {
        crc ^= *data++;
        for (i = 0; i < 8; i++) {
            crc = (crc >> 1) ^ ((crc & 1) ? CRCPOLY_LE : 0);
        }
    }

    /* Reverse CRC32 and return MSB 6 bits only */
    return bitrev8(crc >> 24) >> 2;
}

static void ftmac110_get_descriptor(Ftmac110State *s, Ftmac110Desc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac110: descriptor is not 16-byte aligned!\n");
    }

    dma_memory_read(s->dma, addr, d, sizeof(Ftmac110Desc));

    d->ctrl = le64_to_cpu(d->ctrl);
    d->rsvd = le32_to_cpu(d->rsvd);
    d->buff = le32_to_cpu(d->buff);
}

static void ftmac110_put_descriptor(Ftmac110State *s, Ftmac110Desc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac110: descriptor is not 16-byte aligned!\n");
    }

    d->ctrl = cpu_to_le64(d->ctrl);
    d->rsvd = cpu_to_le32(d->rsvd);
    d->buff = cpu_to_le32(d->buff);

    dma_memory_write(s->dma, addr, d, sizeof(Ftmac110Desc));
}

static void ftmac110_update_irq(Ftmac110State *s)
{
    Ftmac110Regs *regs = (Ftmac110Regs *)s->regs;

    qemu_set_irq(s->irq, !!(regs->isr & regs->ier));
}

static int ftmac110_can_receive(NetClientState *nc)
{
    int ret = 0;
    Ftmac110Desc rxd;
    Ftmac110State *s = qemu_get_nic_opaque(nc);
    Ftmac110Regs *regs = (Ftmac110Regs *)s->regs;
    hwaddr addr = regs->rxbar + s->rxi * sizeof(Ftmac110Desc);
    uint32_t maccr = regs->maccr;

    if ((maccr & FTMAC110_MACCR_RX) && (maccr & FTMAC110_MACCR_RXDMA)) {
        ftmac110_get_descriptor(s, (void *)&rxd, addr);
        if (rxd.ctrl & FTMAC110_RXD_OWNER) {
            ret = 1;
        } else {
            qemu_mod_timer(s->qtimer, qemu_get_clock_ms(vm_clock) + 10);
        }
    }

    return ret;
}

static ssize_t ftmac110_receive(NetClientState *nc, const uint8_t *buf,
        size_t size)
{
    Ftmac110State *s = qemu_get_nic_opaque(nc);
    Ftmac110Regs *regs = (Ftmac110Regs *)s->regs;
    uint32_t maccr = regs->maccr;
    uint16_t proto = FRM_PROTO(buf);
    bool bcst, mcst, ftl;
    const uint8_t *ptr;

    s->stats.rx_pkts++;

    /* Check if it's a long frame. (Frame checksum is excluded) */
    ftl = false;
    if (proto == 0x8100) { /* 802.1Q VLAN */
        ftl = !!(size > 1518);
    } else {
        ftl = !!(size > 1514);
    }
    if (ftl) {
        DPRINTF("ftmac110: frame too long, drop it\n");
        return -1;
    }

    /* Check if it's a broadcast frame */
    if (FRM_BCST(buf)) {
        bcst = true;
        s->stats.rx_bcst++;
        if (!(maccr & FTMAC110_MACCR_RXALL)
            && !(maccr & FTMAC110_MACCR_RXBCST)) {
            DPRINTF("ftmac110: bcst filtered\n");
            return -1;
        }
    } else {
        bcst = false;
    }

    /* Check if it's a multicast frame */
    if (FRM_MCST(buf)) {
        mcst = true;
        s->stats.rx_mcst++;
        if (!(maccr & FTMAC110_MACCR_RXALL)
            && !(maccr & FTMAC110_MACCR_RXMCST)) {
            int hash;

            if (!(maccr & FTMAC110_MACCR_RXMH)) {
                DPRINTF("ftmac110: mcst filtered\n");
                return -1;
            }
            hash = ftmac110_mcast_hash(regs, buf);
            if (!(regs->mht[hash >> 5] & BIT(hash & 0x1f))) {
                DPRINTF("ftmac110: mcst filtered\n");
                return -1;
            }
        }
    } else {
        mcst = false;
    }

    /* Check if its DA matches NIC mac address */
    if (!(maccr & FTMAC110_MACCR_RXALL) && !bcst && !mcst) {
        if (memcmp(s->conf.macaddr.a, buf, 6)) {
            DPRINTF("ftmac110: DA!=NIC filtered\n");
            return -1;
        }
    }

    ptr = buf;
    while (size > 0) {
        Ftmac110Desc rxd;
        hwaddr addr;
        size_t len;

        addr = regs->rxbar + s->rxi * sizeof(Ftmac110Desc);
        ftmac110_get_descriptor(s, &rxd, addr);
        if (!(rxd.ctrl & FTMAC110_RXD_OWNER)) {
            regs->isr |= FTMAC110_ISR_NRB;
            DPRINTF("ftmac110: out of rxd!?\n");
            return -1;
        }

        len = MIN(size, FTMAC110_RXD_BUFSZ(rxd.ctrl));

        /* update rxd.ctrl */
        rxd.ctrl &= FTMAC110_RXD_CLRMASK;
        rxd.ctrl |= FTMAC110_RXD_LEN(len);
        if (bcst) {
            rxd.ctrl |= FTMAC110_RXD_BCST;
        }
        if (mcst) {
            rxd.ctrl |= FTMAC110_RXD_MCST;
        }
        if (ptr == buf) {
            rxd.ctrl |= FTMAC110_RXD_FRS;
        }
        if (size == len) {
            rxd.ctrl |= FTMAC110_RXD_LRS;
        }

        /* copy frame into rxd.buf */
        dma_memory_write(s->dma, rxd.buff, ptr, len);
        ptr  += len;
        size -= len;

        /* write-back the rx descriptor */
        ftmac110_put_descriptor(s, &rxd, addr);

        /* advance the descriptor index */
        if (rxd.ctrl & FTMAC110_RXD_END) {
            s->rxi = 0;
        } else {
            s->rxi += 1;
        }
    }

    /* update interrupt signal */
    regs->isr |= FTMAC110_ISR_RXFIFO | FTMAC110_ISR_RX;
    ftmac110_update_irq(s);

    return (ssize_t)((ptrdiff_t)ptr - (ptrdiff_t)buf);
}

static uint32_t ftmac110_transmit(Ftmac110State *s, uint32_t bar,
        uint32_t idx)
{
    hwaddr addr;
    uint8_t *buf;
    Ftmac110Desc txd;
    Ftmac110Regs *regs = (Ftmac110Regs *)s->regs;
    uint32_t len, maccr = regs->maccr;

    if (!(maccr & FTMAC110_MACCR_TX) || !(maccr & FTMAC110_MACCR_TXDMA)) {
        return idx;
    }

    do {
        addr = bar + (idx * sizeof(Ftmac110Desc));

        ftmac110_get_descriptor(s, &txd, addr);
        if (!(txd.ctrl & FTMAC110_TXD_OWNER)) {
            regs->isr |= FTMAC110_ISR_NTB;
            break;
        }

        len = FTMAC110_TXD_LEN(txd.ctrl);

        if (txd.ctrl & FTMAC110_TXD_FTS) {
            s->buf.len = 0;
        }

        if (len + s->buf.len > FTMAC110_FRMAX) {
            fprintf(stderr, "ftmac110: tx buffer overflow!\n");
            abort();
        }

        buf = s->buf.dat + s->buf.len;
        dma_memory_read(s->dma, txd.buff, buf, len);
        s->buf.len += len;

        if (txd.ctrl & FTMAC110_TXD_LTS) {
            s->stats.tx_pkts++;
            if (maccr & FTMAC110_MACCR_LOOP) {
                ftmac110_receive(qemu_get_queue(s->nic),
                    s->buf.dat, s->buf.len);
            } else {
                qemu_send_packet(qemu_get_queue(s->nic),
                    s->buf.dat, s->buf.len);
            }
        }

        if (txd.ctrl & FTMAC110_TXD_END) {
            idx = 0;
        } else {
            idx++;
        }

        if (txd.ctrl & FTMAC110_TXD_TX2FIC) {
            regs->isr |= FTMAC110_ISR_TXFIFO;
        }

        if (txd.ctrl & FTMAC110_TXD_TXIC) {
            regs->isr |= FTMAC110_ISR_TX;
        }

        /* update txd */
        txd.ctrl &= FTMAC110_TXD_CLRMASK;
        ftmac110_put_descriptor(s, (void *)&txd, addr);
    } while (1);

    return idx;
}

static void ftmac110_bh(void *opaque)
{
    Ftmac110State *s = FTMAC110(opaque);
    Ftmac110Regs *regs = (Ftmac110Regs *)s->regs;

    /* process tx ring */
    if (regs->txbar) {
        s->txi = ftmac110_transmit(s, regs->txbar, s->txi);
    }

    /* update interrupt signal */
    ftmac110_update_irq(s);
}

static void ftmac110_chip_reset(Ftmac110State *s)
{
    memset(s->regs, 0, sizeof(s->regs));

    s->txi = 0;
    s->rxi = 0;

    if (s->bh) {
        qemu_bh_cancel(s->bh);
    }

    if (s->qtimer) {
        qemu_del_timer(s->qtimer);
    }

    ftmac110_update_irq(s);
}

static uint64_t ftmac110_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    uint32_t ret = 0;
    Ftmac110State *s = FTMAC110(opaque);

    if (addr >= FTMAC110_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac110: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftmac110Regs, isr):
        ret = s->regs[addr >> 2];
        s->regs[addr >> 2] = 0;
        ftmac110_update_irq(s);
        break;
    case offsetof(Ftmac110Regs, mac):
        ret = s->conf.macaddr.a[1] | (s->conf.macaddr.a[0] << 8);
        break;
    case offsetof(Ftmac110Regs, mac) + 4:
        ret = s->conf.macaddr.a[5] | (s->conf.macaddr.a[4] << 8)
            | (s->conf.macaddr.a[3] << 16) | (s->conf.macaddr.a[2] << 24);
        break;
    case offsetof(Ftmac110Regs, txpkt):
        ret = s->stats.tx_pkts;
        break;
    case offsetof(Ftmac110Regs, rxpkt):
        ret = s->stats.rx_pkts;
        break;
    case offsetof(Ftmac110Regs, rxbcst):
        ret = s->stats.rx_bcst;
        break;
    case offsetof(Ftmac110Regs, rxmcst):
        ret = s->stats.rx_mcst;
        break;
    case offsetof(Ftmac110Regs, revr):
        ret = FTMAC110_REVISION;
        break;
    case offsetof(Ftmac110Regs, fear):
#ifdef CONFIG_MOXART
        ret = FTMAC110_FEAR_HD | FTMAC110_FEAR_FD | FTMAC110_FEAR_WOL;
#else
        ret = FTMAC110_FEAR_HD | FTMAC110_FEAR_FD;
#endif
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftmac110_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftmac110State *s = FTMAC110(opaque);
    Ftmac110Regs *regs = (Ftmac110Regs *)s->regs;

    if (addr >= FTMAC110_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac110: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftmac110Regs, ier):
        regs->ier = (uint32_t)val;
        ftmac110_update_irq(s);
        break;
    case offsetof(Ftmac110Regs, mac):
        s->conf.macaddr.a[1] = extract32((uint32_t)val, 0, 8);
        s->conf.macaddr.a[0] = extract32((uint32_t)val, 8, 8);
        break;
    case offsetof(Ftmac110Regs, mac) + 4:
        s->conf.macaddr.a[5] = extract32((uint32_t)val, 0, 8);
        s->conf.macaddr.a[4] = extract32((uint32_t)val, 8, 8);
        s->conf.macaddr.a[3] = extract32((uint32_t)val, 16, 8);
        s->conf.macaddr.a[2] = extract32((uint32_t)val, 24, 8);
        break;
    case offsetof(Ftmac110Regs, maccr):
        if (val & FTMAC110_MACCR_RESET) {
            val = 0;
            ftmac110_chip_reset(s);
        }
        regs->maccr = (uint32_t)val;
        if ((val & FTMAC110_MACCR_RX) && (val & FTMAC110_MACCR_RXDMA)) {
            if (ftmac110_can_receive(qemu_get_queue(s->nic))) {
                qemu_flush_queued_packets(qemu_get_queue(s->nic));
            }
        } else {
            qemu_del_timer(s->qtimer);
        }
        break;
    case offsetof(Ftmac110Regs, macsr):
        regs->macsr &= ~((uint32_t)val);
        break;
    case offsetof(Ftmac110Regs, phycr):
#ifdef CONFIG_MOXART
        /* Drop it, if PHY ID != 1 */
        if (FTMAC110_PHYCR_DEV(val) == 1 && (val & FTMAC110_PHYCR_READ)) {
#else
        /* Drop it, if PHY ID != 0 */
        if (!FTMAC110_PHYCR_DEV(val) && (val & FTMAC110_PHYCR_READ)) {
#endif
            val &= 0xffff0000;
            val |= miiphy_regs[FTMAC110_PHYCR_REG(val)];
        } else {
            val |= 0x0000ffff;
        }
        val &= ~(FTMAC110_PHYCR_WRITE | FTMAC110_PHYCR_READ);
        regs->phycr = (uint32_t)val;
        break;
    case offsetof(Ftmac110Regs, txpd):
        qemu_bh_schedule(s->bh);
        break;
    /* read-only register */
    case offsetof(Ftmac110Regs, isr):
    case offsetof(Ftmac110Regs, revr):
    case offsetof(Ftmac110Regs, fear):
    case offsetof(Ftmac110Regs, rsvd1) ... FTMAC110_REG_SIZE - 4:
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftmac110_mmio_read,
    .write = ftmac110_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftmac110_cleanup(NetClientState *nc)
{
    Ftmac110State *s = qemu_get_nic_opaque(nc);

    s->nic = NULL;
}

static NetClientInfo net_ftmac110_info = {
    .type = NET_CLIENT_OPTIONS_KIND_NIC,
    .size = sizeof(NICState),
    .can_receive = ftmac110_can_receive,
    .receive = ftmac110_receive,
    .cleanup = ftmac110_cleanup,
};

static void ftmac110_reset(DeviceState *dev)
{
    ftmac110_chip_reset(FTMAC110(dev));
}

static void ftmac110_timer(void *opaque)
{
    Ftmac110State *s = FTMAC110(opaque);

    if (ftmac110_can_receive(qemu_get_queue(s->nic))) {
        qemu_flush_queued_packets(qemu_get_queue(s->nic));
    }
}

static void ftmac110_realize(DeviceState *dev, Error **errp)
{
    Ftmac110State *s = FTMAC110(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->mmio,
                          &mmio_ops,
                          s,
                          TYPE_FTMAC110,
                          FTMAC110_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);

    qemu_macaddr_default_if_unset(&s->conf.macaddr);
    s->nic = qemu_new_nic(&net_ftmac110_info,
                          &s->conf,
                          object_get_typename(OBJECT(dev)),
                          sbd->qdev.id,
                          s);
    qemu_format_nic_info_str(qemu_get_queue(s->nic), s->conf.macaddr.a);

    s->qtimer = qemu_new_timer_ms(vm_clock, ftmac110_timer, s);
    s->dma = &dma_context_memory;
    s->bh = qemu_bh_new(ftmac110_bh, s);

    ftmac110_chip_reset(s);
}

static const VMStateDescription vmstate_ftmac110 = {
    .name = TYPE_FTMAC110,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftmac110State, FTMAC110_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static Property ftmac110_properties[] = {
    DEFINE_NIC_PROPERTIES(Ftmac110State, conf),
    DEFINE_PROP_END_OF_LIST(),
};

static void ftmac110_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd  = &vmstate_ftmac110;
    dc->props = ftmac110_properties;
    dc->reset = ftmac110_reset;
    dc->realize = ftmac110_realize;
    dc->no_user = 1;
}

static const TypeInfo ftmac110_info = {
    .name           = TYPE_FTMAC110,
    .parent         = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(Ftmac110State),
    .class_init     = ftmac110_class_init,
};

static void ftmac110_register_types(void)
{
    type_register_static(&ftmac110_info);
}

/* Legacy helper function.  Should go away when machine config files are
   implemented.  */
void ftmac110_init(NICInfo *nd, uint32_t base, qemu_irq irq)
{
    DeviceState *dev;
    SysBusDevice *s;

    qemu_check_nic_model(nd, TYPE_FTMAC110);
    dev = qdev_create(NULL, TYPE_FTMAC110);
    qdev_set_nic_properties(dev, nd);
    qdev_init_nofail(dev);
    s = SYS_BUS_DEVICE(dev);
    sysbus_mmio_map(s, 0, base);
    sysbus_connect_irq(s, 0, irq);
}

type_init(ftmac110_register_types)
