/*
 * Faraday FTMAC100 10/100Mbps Ethernet Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"
#include "sysemu/sysemu.h"
#include "sysemu/dma.h"
#include "net/net.h"
#include "ftmac100.h"

#define FTMAC100_REVISION   FARADAY_REVISION(1, 4, 0)

#define FTMAC100_FRMAX      1536 /* Max. frame size */

#define DEBUG   0

#ifndef DEBUG
#define DEBUG   0
#endif

#define DPRINTF(fmt, ...) \
    do { \
        if (DEBUG) { \
            fprintf(stderr, "[qemu]" fmt , ## __VA_ARGS__); \
        } \
    } while (0)

#define TYPE_FTMAC100  "ftmac100"

typedef struct Ftmac100State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    QEMUBH *bh;
    qemu_irq irq;
    NICState *nic;
    NICConf conf;
    DMAContext *dma;
    QEMUTimer *qtimer;

    uint32_t txi;
    uint32_t rxi;

    struct {
        uint8_t dat[FTMAC100_FRMAX];
        uint32_t len;
    } buf;

    struct {
        uint32_t tx_pkts;
        uint32_t rx_pkts;
        uint32_t rx_bcst;
        uint32_t rx_mcst;
    } stats;

    /* hw registers */
    uint32_t regs[FTMAC100_REG_SIZE >> 2];
} Ftmac100State;

#define FTMAC100(obj) \
    OBJECT_CHECK(Ftmac100State, obj, TYPE_FTMAC100)

#define FRM_PROTO(frm) \
    ((((uint8_t *)(frm))[12] << 8) | ((uint8_t *)(frm))[13])

/* Emulating a RTL8201 PHY with 100Mbps link state */
static const uint16_t miiphy_regs[] = {
    0x2100, 0x782d, 0x0000, 0x8201, 0x01e1, 0x45e0,
};

static int ftmac100_mcast_hash(Ftmac100Regs *regs, const uint8_t *data)
{
#define CRCPOLY_LE 0xedb88320
    int i, len = 6;
    uint32_t crc = 0xFFFFFFFF;

    while (len--) {
        crc ^= *data++;
        for (i = 0; i < 8; i++) {
            crc = (crc >> 1) ^ ((crc & 1) ? CRCPOLY_LE : 0);
        }
    }

    /* Reverse CRC32 and return MSB 6 bits only */
    return bitrev8(crc >> 24) >> 2;
}

static void ftmac100_get_rxdesc(Ftmac100State *s, Ftmac100RxDesc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac100: descriptor is not 16-byte aligned!\n");
    }

    dma_memory_read(s->dma, addr, d, sizeof(Ftmac100RxDesc));

    d->rxdes0 = le32_to_cpu(d->rxdes0);
    d->rxdes1 = le32_to_cpu(d->rxdes1);
    d->rxdes2 = le32_to_cpu(d->rxdes2);
    d->rxdes3 = le32_to_cpu(d->rxdes3);
}

static void ftmac100_get_txdesc(Ftmac100State *s, Ftmac100TxDesc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac100: descriptor is not 16-byte aligned!\n");
    }

    dma_memory_read(s->dma, addr, d, sizeof(Ftmac100RxDesc));

    d->txdes0 = le32_to_cpu(d->txdes0);
    d->txdes1 = le32_to_cpu(d->txdes1);
    d->txdes2 = le32_to_cpu(d->txdes2);
    d->txdes3 = le32_to_cpu(d->txdes3);
}

static void ftmac100_put_rxdesc(Ftmac100State *s, Ftmac100RxDesc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac100: descriptor is not 16-byte aligned!\n");
    }

    d->rxdes0 = cpu_to_le32(d->rxdes0);
    d->rxdes1 = cpu_to_le32(d->rxdes1);
    d->rxdes2 = cpu_to_le32(d->rxdes2);
    d->rxdes3 = cpu_to_le32(d->rxdes3);

    dma_memory_write(s->dma, addr, d, sizeof(Ftmac100RxDesc));
}

static void ftmac100_put_txdesc(Ftmac100State *s, Ftmac100TxDesc *d,
        hwaddr addr)
{
    if (addr & 0x0f) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac100: descriptor is not 16-byte aligned!\n");
    }

    d->txdes0 = cpu_to_le32(d->txdes0);
    d->txdes1 = cpu_to_le32(d->txdes1);
    d->txdes2 = cpu_to_le32(d->txdes2);
    d->txdes3 = cpu_to_le32(d->txdes3);

    dma_memory_write(s->dma, addr, d, sizeof(Ftmac100RxDesc));
}

static void ftmac100_update_irq(Ftmac100State *s)
{
    Ftmac100Regs *regs = (Ftmac100Regs *)s->regs;

    qemu_set_irq(s->irq, !!(regs->isr & regs->ier));
}

static int ftmac100_can_receive(NetClientState *nc)
{
    int ret = 0;
    Ftmac100RxDesc rxd;
    Ftmac100State *s = qemu_get_nic_opaque(nc);
    Ftmac100Regs *regs = (Ftmac100Regs *)s->regs;
    hwaddr addr = (regs->rxbar & ~0xf) + s->rxi * sizeof(Ftmac100RxDesc);
    uint32_t maccr = regs->maccr;

    if ((maccr & FTMAC100_MACCR_RX) && (maccr & FTMAC100_MACCR_RXDMA)
        && !(maccr & FTMAC100_MACCR_LOOP)) {
        ftmac100_get_rxdesc(s, &rxd, addr);
        if (rxd.rxdes0 & FTMAC100_RXDES0_RXDMA_OWN) {
            ret = 1;
        } else {
            qemu_mod_timer(s->qtimer, qemu_get_clock_ms(vm_clock) + 10);
        }
    }

    return ret;
}

static ssize_t ftmac100_receive(NetClientState *nc, const uint8_t *buf,
        size_t size)
{
    Ftmac100State *s = qemu_get_nic_opaque(nc);
    Ftmac100Regs *regs = (Ftmac100Regs *)s->regs;
    uint32_t maccr = regs->maccr;
    uint16_t proto = FRM_PROTO(buf);
    bool bcst, mcst, ftl;
    const uint8_t *ptr;

    static const uint8_t broadcast_macaddr[6] =
        { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

    s->stats.rx_pkts++;

    if (!(maccr & FTMAC100_MACCR_RXFTL)) {
        /* Check if it's a long frame. (Frame checksum is excluded) */
        ftl = false;
        if (proto == 0x8100) { /* 802.1Q VLAN */
            ftl = !!(size > 1518);
        } else {
            ftl = !!(size > 1514);
        }

        if (ftl) {
            DPRINTF("ftmac100: frame too long, drop it\n");
            return -1;
        }
    }

    if (!(maccr & FTMAC100_MACCR_RXRUNT) && size < 64) {
        DPRINTF("ftmac100: frame too short, drop it\n");
        return -1;
    }

    bcst = false;
    mcst = false;

    if (!(maccr & FTMAC100_MACCR_RXALL)) {
        /* Check if it's a broadcast frame */
        if (!memcmp(buf,  broadcast_macaddr, 6)) {
            s->stats.rx_bcst++;
            if (!(maccr & FTMAC100_MACCR_RXBCST)) {
                DPRINTF("ftmac100: bcst filtered\n");
                return -1;
            }
            bcst = true;
        } else if (buf[0] & 0x01) {
            s->stats.rx_mcst++;
            if (!(maccr & FTMAC100_MACCR_RXMCST)) {
                int hash;

                if (!(maccr & FTMAC100_MACCR_RXMH)) {
                    DPRINTF("ftmac100: mcst filtered\n");
                    return -1;
                }
                hash = ftmac100_mcast_hash(regs, buf);
                if (!(regs->mht[hash >> 5] & BIT(hash & 0x1f))) {
                    DPRINTF("ftmac100: mcst filtered\n");
                    return -1;
                }
            }
            mcst = true;
        } else if (memcmp(s->conf.macaddr.a, buf, 6)) {
            DPRINTF("ftmac100: DA!=NIC filtered\n");
            return -1;
        }
    }

    ptr = buf;
    while (size > 0) {
        Ftmac100RxDesc rxd;
        hwaddr addr;
        size_t len;

        addr = (regs->rxbar & ~0xf) + s->rxi * sizeof(Ftmac100RxDesc);

        ftmac100_get_rxdesc(s, &rxd, addr);

    /*fprintf(stderr, "ftmac100 rx: %" HWADDR_PRIx " 0=0x%08" PRIx32 ", 1=0x%08" PRIx32 "\n", addr, rxd.rxdes0, rxd.rxdes1);*/

        if (!(rxd.rxdes0 & FTMAC100_RXDES0_RXDMA_OWN)) {
            regs->isr |= FTMAC100_ISR_NRB;
            DPRINTF("ftmac100: out of rxd!?\n");
            return -1;
        }

        len = MIN(size, FTMAC100_RXDES1_RXBUF_SIZE(rxd.rxdes1));

        /* update rxd */
        rxd.rxdes0 = len & FTMAC100_RXDES0_RFL;

        if (bcst) {
            rxd.rxdes0 |= FTMAC100_RXDES0_BROADCAST;
        }

        if (mcst) {
            rxd.rxdes0 |= FTMAC100_RXDES0_MULTICAST;
        }

        if (ptr == buf) {
            rxd.rxdes0 |= FTMAC100_RXDES0_FRS;

            if (len > 1518) {
                rxd.rxdes0 |= FTMAC100_RXDES0_FTL;
            } else if (len < 64)   {
                rxd.rxdes0 |= FTMAC100_RXDES0_RUNT;
            }
        }

        if (size == len) {
            rxd.rxdes0 |= FTMAC100_RXDES0_LRS;
        }

        /* copy frame into rxd.buf */
        dma_memory_write(s->dma, rxd.rxdes2, ptr, len);
        ptr  += len;
        size -= len;

    /*fprintf(stderr, "ftmac100 rx: %" HWADDR_PRIx " 0=0x%08" PRIx32 ", 1=0x%08" PRIx32 "\n", addr, rxd.rxdes0, rxd.rxdes1);*/

        /* write-back the rx descriptor */
        ftmac100_put_rxdesc(s, &rxd, addr);

        /* advance the descriptor index */
        if (le32_to_cpu(rxd.rxdes1) & FTMAC100_RXDES1_EDORR) {
            s->rxi = 0;
        } else
            s->rxi++;
    }

    regs->isr |= FTMAC100_ISR_RXFIFO | FTMAC100_ISR_RX;

    if (!(maccr & FTMAC100_MACCR_LOOP)) {
        ftmac100_update_irq(s);
    }

    return (ssize_t)((ptrdiff_t)ptr - (ptrdiff_t)buf);
}

static void ftmac100_transmit(Ftmac100State *s)
{
    hwaddr addr;
    uint8_t *buf;
    Ftmac100TxDesc txd;
    Ftmac100Regs *regs = (Ftmac100Regs *)s->regs;
    uint32_t len, maccr = regs->maccr;

    if (!(maccr & FTMAC100_MACCR_TX) || !(maccr & FTMAC100_MACCR_TXDMA)) {
        return;
    }

    do {
        addr = (regs->txbar & ~0xf) + (s->txi * sizeof(Ftmac100TxDesc));

        ftmac100_get_txdesc(s, &txd, addr);

    /*fprintf(stderr, "ftmac100 tx: %" HWADDR_PRIx " 0=0x%08" PRIx32 ", 1=0x%08" PRIx32 "\n", addr, txd.txdes0, txd.txdes1);*/

        if (!(txd.txdes0 & FTMAC100_TXDES0_TXDMA_OWN)) {
            regs->isr |= FTMAC100_ISR_NTB;
            break;
        }

        len = FTMAC100_TXDES1_TXBUF_SIZE(txd.txdes1);

        if (txd.txdes1 & FTMAC100_TXDES1_FTS) {
            s->buf.len = 0;
        }

        if (len + s->buf.len > FTMAC100_FRMAX) {
            fprintf(stderr, "ftmac100: tx buffer overflow!\n");
            abort();
        }

        buf = s->buf.dat + s->buf.len;
        dma_memory_read(s->dma, txd.txdes2, buf, len);
        s->buf.len += len;

        if (txd.txdes1 & FTMAC100_TXDES1_LTS) {
            s->stats.tx_pkts++;
            if (maccr & FTMAC100_MACCR_LOOP) {
                ftmac100_receive(qemu_get_queue(s->nic), buf, len);
            } else {
                qemu_send_packet(qemu_get_queue(s->nic), s->buf.dat, s->buf.len);
            }
        }

        if (txd.txdes1 & FTMAC100_TXDES1_TX2FIC) {
            regs->isr |= FTMAC100_ISR_TXFIFO;
        }

        if (txd.txdes1 & FTMAC100_TXDES1_TXIC) {
            regs->isr |= FTMAC100_ISR_TX;
        }

        /* update txd */
        txd.txdes0 &= ~FTMAC100_TXDES0_TXDMA_OWN;

    /*fprintf(stderr, "ftmac100 tx: %" HWADDR_PRIx " 0=0x%08" PRIx32 ", 1=0x%08" PRIx32 "\n", addr, txd.txdes0, txd.txdes1);*/

        ftmac100_put_txdesc(s, &txd, addr);

        if (le32_to_cpu(txd.txdes1) & FTMAC100_TXDES1_EDOTR) {
            s->txi = 0;
        } else {
            s->txi++;
        }
    } while (1);

    /* update interrupt signal */
    ftmac100_update_irq(s);
}

static void ftmac100_bh(void *opaque)
{
    Ftmac100State *s = FTMAC100(opaque);

    /* process tx ring */
    ftmac100_transmit(s);
}

static void ftmac100_chip_reset(Ftmac100State *s)
{
    memset(s->regs, 0, sizeof(s->regs));

    s->txi = 0;
    s->rxi = 0;

    if (s->bh) {
        qemu_bh_cancel(s->bh);
    }

    if (s->qtimer) {
        qemu_del_timer(s->qtimer);
    }

    ftmac100_update_irq(s);
}

static uint64_t ftmac100_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    uint32_t ret = 0;
    Ftmac100State *s = FTMAC100(opaque);

    if (addr >= FTMAC100_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac100: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftmac100Regs, isr):
        ret = s->regs[addr >> 2];
        s->regs[addr >> 2] = 0;
        ftmac100_update_irq(s);
        break;
    case offsetof(Ftmac100Regs, mac):
        ret = s->conf.macaddr.a[1] | (s->conf.macaddr.a[0] << 8);
        break;
    case offsetof(Ftmac100Regs, mac) + 4:
        ret = s->conf.macaddr.a[5] | (s->conf.macaddr.a[4] << 8)
            | (s->conf.macaddr.a[3] << 16) | (s->conf.macaddr.a[2] << 24);
        break;
    case offsetof(Ftmac100Regs, txpkt):
        ret = s->stats.tx_pkts;
        break;
    case offsetof(Ftmac100Regs, rxpkt):
        ret = s->stats.rx_pkts;
        break;
    case offsetof(Ftmac100Regs, rxbcst):
        ret = s->stats.rx_bcst;
        break;
    case offsetof(Ftmac100Regs, rxmcst):
        ret = s->stats.rx_mcst;
        break;
    case offsetof(Ftmac100Regs, revr):
        ret = FTMAC100_REVISION;
        break;
    case offsetof(Ftmac100Regs, fear):
        ret = FTMAC100_FEAR_HD | FTMAC100_FEAR_FD | FTMAC100_FEAR_WOL;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

/*fprintf(stderr, "ftmac100 r: 0x%02" HWADDR_PRIx " val=0x%08" PRIx32 "\n", addr, ret);*/

    return ret;
}

static void ftmac100_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftmac100State *s = FTMAC100(opaque);
    Ftmac100Regs *regs = (Ftmac100Regs *)s->regs;
    /*int i;*/
    if (addr >= FTMAC100_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftmac100: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

/*fprintf(stderr, "ftmac100 w: 0x%02" HWADDR_PRIx " val=0x%016" PRIx64 "\n", addr, val);*/

    switch (addr) {
    case offsetof(Ftmac100Regs, ier):
        regs->ier = (uint32_t)val;
        ftmac100_update_irq(s);
        break;
    case offsetof(Ftmac100Regs, mac):
        s->conf.macaddr.a[1] = extract32((uint32_t)val, 0, 8);
        s->conf.macaddr.a[0] = extract32((uint32_t)val, 8, 8);
        break;
    case offsetof(Ftmac100Regs, mac) + 4:
        s->conf.macaddr.a[5] = extract32((uint32_t)val, 0, 8);
        s->conf.macaddr.a[4] = extract32((uint32_t)val, 8, 8);
        s->conf.macaddr.a[3] = extract32((uint32_t)val, 16, 8);
        s->conf.macaddr.a[2] = extract32((uint32_t)val, 24, 8);
        break;
    case offsetof(Ftmac100Regs, maccr):
        if (val & FTMAC100_MACCR_RESET) {
            val = 0;
            ftmac100_chip_reset(s);
        }
        regs->maccr = (uint32_t)val;
        if ((val & FTMAC100_MACCR_RX) && (val & FTMAC100_MACCR_RXDMA)) {
            if (ftmac100_can_receive(qemu_get_queue(s->nic))) {
                qemu_flush_queued_packets(qemu_get_queue(s->nic));
            }
        } else {
            qemu_del_timer(s->qtimer);
        }
        break;
    case offsetof(Ftmac100Regs, macsr):
        regs->macsr &= ~((uint32_t)val);
        break;
    case offsetof(Ftmac100Regs, phycr):
        /* Drop it, if PHY ID != 1 */
        if (FTMAC100_PHYCR_DEV(val) == 1 && (val & FTMAC100_PHYCR_READ)) {
            val &= 0xffff0000;
            val |= miiphy_regs[FTMAC100_PHYCR_REG(val)];
        } else {
            if ((regs->maccr & FTMAC100_MACCR_LOOP) && regs->phydr & BMCR_LOOPBACK) {
                /**/
            }
            val |= 0x0000ffff;
        }
        val &= ~(FTMAC100_PHYCR_WRITE | FTMAC100_PHYCR_READ);
        regs->phycr = (uint32_t)val;
        break;
    case offsetof(Ftmac100Regs, txpd):
        ftmac100_transmit(s);
        /*qemu_bh_schedule(s->bh);*/
        break;
    /*case offsetof(Ftmac100Regs, txbar):
        s->regs[addr >> 2] = (uint32_t)val;

        Ftmac100TxDesc txd;
        i = 0;
        do {
            hwaddr addr = (val & ~0xf) + (i * sizeof(Ftmac100TxDesc));
            ftmac100_get_txdesc(s, &txd, addr);
            
            if (txd.txdes1 & FTMAC100_TXDES1_EDOTR) {
                break;
            }
        } while (++i);

        fprintf(stderr, "%d\n\n", i);

        break;
    case offsetof(Ftmac100Regs, rxbar):
        s->regs[addr >> 2] = (uint32_t)val;

        Ftmac100RxDesc rxd;
        i = 0;
        do {
            hwaddr addr = (val & ~0xf) + (i * sizeof(Ftmac100TxDesc));
            ftmac100_get_rxdesc(s, &rxd, addr);
            
            if (rxd.rxdes1 & FTMAC100_RXDES1_EDORR) {
                break;
            }
        } while (++i);

        fprintf(stderr, "%d\n\n", i);

        break;*/
    /* read-only register */
    case offsetof(Ftmac100Regs, isr):
    case offsetof(Ftmac100Regs, revr):
    case offsetof(Ftmac100Regs, fear):
    case offsetof(Ftmac100Regs, rsvd1) ... FTMAC100_REG_SIZE - 4:
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftmac100_mmio_read,
    .write = ftmac100_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftmac100_cleanup(NetClientState *nc)
{
    Ftmac100State *s = qemu_get_nic_opaque(nc);

    s->nic = NULL;
}

static NetClientInfo net_ftmac100_info = {
    .type = NET_CLIENT_OPTIONS_KIND_NIC,
    .size = sizeof(NICState),
    .can_receive = ftmac100_can_receive,
    .receive = ftmac100_receive,
    .cleanup = ftmac100_cleanup,
};

static void ftmac100_reset(DeviceState *dev)
{
    ftmac100_chip_reset(FTMAC100(dev));
}

static void ftmac100_timer(void *opaque)
{
    Ftmac100State *s = FTMAC100(opaque);

    if (ftmac100_can_receive(qemu_get_queue(s->nic))) {
        qemu_flush_queued_packets(qemu_get_queue(s->nic));
    }
}

static void ftmac100_realize(DeviceState *dev, Error **errp)
{
    Ftmac100State *s = FTMAC100(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->mmio,
                          &mmio_ops,
                          s,
                          TYPE_FTMAC100,
                          FTMAC100_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);

    qemu_macaddr_default_if_unset(&s->conf.macaddr);
    s->nic = qemu_new_nic(&net_ftmac100_info,
                          &s->conf,
                          object_get_typename(OBJECT(dev)),
                          sbd->qdev.id,
                          s);
    qemu_format_nic_info_str(qemu_get_queue(s->nic), s->conf.macaddr.a);

    s->qtimer = qemu_new_timer_ms(vm_clock, ftmac100_timer, s);
    s->dma = &dma_context_memory;
    s->bh = qemu_bh_new(ftmac100_bh, s);

    ftmac100_chip_reset(s);
}

static const VMStateDescription vmstate_ftmac100 = {
    .name = TYPE_FTMAC100,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftmac100State, FTMAC100_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static Property ftmac100_properties[] = {
    DEFINE_NIC_PROPERTIES(Ftmac100State, conf),
    DEFINE_PROP_END_OF_LIST(),
};

static void ftmac100_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd  = &vmstate_ftmac100;
    dc->props = ftmac100_properties;
    dc->reset = ftmac100_reset;
    dc->realize = ftmac100_realize;
    dc->no_user = 1;
}

static const TypeInfo ftmac100_info = {
    .name           = TYPE_FTMAC100,
    .parent         = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(Ftmac100State),
    .class_init     = ftmac100_class_init,
};

static void ftmac100_register_types(void)
{
    type_register_static(&ftmac100_info);
}

/* Legacy helper function.  Should go away when machine config files are
   implemented.  */
void ftmac100_init(NICInfo *nd, uint32_t base, qemu_irq irq)
{
    DeviceState *dev;
    SysBusDevice *s;

    qemu_check_nic_model(nd, TYPE_FTMAC100);
    dev = qdev_create(NULL, TYPE_FTMAC100);
    qdev_set_nic_properties(dev, nd);
    qdev_init_nofail(dev);
    s = SYS_BUS_DEVICE(dev);
    sysbus_mmio_map(s, 0, base);
    sysbus_connect_irq(s, 0, irq);
}

type_init(ftmac100_register_types)
