/*
 * Faraday FTGMAC100 Gigabit Ethernet Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#ifndef HW_NET_FTGMAC100_H
#define HW_NET_FTGMAC100_H

typedef struct Ftgmac100Regs {
    uint32_t isr;      /* 0x00: Interrups Status Register */
    uint32_t imr;      /* 0x04: Interrupt Mask Register */
    uint32_t mac[2];   /* 0x08: MAC Address */
    uint32_t mht[2];   /* 0x10: Multicast Hash Table 0 Register */
    uint32_t txpd;     /* 0x18: Tx Poll Demand Register */
    uint32_t rxpd;     /* 0x1c: Rx Poll Demand register */
    uint32_t txbar;    /* 0x20: Tx Ring Base Address Register */
    uint32_t rxbar;    /* 0x24: Rx Ring Base Address Register */
    uint32_t hptxpd;   /* 0x28: High-Priority Tx Poll Demand */
    uint32_t hptxbar;  /* 0x2C: High-Priority Tx Ring Base Address */
    uint32_t itc;      /* 0x30: Interrupt Timer Control Register */
    uint32_t aptc;     /* 0x34: Automatic Polling Timer Control Register */
    uint32_t dblac;    /* 0x38: DMA Burst Length&Arbitration Control */
    uint32_t dmasr;    /* 0x3C: DMA/FIFO Status Register */
    uint32_t revr;     /* 0x40: Revision Register */
    uint32_t fear;     /* 0x44: Feature Register */
    uint32_t tpafcr;   /* 0x48: Tx Priority Arbitration & FIFO Control */
    uint32_t rbsr;     /* 0x4C: Rx Buffer Size Register */
    uint32_t maccr;    /* 0x50: MAC Control Register */
    uint32_t macsr;    /* 0x54: MAC Status Register */
    uint32_t tstmr;    /* 0x58: Test Mode Register */
    uint32_t rsvd0;
    uint32_t phycr;    /* 0x60: PHY Control Register */
    uint32_t phydr;    /* 0x64: PHY Data Register */
    uint32_t fcr;      /* 0x68: Flow Control Register */
    uint32_t bpr;      /* 0x6C: Back Pressure Register */
    uint32_t wolcr;    /* 0x70: Wake-On-LAN Control Register */
    uint32_t wolsr;    /* 0x74: Wake-On-LAN Status Register */
    uint32_t wfcrc;    /* 0x78: Wake-up Frame CRC Register */
    uint32_t rsvd1;
    uint32_t wfbm[4];  /* 0x80: Wake-up Frame Mask Register */
    uint32_t txptr;    /* 0x90: Tx Pointer Register */
    uint32_t hptxptr;  /* 0x94: High-Priority Tx Pointer Register */
    uint32_t rxptr;    /* 0x98: Rx Pointer Register */
    uint32_t rsvd2;
    uint32_t txpkt;    /* 0xa0: Tx Packet Counter Register */
    uint32_t txerrs[3];/* 0xa4: Tx Error Statistics Register */
    uint32_t rxpkt;    /* 0xb0: Rx Packet Counter Register */
    uint32_t rxbcst;   /* 0xb4: Rx Boradcast Counter Register */
    uint32_t rxmcst;   /* 0xb8: Rx Multicast Counter Register */
    uint32_t rxstat[4];/* 0xb0: Rx Error Statistics Register */
} Ftgmac100Regs;

#define FTGMAC100_REG_SIZE      sizeof(Ftgmac100Regs)

/* Interrupt status register */
#define FTGMAC100_ISR_NHPTB     (1 << 10) /* out of high priority tx buffer */
#define FTGMAC100_ISR_PHYST     (1 << 9)  /* phy status change */
#define FTGMAC100_ISR_BUS       (1 << 8)  /* bus error */
#define FTGMAC100_ISR_TXLOST    (1 << 7)  /* tx packet lost */
#define FTGMAC100_ISR_NTB       (1 << 6)  /* out of tx buffer */
#define FTGMAC100_ISR_TXFIFO    (1 << 5)  /* tx fifo */
#define FTGMAC100_ISR_TX        (1 << 4)  /* tx compelete */
#define FTGMAC100_ISR_RXLOST    (1 << 3)  /* rx packet lost */
#define FTGMAC100_ISR_NRB       (1 << 2)  /* out of rx buffer */
#define FTGMAC100_ISR_RXFIFO    (1 << 1)  /* rx fifo */
#define FTGMAC100_ISR_RX        (1 << 0)  /* rx compelete */

/* Feature register */
#define FTGMAC100_FEAR_TFSZ(x)  ((((x) >> 12) & 7) << 3) /* Tx FIFO size */
#define FTGMAC100_FEAR_RFSZ(x)  ((((x) >> 12) & 7) << 0) /* Rx FIFO size */

/* MAC control register */
#define FTGMAC100_MACCR_RESET   (1 << 31) /* software reset */
#define FTGMAC100_MACCR_100M    (1 << 19) /* 100Mbps */
#define FTGMAC100_MACCR_CRCDIS  (1 << 18) /* discard crc error */
#define FTGMAC100_MACCR_RXBCST  (1 << 17) /* recv all broadcast packets */
#define FTGMAC100_MACCR_RXMCST  (1 << 16) /* recv all multicast packets */
#define FTGMAC100_MACCR_RXMH    (1 << 15) /* recv multicast by hash */
#define FTGMAC100_MACCR_RXALL   (1 << 14) /* recv all packets */
#define FTGMAC100_MACCR_JUMBO   (1 << 13) /* support jumbo frame */
#define FTGMAC100_MACCR_RXRUNT  (1 << 12) /* recv runt packets */
#define FTGMAC100_MACCR_CRCAPD  (1 << 10) /* crc append */
#define FTGMAC100_MACCR_GIGA    (1 << 9)  /* giga mode */
#define FTGMAC100_MACCR_FD      (1 << 8)  /* full duplex */
#define FTGMAC100_MACCR_HD_RT   (1 << 7)  /* rx while tx in half duplex */
#define FTGMAC100_MACCR_LOOP    (1 << 6)  /* loopback */
#define FTGMAC100_MACCR_HPTX    (1 << 5)  /* high priority tx enabled */
#define FTGMAC100_MACCR_VLANRM  (1 << 4)  /* vlan removal enabled */
#define FTGMAC100_MACCR_RX      (1 << 3)  /* rx engine enabled */
#define FTGMAC100_MACCR_TX      (1 << 2)  /* tx engine enabled */
#define FTGMAC100_MACCR_RXDMA   (1 << 1)  /* rx dma enabled */
#define FTGMAC100_MACCR_TXDMA   (1 << 0)  /* tx dma enabled */

/* MDIO */
#define FTGMAC100_PHYCR_MDIOWR  (1 << 27) /* mdio write */
#define FTGMAC100_PHYCR_MDIORD  (1 << 26) /* mdio read */
#define FTGMAC100_PHYCR_REG(x)  extract32((uint32_t)(x), 21, 5)
#define FTGMAC100_PHYCR_DEV(x)  extract32((uint32_t)(x), 16, 5)

/* Tx/Rx Descriptors */
typedef struct Ftgmac100Desc {
    uint64_t ctrl;
    uint32_t rsvd;
    uint32_t buff;
} Ftgmac100Desc;

#define FTGMAC100_RXD_IPCS      ((uint64_t)1 << 59) /* IP crc err */
#define FTGMAC100_RXD_UDPCS     ((uint64_t)1 << 58) /* UDP crc err */
#define FTGMAC100_RXD_TCPCS     ((uint64_t)1 << 57) /* TCP crc err */
#define FTGMAC100_RXD_VLAN      ((uint64_t)1 << 56) /* VLAN tagged */
#define FTGMAC100_RXD_FRAG      ((uint64_t)1 << 55) /* Fragmented */
#define FTGMAC100_RXD_LLC       ((uint64_t)1 << 54) /* LLC packet */
#define FTGMAC100_RXD_PROTO_NIP ((uint64_t)0 << 52) /* no ip */
#define FTGMAC100_RXD_PROTO_IP  ((uint64_t)1 << 52)
#define FTGMAC100_RXD_PROTO_TCP ((uint64_t)2 << 52)
#define FTGMAC100_RXD_PROTO_UDP ((uint64_t)3 << 52)
#define FTGMAC100_RXD_VTAG(x)   (((x) >> 32) & 0xffff)

#define FTGMAC100_RXD_OWNER_SW  ((uint64_t)1 << 31) /* owner: 1=SW, 0=HW */
#define FTGMAC100_RXD_FRS       ((uint64_t)1 << 29) /* first pkt desc */
#define FTGMAC100_RXD_LRS       ((uint64_t)1 << 28) /* last pkt desc */
#define FTGMAC100_RXD_PF        ((uint64_t)1 << 25) /* pause frame */
#define FTGMAC100_RXD_PFOPC     ((uint64_t)1 << 24) /* pause OP code */
#define FTGMAC100_RXD_FF        ((uint64_t)1 << 23) /* fifo full */
#define FTGMAC100_RXD_ONB       ((uint64_t)1 << 22) /* odd nibble */
#define FTGMAC100_RXD_RUNT      ((uint64_t)1 << 21) /* runt pkt */
#define FTGMAC100_RXD_FTL       ((uint64_t)1 << 20) /* frame too long */
#define FTGMAC100_RXD_CRC       ((uint64_t)1 << 19) /* pkt crc error */
#define FTGMAC100_RXD_BUS       ((uint64_t)1 << 18) /* bus error */
#define FTGMAC100_RXD_ERRMASK   ((uint64_t)0x1f << 18) /* all errors */
#define FTGMAC100_RXD_BCST      ((uint64_t)1 << 17) /* Bcst pkt */
#define FTGMAC100_RXD_MCST      ((uint64_t)1 << 16) /* Mcst pkt */
#define FTGMAC100_RXD_END       ((uint64_t)1 << 15) /* end of ring */
#define FTGMAC100_RXD_LEN(x)    ((x) & 0x3fff)
#define FTGMAC100_RXD_LEN_MASK  0x3fff
#define FTGMAC100_RXD_LEN_SHIFT 0

#define FTGMAC100_RXD_CLRMASK   (FTGMAC100_RXD_END)

#define FTGMAC100_TXD_TXIC      ((uint64_t)1 << 63) /* Tx IRQ */
#define FTGMAC100_TXD_TX2FIC    ((uint64_t)1 << 62) /* Tx FIFO IRQ */
#define FTGMAC100_TXD_LLC       ((uint64_t)1 << 54) /* LLC packet */
#define FTGMAC100_TXD_IPCS      ((uint64_t)1 << 51) /* IP offload */
#define FTGMAC100_TXD_UDPCS     ((uint64_t)1 << 50) /* UDP offload */
#define FTGMAC100_TXD_TCPCS     ((uint64_t)1 << 49) /* TCP offload */
#define FTGMAC100_TXD_VINS      ((uint64_t)1 << 48) /* VLAN TAG insert */
#define FTGMAC100_TXD_VTAG(x)   (((x) >> 32) & 0xffff)

#define FTGMAC100_TXD_OWNER_HW  ((uint64_t)1 << 31) /* owner: 1=HW, 0=SW */
#define FTGMAC100_TXD_FTS       ((uint64_t)1 << 29) /* first pkt desc */
#define FTGMAC100_TXD_LTS       ((uint64_t)1 << 28) /* last pkt desc */
#define FTGMAC100_TXD_CRC       ((uint64_t)1 << 19) /* crc error */
#define FTGMAC100_TXD_END       ((uint64_t)1 << 15) /* end of ring */
#define FTGMAC100_TXD_LEN(x)    ((x) & 0x3fff)
#define FTGMAC100_TXD_LEN_MASK  0x3fff
#define FTGMAC100_TXD_LEN_SHIFT 0

#define FTGMAC100_TXD_CLRMASK   (~(FTGMAC100_TXD_CRC | FTGMAC100_TXD_OWNER_HW))

#endif /* HW_NET_FTGMAC100_H */
