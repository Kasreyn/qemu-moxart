/*
 * QEMU model of Faraday FTWDT010 WatchDog Timer
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "sysemu/sysemu.h"
#include "sysemu/watchdog.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"

#define FTWDT010_REVID          0x10601 /* rev. 1.6.1 */

#ifdef CONFIG_MOXART
#undef FTWDT010_REVID
#define FTWDT010_REVID          FARADAY_REVISION(1, 3, 0)
#endif

/* Hardware registers */
#define FTWDT010_REG_COUNTER    0x00    /* counter register */
#define FTWDT010_REG_LOAD       0x04    /* (re)load register */
#define FTWDT010_REG_RESTART    0x08    /* restart register */
#define FTWDT010_REG_CR         0x0C    /* control register */
#define FTWDT010_REG_SR         0x10    /* status register */
#define FTWDT010_REG_SCR        0x14    /* status clear register */
#define FTWDT010_REG_INTR_LEN   0x18    /* interrupt length register */
#define FTWDT010_REG_REVR       0x1C    /* revision register */

#define RESTART_MAGIC   0x5ab9  /* magic for watchdog restart */

#define CR_CLKS         BIT(4)  /* clock source */
#define CR_ESIG         BIT(3)  /* external signal enabled */
#define CR_INTR         BIT(2)  /* system reset interrupt enabled */
#define CR_SRST         BIT(1)  /* system reset enabled */
#define CR_EN           BIT(0)  /* chip enabled */

#define SR_SRST         BIT(1)  /* system reset */

#define TYPE_FTWDT010   "ftwdt010"

typedef struct Ftwdt010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    qemu_irq irq;
    QEMUTimer *qtimer;

    uint64_t timeout;
    uint64_t freq;  /* emulated clock rate */
    uint64_t delta; /* get_ticks_per_sec() / freq */

    /* hw registers */
    uint32_t load;
    uint32_t cr;
    uint32_t sr;
} Ftwdt010State;

#define FTWDT010(obj) \
    OBJECT_CHECK(Ftwdt010State, obj, TYPE_FTWDT010)

static void ftwdt010_restart(Ftwdt010State *s)
{
    s->timeout = (s->delta * (uint64_t)s->load) / 1000000ULL;
    s->timeout = qemu_get_clock_ms(rt_clock) + MAX(s->timeout, 1);
    qemu_mod_timer(s->qtimer, s->timeout);
}

static uint64_t ftwdt010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftwdt010State *s = FTWDT010(opaque);
    uint64_t now = qemu_get_clock_ms(rt_clock);
    uint32_t ret = 0;

    switch (addr) {
    case FTWDT010_REG_COUNTER:
        if (s->cr & CR_EN) {
            ret = (s->timeout > now) ? (s->timeout - now) : 0;
            ret = ret * 1000000ULL / s->delta;
        } else {
            ret = s->load;
        }
        break;
    case FTWDT010_REG_LOAD:
        return s->load;
    case FTWDT010_REG_CR:
        return s->cr;
    case FTWDT010_REG_SR:
        return s->sr;
    case FTWDT010_REG_REVR:
        return FTWDT010_REVID;
    default:
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftwdt010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        break;
    }

    return ret;
}

static void ftwdt010_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftwdt010State *s = FTWDT010(opaque);

    switch (addr) {
    case FTWDT010_REG_LOAD:
        s->load = (uint32_t)val;
        break;
    case FTWDT010_REG_RESTART:
        if ((s->cr & CR_EN) && (val == RESTART_MAGIC)) {
            ftwdt010_restart(s);
        }
        break;
    case FTWDT010_REG_CR:
        if (!(s->cr & CR_EN) && (val & CR_EN)) {
            ftwdt010_restart(s);
        } else if ((s->cr & CR_EN) && !(val & CR_EN)) {
            qemu_del_timer(s->qtimer);
        }
        s->cr = (uint32_t)val;
        break;
    case FTWDT010_REG_SCR:
        s->sr &= ~(uint32_t)val;
        break;
    default:
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftwdt010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftwdt010_mmio_read,
    .write = ftwdt010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftwdt010_timer(void *opaque)
{
    Ftwdt010State *s = FTWDT010(opaque);

    s->sr = SR_SRST;

    /* send interrupt signal */
    qemu_set_irq(s->irq, !!(s->cr & CR_INTR));

    /* send system reset */
    if (s->cr & CR_SRST) {
        watchdog_perform_action();
    }
}

static void ftwdt010_reset(DeviceState *dev)
{
    Ftwdt010State *s = FTWDT010(dev);

    s->cr      = 0;
    s->sr      = 0;
    s->load    = 0x3ef1480;
    s->timeout = 0;
}

static void ftwdt010_realize(DeviceState *dev, Error **errp)
{
    Ftwdt010State *s = FTWDT010(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    s->delta = (uint64_t)get_ticks_per_sec() / s->freq;
    s->qtimer = qemu_new_timer_ms(rt_clock, ftwdt010_timer, s);

    memory_region_init_io(&s->mmio,
                          &mmio_ops,
                          s,
                          TYPE_FTWDT010,
                          0x20);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);
}

static const VMStateDescription vmstate_ftwdt010 = {
    .name = TYPE_FTWDT010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT64(timeout, Ftwdt010State),
        VMSTATE_UINT64(freq, Ftwdt010State),
        VMSTATE_UINT64(delta, Ftwdt010State),
        VMSTATE_UINT32(load, Ftwdt010State),
        VMSTATE_UINT32(cr, Ftwdt010State),
        VMSTATE_UINT32(sr, Ftwdt010State),
        VMSTATE_END_OF_LIST()
    }
};

static Property ftwdt010_properties[] = {
#ifdef CONFIG_MOXART
    DEFINE_PROP_UINT32("freq", Fttmr010State, freq, 48000000ULL),
#else
    DEFINE_PROP_UINT64("freq", Ftwdt010State, freq, 66000000ULL),
#endif
    DEFINE_PROP_END_OF_LIST(),
};

static void ftwdt010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftwdt010;
    dc->props   = ftwdt010_properties;
    dc->reset   = ftwdt010_reset;
    dc->realize = ftwdt010_realize;
    dc->no_user = 1;
}

static const TypeInfo ftwdt010_info = {
    .name           = TYPE_FTWDT010,
    .parent         = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(Ftwdt010State),
    .class_init     = ftwdt010_class_init,
};

static void ftwdt010_register_types(void)
{
    type_register_static(&ftwdt010_info);
}

type_init(ftwdt010_register_types)
