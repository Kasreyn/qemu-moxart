/*
 * Faraday FTSDC010 MMC/SD Host Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sd.h"
#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"
#include "sysemu/blockdev.h"
#include "qemu/bitops.h"

#define TYPE_FTSDC010       "ftsdc010"
#define FTSDC010_REVISION   FARADAY_REVISION(3, 3, 0)

#ifdef CONFIG_MOXART
#undef FTSDC010_REVISION
#define FTSDC010_REVISION   FARADAY_REVISION(1, 6, 0)
#endif

typedef struct Ftsdc010Regs {
    uint32_t cmd;       /* 0x00 - command register */
    uint32_t arg;       /* 0x04 - argument register */
    uint32_t rsp[4];    /* 0x08 - response register */
    uint32_t rspcmd;    /* 0x18 - responded cmd register */
    uint32_t dcr;       /* 0x1c - data control register */
    uint32_t dtr;       /* 0x20 - data timer register */
    uint32_t dlr;       /* 0x24 - data length register */
    uint32_t sr;        /* 0x28 - status register */
    uint32_t scr;       /* 0x2c - status clear register */
    uint32_t imr;       /* 0x30 - interrupt mask register */
    uint32_t pcr;       /* 0x34 - power control register */
    uint32_t ccr;       /* 0x38 - clock contorl register */
    uint32_t bwr;       /* 0x3c - bus width register */
    uint32_t dr;        /* 0x40 - data register */
    uint32_t mmc_irtr;  /* 0x44 - MMC intr resp time register */
    uint32_t gpo;       /* 0x48 - gerenal purpose output */
    uint32_t rsvd0[8];  /* 0x50 - 0x68 reserved */
    uint32_t sdio_cr[2];/* 0x6c - SDIO control register */
    uint32_t sdio_sr;   /* 0x74 - SDIO status register */
    uint32_t rsvd1[9];  /* 0x78 - 0x98 reserved */
    uint32_t fear;      /* 0x9c - feature register */
    uint32_t revr;      /* 0xa0 - revision register */
} Ftsdc010Regs;

#define FTSDC010_REG_SIZE       sizeof(Ftsdc010Regs)

/* command register */
#define FTSDC010_CMD_CODE(x)    ((x) & 0x3f)
#define FTSDC010_CMD_WAIT_RSP   0x00000040
#define FTSDC010_CMD_LONG_RSP   0x00000080
#define FTSDC010_CMD_APP        0x00000100
#define FTSDC010_CMD_EN         0x00000200
#define FTSDC010_CMD_RESET      0x00000400

/* responsed command register */
#define FTSDC010_RSPCMD_SET(x)  ((x) & 0x13f)

/* data control register */
#define FTSDC010_DCR_BKSZ       0x0000000F
#define FTSDC010_DCR_WR         0x00000010
#define FTSDC010_DCR_RD         0x00000000
#define FTSDC010_DCR_DMA        0x00000020
#define FTSDC010_DCR_EN         0x00000040
#define FTSDC010_DCR_THRES      0x00000080
#define FTSDC010_DCR_BURST1     0x00000000
#define FTSDC010_DCR_BURST4     0x00000100
#define FTSDC010_DCR_BURST8     0x00000200
#define FTSDC010_DCR_RESET      0x00000400

/* status register */
#define FTSDC010_SR_RSP_CRC     0x00000001
#define FTSDC010_SR_DAT_CRC     0x00000002
#define FTSDC010_SR_RSP_TIMEOUT 0x00000004
#define FTSDC010_SR_DAT_TIMEOUT 0x00000008
#define FTSDC010_SR_RSP_ERROR   (FTSDC010_SR_RSP_CRC | FTSDC010_SR_RSP_TIMEOUT)
#define FTSDC010_SR_DAT_ERROR   (FTSDC010_SR_DAT_CRC | FTSDC010_SR_DAT_TIMEOUT)
#define FTSDC010_SR_RSP         0x00000010
#define FTSDC010_SR_DAT         0x00000020
#define FTSDC010_SR_CMD         0x00000040
#define FTSDC010_SR_DAT_END     0x00000080
#define FTSDC010_SR_TXRDY       0x00000100
#define FTSDC010_SR_RXRDY       0x00000200
#define FTSDC010_SR_CARD_CH     0x00000400
#define FTSDC010_SR_CARD_RM     0x00000800
#define FTSDC010_SR_CARD_WP       0x00001000
#define FTSDC010_SR_SDIO        0x00010000
#define FTSDC010_SR_DAT0        0x00020000

/* clock control register */
#define FTSDC010_CCR_HISPD      0x00000200
#define FTSDC010_CCR_OFF        0x00000100
#define FTSDC010_CCR_SD         0x00000080

/* bus register */
#define FTSDC010_BWR_CARD_DAT3  0x00000020
#define FTSDC010_BWR_MODE_4BITS 0x00000008
#define FTSDC010_BWR_MODE_8BITS 0x00000010
#define FTSDC010_BWR_SUPP_4BITS 0x00000004
#define FTSDC010_BWR_SUPP_8BITS 0x00000002
#define FTSDC010_BWR_SUPP_1BITS 0x00000001

/* feature register */
#define FTSDC010_FEAR_SUPP_CPRM 0x00000100
#define FTSDC010_FEAR_FIFO(x)   ((x) & 0xff)

typedef struct Ftsdc010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    SDState *card;
    qemu_irq irq;

    int len;        /* length of data buffer */
    bool card_wp;   /* card: write protected */
    bool card_rm;   /* card: removed */
    qemu_irq card_wp_cb;
    qemu_irq card_rm_cb;

    /* DMA hardware handshake */
    qemu_irq req;

    /* hw registers */
    uint32_t regs[FTSDC010_REG_SIZE >> 2];
} Ftsdc010State;

#define FTSDC010(obj) \
    OBJECT_CHECK(Ftsdc010State, obj, TYPE_FTSDC010)

static void ftsdc010_irq_update(Ftsdc010State *s)
{
    Ftsdc010Regs *regs = (Ftsdc010Regs *)s->regs;
    uint32_t sr = regs->sr | FTSDC010_SR_TXRDY | FTSDC010_SR_RXRDY;

    qemu_set_irq(s->irq, !!(regs->imr & sr));
}

static void ftsdc010_card_wp(void *opaque, int line, int level)
{
    Ftsdc010State *s = FTSDC010(opaque);

    s->card_wp = !!level;
}

static void ftsdc010_card_rm(void *opaque, int line, int level)
{
    Ftsdc010State *s = FTSDC010(opaque);
    Ftsdc010Regs *regs = (Ftsdc010Regs *)s->regs;

    s->card_rm = !level;
    regs->sr |= FTSDC010_SR_CARD_CH;

    ftsdc010_irq_update(s);
}

static void ftsdc010_dmahs_ack(void *opaque, int line, int level)
{
    Ftsdc010State *s = FTSDC010(opaque);
    Ftsdc010Regs *regs = (Ftsdc010Regs *)s->regs;

    if (!(regs->dcr & FTSDC010_DCR_DMA)) {
        return;
    }

    if (level) {
        qemu_set_irq(s->req, 0);
    } else if (s->len > 0) {
        qemu_set_irq(s->req, 1);
    }
}

static void ftsdc010_send_command(Ftsdc010State *s)
{
    Ftsdc010Regs *regs = (Ftsdc010Regs *)s->regs;
    SDRequest request;
    uint8_t response[16];
    int rlen;


    regs->sr = 0;

    request.cmd = FTSDC010_CMD_CODE(regs->cmd);
    request.arg = regs->arg;

    rlen = sd_do_command(s->card, &request, response);

    regs->cmd &= ~FTSDC010_CMD_EN;

    if (rlen < 0) {
        goto error;
    }

    if (regs->cmd & FTSDC010_CMD_WAIT_RSP) {

        if (((regs->cmd & FTSDC010_CMD_LONG_RSP) && (rlen != 16))
            || (!(regs->cmd & FTSDC010_CMD_LONG_RSP) && (rlen != 4))) {
            goto error;
        }

#define RWORD(n) ((response[n] << 24) | (response[n + 1] << 16) \
                  | (response[n + 2] << 8) | response[n + 3])

        if (rlen == 4) {
            regs->rsp[0] = RWORD(0);
            regs->rsp[1] = 0;
            regs->rsp[2] = 0;
            regs->rsp[3] = 0;
        } else {
            regs->rsp[3] = RWORD(0);
            regs->rsp[2] = RWORD(4);
            regs->rsp[1] = RWORD(8);
            regs->rsp[0] = RWORD(12) & (~1);
        }
        regs->rspcmd = FTSDC010_RSPCMD_SET(regs->cmd);
        regs->sr |= FTSDC010_SR_RSP;

#undef RWORD

    } else {
        regs->sr |= FTSDC010_SR_CMD;
    }

    if ((regs->dcr & FTSDC010_DCR_DMA) && (s->len > 0)) {
        qemu_set_irq(s->req, 1);
    }

    return;

error:
    regs->sr |= FTSDC010_SR_RSP_TIMEOUT;
}

static void ftsdc010_chip_reset(Ftsdc010State *s)
{
    memset(s->regs, 0, FTSDC010_REG_SIZE);

    s->len = 0;
    s->card_wp = false;
    s->card_rm = true;

    /* Reload card status */
    sd_set_cb(s->card, s->card_wp_cb, s->card_rm_cb);

    /* Release DMA request */
    qemu_set_irq(s->req, 0);
}

static uint64_t ftsdc010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftsdc010State *s = FTSDC010(opaque);
    Ftsdc010Regs *regs = (Ftsdc010Regs *)s->regs;
    uint32_t i, ret = 0;

    if (addr >= FTSDC010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftsdc010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftsdc010Regs, sr):
        ret = regs->sr
            | FTSDC010_SR_TXRDY | FTSDC010_SR_RXRDY
            | (s->card_wp ? FTSDC010_SR_CARD_WP : 0)
            | (s->card_rm ? FTSDC010_SR_CARD_RM : 0);
        break;
    case offsetof(Ftsdc010Regs, dr):
        if (!(regs->dcr & FTSDC010_DCR_WR) && s->len) {
            for (i = 0; i < 4 && s->len; i++, s->len--) {
                ret = deposit32(ret, i * 8, 8, sd_read_data(s->card));
            }
            if (s->len <= 0) {
                regs->sr |= FTSDC010_SR_DAT_END;
            }
            regs->sr |= FTSDC010_SR_DAT;
            ftsdc010_irq_update(s);
        }
        break;
    case offsetof(Ftsdc010Regs, bwr):
        ret = regs->bwr | FTSDC010_BWR_SUPP_8BITS | FTSDC010_BWR_SUPP_4BITS
            | FTSDC010_BWR_SUPP_1BITS;
        break;
    case offsetof(Ftsdc010Regs, fear):
        ret = FTSDC010_FEAR_FIFO(16);
        break;
    case offsetof(Ftsdc010Regs, revr):
        ret = FTSDC010_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftsdc010_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftsdc010State *s = FTSDC010(opaque);
    Ftsdc010Regs *regs = (Ftsdc010Regs *)s->regs;
    int i;

    if (addr >= FTSDC010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftsdc010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftsdc010Regs, dr):
        if ((regs->dcr & FTSDC010_DCR_WR) && s->len) {
            for (i = 0; i < 4 && s->len; i++, s->len--) {
                sd_write_data(s->card, extract32((uint32_t)val, i * 8, 8));
            }
            if (s->len <= 0) {
                regs->sr |= FTSDC010_SR_DAT_END;
            }
            regs->sr |= FTSDC010_SR_DAT;
            ftsdc010_irq_update(s);
        }
        break;
    case offsetof(Ftsdc010Regs, cmd):
        regs->cmd = (uint32_t)val;
        if (val & FTSDC010_CMD_RESET) {
            ftsdc010_chip_reset(s);
        } else if (val & FTSDC010_CMD_EN) {
            ftsdc010_send_command(s);
        }
        break;
    case offsetof(Ftsdc010Regs, dcr):
        regs->dcr = (uint32_t)val;
        if (val & FTSDC010_DCR_EN) {
            regs->dcr &= ~(FTSDC010_DCR_EN);
            regs->sr &= ~(FTSDC010_SR_DAT | FTSDC010_SR_DAT_END
                | FTSDC010_SR_DAT_ERROR);
            s->len = (int)regs->dlr;
        }
        break;
    case offsetof(Ftsdc010Regs, scr):
        regs->sr &= (uint32_t)(~val);
        ftsdc010_irq_update(s);
        break;
    case offsetof(Ftsdc010Regs, imr):
        regs->imr = (uint32_t)val;
        ftsdc010_irq_update(s);
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftsdc010_mmio_read,
    .write = ftsdc010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftsdc010_reset(DeviceState *dev)
{
    ftsdc010_chip_reset(FTSDC010(dev));
}

static void ftsdc010_realize(DeviceState *dev, Error **errp)
{
    Ftsdc010State *s = FTSDC010(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    DriveInfo *dinfo;

    memory_region_init_io(&s->iomem, &mmio_ops, s,
        TYPE_FTSDC010, FTSDC010_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    sysbus_init_irq(sbd, &s->irq);

    qdev_init_gpio_in(&sbd->qdev, ftsdc010_dmahs_ack, 1);
    qdev_init_gpio_out(&sbd->qdev, &s->req, 1);

    dinfo = drive_get_next(IF_SD);
    s->card = sd_init(dinfo ? dinfo->bdrv : NULL, 0);
    s->card_wp_cb = qemu_allocate_irqs(ftsdc010_card_wp, s, 1)[0];
    s->card_rm_cb = qemu_allocate_irqs(ftsdc010_card_rm, s, 1)[0];
    sd_set_cb(s->card, s->card_wp_cb, s->card_rm_cb);
}

static const VMStateDescription vmstate_ftsdc010 = {
    .name = TYPE_FTSDC010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftsdc010State, FTSDC010_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static void ftsdc010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftsdc010;
    dc->reset   = ftsdc010_reset;
    dc->realize = ftsdc010_realize;
    dc->no_user = 1;
}

static const TypeInfo ftsdc010_info = {
    .name           = TYPE_FTSDC010,
    .parent         = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(Ftsdc010State),
    .class_init     = ftsdc010_class_init,
};

static void ftsdc010_register_types(void)
{
    type_register_static(&ftsdc010_info);
}

type_init(ftsdc010_register_types)
