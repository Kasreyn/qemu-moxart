/*
 * QEMU model of Faraday FTI2C010 I2C Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "hw/i2c/i2c.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"
#include "qemu/bitops.h"

#define FTI2C010_REVISION       FARADAY_REVISION(1, 10, 0)

/* FTI2C010 registers */
typedef struct Fti2c010Regs {
    uint32_t cr;  /* 0x00: control register */
    uint32_t sr;  /* 0x04: status register */
    uint32_t cdr; /* 0x08: clock division register */
    uint32_t dr;  /* 0x0c: data register */
    uint32_t sar; /* 0x10: slave address register */
    uint32_t tgsr;/* 0x14: time & glitch suppression register */
    uint32_t bmr; /* 0x18: bus monitor register */
    uint32_t rsvd[5];
    uint32_t revr;/* 0x30: revision register */
} Fti2c010Regs;

#define FTI2C010_REG_SIZE       sizeof(Fti2c010Regs)

/* FTI2C010 Control Register */
#define FTI2C010_CR_STARTIEN    0x4000  /* START Detected IRQ (slave) */
#define FTI2C010_CR_ALIEN       0x2000  /* Arbitration Lose IRQ (master) */
#define FTI2C010_CR_SAMIEN      0x1000  /* Slave Address Match IRQ (slave) */
#define FTI2C010_CR_STOPIEN     0x800   /* STOP Detected IRQ (slave) */
#define FTI2C010_CR_BERRIEN     0x400   /* NACK Resp IRQ (master) */
#define FTI2C010_CR_DRIEN       0x200   /* RX IRQ */
#define FTI2C010_CR_DTIEN       0x100   /* TX IRQ */
#define FTI2C010_CR_TBEN        0x80    /* Transfer Byte Enable */
#define FTI2C010_CR_NACK        0x40    /* Acknowledgement: 0=ACK, 1=NACK */
#define FTI2C010_CR_STOP        0x20    /* STOP (master) */
#define FTI2C010_CR_START       0x10    /* START (master) */
#define FTI2C010_CR_GCEN        0x8     /* General Call Message (slave) */
#define FTI2C010_CR_SCKEN       0x4     /* Clock Enable (master) */
#define FTI2C010_CR_I2CEN       0x2     /* I2C Enable */
#define FTI2C010_CR_I2CRST      0x1     /* I2C Reset */

#define FTI2C010_CR_SLAVE_MASK  \
    (FTI2C010_CR_GCEN | FTI2C010_CR_SAMIEN | FTI2C010_CR_STOPIEN \
    | FTI2C010_CR_STARTIEN)

#define FTI2C010_CR_MASTER_INTR \
    (FTI2C010_CR_ALIEN | FTI2C010_CR_BERRIEN | FTI2C010_CR_DRIEN \
    | FTI2C010_CR_DTIEN)

#define FTI2C010_CR_MASTER      \
    (FTI2C010_CR_SCKEN | FTI2C010_CR_I2CEN | FTI2C010_CR_TBEN)

/* FTI2C010 Status Register */
#define FTI2C010_SR_START       0x800   /* START Detected (RC) */
#define FTI2C010_SR_AL          0x400   /* Arbitration Lose (RC) */
#define FTI2C010_SR_GC          0x200   /* General Call Message (RC) */
#define FTI2C010_SR_SAM         0x100   /* Slave Address Match (RC) */
#define FTI2C010_SR_STOP        0x80    /* STOP Detected (RC) */
#define FTI2C010_SR_BERR        0x40    /* Bus Rrror (RC) */
#define FTI2C010_SR_DR          0x20    /* RX Ready (RC) */
#define FTI2C010_SR_DT          0x10    /* TX Transmit (RC) */
#define FTI2C010_SR_BB          0x8     /* I2C Bus Busy */
#define FTI2C010_SR_I2CB        0x4     /* I2C Chip Busy */
#define FTI2C010_SR_ACK         0x2
#define FTI2C010_SR_RW          0x1
#define FTI2C010_SR_RCMASK      0xff0   /* Read-Clear Mask */

/* FTI2C010 Time & Glitch Suppression Register */
#define FTI2C010_TGSR_TSR(x)    ((x) & 0x3f)         /* SETUP/HOLD time */
#define FTI2C010_TGSR_GSR(x)    (((x) & 0x07) << 10) /* Glitch Suppression */

#define I2C_RD  1
#define I2C_WR  0

#define TYPE_FTI2C010   "fti2c010"

typedef struct Fti2c010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;
    qemu_irq irq;
    i2c_bus *bus;
    bool rx;    /* RD = true; WR = false */

    /* hw registers */
    uint32_t regs[FTI2C010_REG_SIZE >> 2];
} Fti2c010State;

#define FTI2C010(obj) \
    OBJECT_CHECK(Fti2c010State, obj, TYPE_FTI2C010)

static void fti2c010_update_irq(Fti2c010State *s)
{
    Fti2c010Regs *regs = (Fti2c010Regs *)s->regs;

    uint32_t sr = extract32(regs->sr, 4, 8);
    uint32_t cr = extract32(regs->cr, 8, 8);

    qemu_set_irq(s->irq, !!(sr & cr));
}

static uint64_t fti2c010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Fti2c010State *s = FTI2C010(opaque);
    Fti2c010Regs *regs = (Fti2c010Regs *)s->regs;
    uint32_t ret = 0;

    if (addr >= FTI2C010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "fti2c010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(Fti2c010Regs, sr):
        ret = regs->sr | (i2c_bus_busy(s->bus) ? FTI2C010_SR_BB : 0);
        regs->sr &= ~FTI2C010_SR_RCMASK;
        fti2c010_update_irq(s);
        break;
    case offsetof(Fti2c010Regs, bmr):
        ret = 3;    /* SCL=1, SDA=1 */
        break;
    case offsetof(Fti2c010Regs, revr):
        ret = FTI2C010_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void fti2c010_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Fti2c010State *s = FTI2C010(opaque);
    Fti2c010Regs *regs = (Fti2c010Regs *)s->regs;

    if (addr >= FTI2C010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "fti2c010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Fti2c010Regs, cr):
        if (val & FTI2C010_CR_SLAVE_MASK) {
            qemu_log_mask(LOG_UNIMP,
                "fti2c010: slave mode is not yet implemented!\n");
            val &= ~FTI2C010_CR_SLAVE_MASK;
        }
        if (val & FTI2C010_CR_I2CRST) {
            val = 0;
            regs->dr = 0;
            regs->sr = 0;
        } else if ((val & FTI2C010_CR_MASTER) == FTI2C010_CR_MASTER) {
            regs->sr &= ~FTI2C010_SR_ACK;
            if (val & FTI2C010_CR_START) {
                s->rx = !!(regs->dr & I2C_RD);
                if (!i2c_start_transfer(s->bus,
                    extract32(regs->dr, 1, 7), s->rx)) {
                    regs->sr |= FTI2C010_SR_DT | FTI2C010_SR_ACK;
                } else {
                    regs->sr &= ~FTI2C010_SR_DT;
                }
            } else {
                if (s->rx) {
                    regs->dr = i2c_recv(s->bus);
                    regs->sr |= FTI2C010_SR_DR;
                } else {
                    i2c_send(s->bus, (uint8_t)regs->dr);
                    regs->sr |= FTI2C010_SR_DT;
                }
                if (val & FTI2C010_CR_NACK) {
                    i2c_nack(s->bus);
                }
                regs->sr |= FTI2C010_SR_ACK;
                if (val & FTI2C010_CR_STOP) {
                    i2c_end_transfer(s->bus);
                }
            }
        }
        regs->cr = (uint32_t)val;
        fti2c010_update_irq(s);
        break;
    case offsetof(Fti2c010Regs, cdr):
        s->regs[addr >> 2] = (uint32_t)val & 0x3ffff;
        break;
    case offsetof(Fti2c010Regs, dr):
        s->regs[addr >> 2] = (uint32_t)val & 0xff;
        break;
    case offsetof(Fti2c010Regs, tgsr):
        s->regs[addr >> 2] = (uint32_t)val & 0x1fff;
        break;
    default:
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = fti2c010_mmio_read,
    .write = fti2c010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void fti2c010_reset(DeviceState *dev)
{
    Fti2c010State *s = FTI2C010(dev);
    Fti2c010Regs *regs = (Fti2c010Regs *)s->regs;

    memset(s->regs, 0, FTI2C010_REG_SIZE);
    regs->tgsr = FTI2C010_TGSR_TSR(1) | FTI2C010_TGSR_GSR(1);

    qemu_set_irq(s->irq, 0);
}

static void fti2c010_realize(DeviceState *dev, Error **errp)
{
    Fti2c010State *s = FTI2C010(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    s->bus = i2c_init_bus(&sbd->qdev, "i2c");

    memory_region_init_io(&s->mmio, &mmio_ops,
        s, TYPE_FTI2C010, FTI2C010_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);
}

static const VMStateDescription vmstate_fti2c010 = {
    .name = TYPE_FTI2C010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Fti2c010State, FTI2C010_REG_SIZE >> 2),
        VMSTATE_BOOL(rx, Fti2c010State),
        VMSTATE_END_OF_LIST()
    }
};

static void fti2c010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd = &vmstate_fti2c010;
    dc->reset = fti2c010_reset;
    dc->realize = fti2c010_realize;
    dc->no_user = 1;
}

static const TypeInfo fti2c010_info = {
    .name = TYPE_FTI2C010,
    .parent = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Fti2c010State),
    .class_init = fti2c010_class_init,
};

static void fti2c010_register_types(void)
{
    type_register_static(&fti2c010_info);
}

type_init(fti2c010_register_types)
