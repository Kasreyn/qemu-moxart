/*
 * Faraday FTAHBC020S AHB Controller
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * Code based on ftahbc020.c by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under GNU GPL v2+
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"
#include "qemu/bitops.h"
#include "exec/address-spaces.h"
#include "hw/arm/moxa_art.h"

#define TYPE_FTAHBC020S        "ftahbc020s"

typedef struct Ftahbc020sRegs {
    uint32_t sbsr[32];  /* slave base & size register */
    uint32_t pcr;       /* priority control register */
    uint32_t tcr;       /* transfer control register */
    uint32_t cr;        /* control register */
} Ftahbc020sRegs;

#define FTAHBC020S_REG_SIZE      sizeof(Ftahbc020sRegs)

/* slave base & size register */
#define FTAHBC020S_SBSR_BASE(x)  ((uint32_t)(x) & 0xfff00000)
#define FTAHBC020S_SBSR_SIZE(x)  (1 << (20 + extract32((uint32_t)(x), 16, 4)))

/* priority control register */
#define FTAHBC020S_PCR_PLEVEL(x)  (1 << (x)) /* x: 1-9 */

/* control register */
#define FTAHBC020S_CR_INTSTS    (1 << 24) /* interrupt status */
#define FTAHBC020S_CR_RESP(x)   (((x) & 0x3) << 20)
#define FTAHBC020S_CR_INTSMASK  (1 << 16) /* enable interrupt */
#define FTAHBC020S_CR_REMAP     (1 << 0)  /* activate AHB remap */

typedef struct Ftahbc020sState {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    DeviceState *soc;

    /* HW registers */
    uint32_t regs[FTAHBC020S_REG_SIZE >> 2];
} Ftahbc020sState;

#define FTAHBC020S(obj) \
    OBJECT_CHECK(Ftahbc020sState, obj, TYPE_FTAHBC020S)

static void ftahbc020s_map_rom(Ftahbc020sState *s)
{
    Ftahbc020sRegs *regs = (Ftahbc020sRegs *)s->regs;
    MoxaArtState *soc = MOXA_ART(s->soc);

    assert(soc && soc->rom);

    memory_region_set_address(soc->rom, FTAHBC020S_SBSR_BASE(regs->sbsr[4]));
}

static void ftahbc020s_map_ram(Ftahbc020sState *s)
{
    Ftahbc020sRegs *regs = (Ftahbc020sRegs *)s->regs;
    MoxaArtState *soc = MOXA_ART(s->soc);

    assert(soc && soc->ram);

    memory_region_set_address(soc->ram, FTAHBC020S_SBSR_BASE(regs->sbsr[6]));
}

static void ftahbc020s_remap(Ftahbc020sState *s)
{
    Ftahbc020sRegs *regs = (Ftahbc020sRegs *)s->regs;
    uint32_t s4bs, s6bs;

    s4bs = regs->sbsr[4];
    s6bs = regs->sbsr[6];

    regs->sbsr[4] = (s4bs & 0xfffff) | FTAHBC020S_SBSR_BASE(s6bs);
    regs->sbsr[6] = (s6bs & 0xfffff) | FTAHBC020S_SBSR_BASE(s4bs);

    ftahbc020s_map_rom(s);
    ftahbc020s_map_ram(s);
}

static uint64_t ftahbc020s_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftahbc020sState *s = FTAHBC020S(opaque);
    uint64_t ret = 0;

    if (addr >= FTAHBC020S_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftahbc020s: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftahbc020s_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftahbc020sState *s = FTAHBC020S(opaque);

    if (addr >= FTAHBC020S_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftahbc020s: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftahbc020sRegs, sbsr[4]):
        s->regs[addr >> 2] = (uint32_t)val;
        ftahbc020s_map_rom(s);
        break;
    case offsetof(Ftahbc020sRegs, sbsr[6]):
        s->regs[addr >> 2] = (uint32_t)val;
        ftahbc020s_map_ram(s);
        break;
    case offsetof(Ftahbc020sRegs, cr):
        if ((s->regs[addr >> 2] ^ val) & FTAHBC020S_CR_REMAP) {
            ftahbc020s_remap(s);
        }
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    case offsetof(Ftahbc020sRegs, pcr):
    default:
        break;
    }
}

static const MemoryRegionOps ftahbc020s_mmio_ops = {
    .read  = ftahbc020s_mmio_read,
    .write = ftahbc020s_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void ftahbc020s_reset(DeviceState *dev)
{
    Ftahbc020sState *s = FTAHBC020S(dev);
    MoxaArtState *soc;

    memset(s->regs, 0, sizeof(s->regs));

    s->soc = DEVICE(object_property_get_link(OBJECT(s), "soc", NULL));

    assert(s->soc);

    soc = MOXA_ART(s->soc);

    assert(soc->common.ahbc_init);

    soc->common.ahbc_init(soc, s->regs);

    ftahbc020s_map_rom(s);
    ftahbc020s_map_ram(s);
}

static void ftahbc020s_realize(DeviceState *dev, Error **errp)
{
    Ftahbc020sState *s = FTAHBC020S(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &ftahbc020s_mmio_ops,
                          s,
                          TYPE_FTAHBC020S,
                          FTAHBC020S_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
}

static const VMStateDescription vmstate_ftahbc020s = {
    .name = TYPE_FTAHBC020S,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftahbc020sState, FTAHBC020S_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    }
};

static void ftahbc020s_instance_init(Object *obj)
{
    Ftahbc020sState *s = FTAHBC020S(obj);

    object_property_add_link(obj, "soc", TYPE_DEVICE,
        (Object **)&s->soc, NULL);
}


static void ftahbc020s_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc  = TYPE_FTAHBC020S;
    dc->vmsd  = &vmstate_ftahbc020s;
    dc->reset = ftahbc020s_reset;
    dc->realize = ftahbc020s_realize;
    dc->no_user = 1;
}

static const TypeInfo ftahbc020s_info = {
    .name          = TYPE_FTAHBC020S,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftahbc020sState),
    .instance_init = ftahbc020s_instance_init,
    .class_init    = ftahbc020s_class_init,
};

static void ftahbc020s_register_types(void)
{
    type_register_static(&ftahbc020s_info);
}

type_init(ftahbc020s_register_types)
