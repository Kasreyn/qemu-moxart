/*
 * Faraday A369 System Control Unit
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * The system control unit (SCU) is responsible for
 * power, clock and pinmux management.
 *
 * This code is licensed under GNU GPL v2+
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"

#define A369SCU_CHIPID      0x00003369; /* FIE3369(A369) */
#define A369SCU_REVISION    FARADAY_REVISION(1, 0, 0)

typedef struct A369ScuRegs {
    /* 0x000 ~ 0x0ff */
    uint32_t idr;      /* ID Register */
    uint32_t revr;     /* SCU revision id */
    uint32_t hwcfg;    /* HW configuration strap */
    uint32_t cpumcfr;  /* CPUM (master) freq. control */
    uint32_t cr;       /* SCU control register */
    uint32_t sr;       /* SCU status register */
    uint32_t rsvd0[1];
    uint32_t osccr;    /* OSC control register */
    uint32_t pllcr;    /* PLL1 control register */
    uint32_t dllcr;    /* DLL control register */
    uint32_t hclkgr;   /* HCLK gating register */
    uint32_t pclkgr;   /* PCLK gating register */
    uint32_t rsvd1[52];

    /* 0x100 ~ 0x1ff */
    uint32_t spr[16];  /* Scratchpad register */
    uint32_t rsvd2[48];

    /* 0x200 ~ 0x2ff */
    uint32_t gpmux;    /* General PINMUX */
    uint32_t ehwcfg;   /* Extended HW configuration strap */
    uint32_t rsvd3[8];
    uint32_t sccfg[2]; /* Special clock configuration */
    uint32_t scer;     /* Special clock enable register */
    uint32_t rsvd;
    uint32_t mfpmux[2];/* Multi-function pinmux */
    uint32_t dcsrcr[2];/* Driving cap. & Slew rate control */
    uint32_t rsvd4[3];
    uint32_t dccr;     /* Delay chain control register */
    uint32_t pcr;      /* Power control register */
} A369ScuRegs;

#define A369SCU_REG_SIZE    sizeof(A369ScuRegs)

#define TYPE_A369SCU        "a369.scu"

typedef struct A369ScuState {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    MemoryRegion iomem;

    /* HW registers */
    uint32_t regs[A369SCU_REG_SIZE >> 2];
} A369ScuState;

#define A369SCU(obj) \
    OBJECT_CHECK(A369ScuState, obj, TYPE_A369SCU)

static uint64_t a369scu_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    A369ScuState *s = A369SCU(opaque);
    uint64_t ret = 0;

    if (addr >= A369SCU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "a369scu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(A369ScuRegs, idr):
        ret = A369SCU_CHIPID;
        break;
    case offsetof(A369ScuRegs, revr):
        ret = A369SCU_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void a369scu_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    A369ScuState *s = A369SCU(opaque);

    if (addr >= A369SCU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "a369scu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    s->regs[addr >> 2] = (uint32_t)val;
}

static const MemoryRegionOps a369scu_mmio_ops = {
    .read  = a369scu_mmio_read,
    .write = a369scu_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void a369scu_reset(DeviceState *dev)
{
    A369ScuState *s = A369SCU(dev);
    A369ScuRegs *regs = (A369ScuRegs *)s->regs;

    memset(s->regs, 0, sizeof(s->regs));

    /*
     * None of the following stuff are implemented in this model,
     * so we only reset all of them to the hardware defaults.
     */
    regs->hwcfg = 0x00000c10; /* CPU = 4 * HCLK */
    regs->cpumcfr = 0x00000230; /* CPU = 4 * HCLK */
    regs->cr = 0x00000083; /* no low power detect */
    regs->sr = 0x00000100; /* CPU freq. stable */
    regs->osccr = 0x00000003; /* OSCH disabled */
    regs->pllcr = 0x20010003; /* PLL stable, NS = 32 */
    regs->dllcr = 0x00000003; /* DLL stable */
    regs->gpmux = 0x00001078; /* Pinmux */
    regs->ehwcfg = 0x00001cc8; /* Boot from NAND flash */
    regs->sccfg[0] = 0x26877330; /* LCD = HCLK */
    regs->sccfg[1] = 0x000a0a0a; /* SD = HCLK, SPI=PCLK */
    regs->scer = 0x00003fff; /* All clock enabled */
    regs->mfpmux[0] = 0x00000241; /* Pinmux */
    regs->mfpmux[1] = 0x00000000; /* Pinmux */
    regs->dcsrcr[0] = 0x11111111; /* Slow slew rate */
    regs->dcsrcr[1] = 0x11111111; /* Slow slew rate */
    regs->dccr = 0x00000303; /* All delay chain = 3 */
    regs->pcr = 0x8000007f; /* High performance mode */
}

static void a369scu_realize(DeviceState *dev, Error **errp)
{
    A369ScuState *s = A369SCU(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &a369scu_mmio_ops,
                          s,
                          TYPE_A369SCU,
                          A369SCU_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
}

static const VMStateDescription vmstate_a369scu = {
    .name = TYPE_A369SCU,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, A369ScuState, A369SCU_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    }
};

static void a369scu_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc  = TYPE_A369SCU;
    dc->vmsd  = &vmstate_a369scu;
    dc->reset = a369scu_reset;
    dc->realize = a369scu_realize;
    dc->no_user = 1;
}

static const TypeInfo a369scu_info = {
    .name          = TYPE_A369SCU,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(A369ScuState),
    .class_init    = a369scu_class_init,
};

static void a369scu_register_types(void)
{
    type_register_static(&a369scu_info);
}

type_init(a369scu_register_types)
