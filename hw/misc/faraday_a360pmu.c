/*
 * Faraday A369 System Control Unit
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * The system control unit (SCU) is responsible for
 * power, clock and pinmux management.
 *
 * This code is licensed under GNU GPL v2+
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "sysemu/sysemu.h"

#define TYPE_A360PMU        "a360.pmu"
#define A360PMU_CHIPID      0x00003360  /* FIE3360 */

typedef struct A360PmuRegs {
	uint32_t idr;      /* ID register */
	uint32_t rsvd0;
	uint32_t osccr;    /* OSC control register */
	uint32_t pmr;      /* Power mode register */

	uint32_t pmcr;     /* Power manager control register */
	uint32_t peer;     /* Power manager edge detection enable register */
	uint32_t pesr;     /* Power manager edge detection status register */
	uint32_t rsvd1;

	uint32_t pmsr;     /* Power manager status register */
	uint32_t pgsr;     /* Power manager GPIO sleep state register */
	uint32_t rsvd2;
	uint32_t mcr;      /* Misc. control register */

	uint32_t pdcr;     /* PLL/DLL control register */
	uint32_t rsvd3[7];

	uint32_t pspr[16]; /* Power manager scratch pad register */

	uint32_t rsvd4[3];
	uint32_t jssr;     /* Jumper setting status register */
} A360PmuRegs;

#define OSCCR_STABLE    (1 << 9) /* OSCH is stable */
#define OSCCR_DISABLE   (1 << 8) /* Disable OSCH */

#define PMR_FCS         (1 << 2) /* Activate freq. change sequence */
#define PMR_TURBO       (1 << 1) /* Activate CPU turbo mode (2 x AHB) */
#define PMR_SLEEP       (1 << 0) /* Activate system sleep */

#define PMCR_PWLOW      (1 << 5) /* Mask X_powerlow_b pin source */
#define PMCR_WAIT       (1 << 4) /* Enable countdown for CPU power-off */
#define PMCR_WDT        (1 << 3) /* Use WatchDog reset */
#define PMCR_RTC        (1 << 2) /* Enable system wake-up from RTC alarm */
#define PMCR_GPIO1      (1 << 1) /* Enable system wake-up from GPIO1 */
#define PMCR_GPIO0      (1 << 0) /* Enable system wake-up from GPIO0 */

#define PEER_GPIO1_RE   (1 << 9) /* Wake-up upon Rising-Edge of GPIO1 */
#define PEER_GPIO0_RE   (1 << 8) /* Wake-up upon Rising-Edge of GPIO0 */
#define PEER_GPIO1_FE   (1 << 1) /* Wake-up upon Falling-Edge of GPIO1 */
#define PEER_GPIO0_FE   (1 << 0) /* Wake-up upon Falling-Edge of GPIO0 */

#define PESR_RTC        (1 << 2) /* Wake-up from RTC */
#define PESR_GPIO1      (1 << 1) /* Wake-up from GPIO1 */
#define PESR_GPIO0      (1 << 0) /* Wake-up from GPIO0 */

#define PMSR_PWLOW      (1 << 19)/* X_powerlow_b is pulled low */
#define PMSR_ISR_PWLOW  (1 << 18)/* Interrupt Status: X_powerlow_b */
#define PMSR_ISR_FCS    (1 << 17)/* Interrupt Status: FCS */
#define PMSR_ISR_TURBO  (1 << 16)/* Interrupt Status: TURBO */
#define PMSR_WAKEUP     (1 << 10)/* Wake-up from sleep mode */
#define PMSR_WDT        (1 << 9) /* Reset by WatchDog */
#define PMSR_HW         (1 << 8) /* Reset by Hardware */
#define PMSR_NOGPI      (1 << 2) /* Disable GPIO input mode */
#define PMSR_GPIO_HELD  (1 << 1) /* GPIO are held in sleep mode */

#define PDCR_WAITDLLS   (1 << 23)/* Wait until DLL stable */
#define PDCR_DLLS       (1 << 22)/* DLL is stable */
#define PDCR_DDLL       (1 << 21)/* Disable DLL */
#define PDCR_PLL1MS(x)  (((x) & 0xf) << 11)/* PLL1 MS */
#define PDCR_PLL1NS(x)  (((x) & 0x3f) << 3)/* PLL1 NS */
#define PDCR_WAITPLL1S  (1 << 2) /* Wait until PLL1 stable */
#define PDCR_PLL1S      (1 << 1) /* PLL1 is stable */
#define PDCR_DPLL1      (1 << 0) /* Disable PLL1 */

#define JSSR_PLL1NS(x)  (((x) & 0x3f) << 2)/* PLL1 NS */
#define JSSR_OSC        (1 << 0) /* PLL1/DLL are disabled */

#define A360PMU_REG_SIZE    sizeof(A360PmuRegs)

typedef struct A360PmuState {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    MemoryRegion iomem;

    /* HW registers */
    uint32_t regs[A360PMU_REG_SIZE >> 2];
} A360PmuState;

#define A360PMU(obj) \
    OBJECT_CHECK(A360PmuState, obj, TYPE_A360PMU)

static uint64_t a360pmu_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    A360PmuState *s = A360PMU(opaque);
    uint64_t ret = 0;

    if (addr >= A360PMU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "a360pmu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(A360PmuRegs, idr):
        ret = A360PMU_CHIPID;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void a360pmu_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    A360PmuState *s = A360PMU(opaque);

    if (addr >= A360PMU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "a360pmu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    s->regs[addr >> 2] = (uint32_t)val;
}

static const MemoryRegionOps a360pmu_mmio_ops = {
    .read  = a360pmu_mmio_read,
    .write = a360pmu_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void a360pmu_reset(DeviceState *dev)
{
    A360PmuState *s = A360PMU(dev);
    A360PmuRegs *regs = (A360PmuRegs *)s->regs;

    memset(s->regs, 0, sizeof(s->regs));

    /*
     * None of the following stuff are implemented in this model,
     * so we only reset all of them to the hardware defaults.
     */
    regs->osccr = OSCCR_STABLE;
    regs->pmr = PMR_TURBO;
    regs->pmcr = PMCR_PWLOW | PMCR_WAIT | PMCR_WDT | PMCR_GPIO0;
    regs->peer = PEER_GPIO0_FE | PEER_GPIO0_RE;
    regs->pesr = PESR_GPIO0;
    regs->pmsr = PMSR_PWLOW;
    regs->pdcr = PDCR_DLLS | PDCR_PLL1MS(1) | PDCR_PLL1NS(27) | PDCR_PLL1S;
    regs->jssr = JSSR_PLL1NS(27);
}

static void a360pmu_realize(DeviceState *dev, Error **errp)
{
    A360PmuState *s = A360PMU(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &a360pmu_mmio_ops,
                          s,
                          TYPE_A360PMU,
                          A360PMU_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
}

static const VMStateDescription vmstate_a360pmu = {
    .name = TYPE_A360PMU,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, A360PmuState, A360PMU_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    }
};

static void a360pmu_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc  = TYPE_A360PMU;
    dc->vmsd  = &vmstate_a360pmu;
    dc->reset = a360pmu_reset;
    dc->realize = a360pmu_realize;
    dc->no_user = 1;
}

static const TypeInfo a360pmu_info = {
    .name          = TYPE_A360PMU,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(A360PmuState),
    .class_init    = a360pmu_class_init,
};

static void a360pmu_register_types(void)
{
    type_register_static(&a360pmu_info);
}

type_init(a360pmu_register_types)
