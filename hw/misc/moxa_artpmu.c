/*
 * Moxa ART Power Management Unit
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * Code based on faraday_a360pmu.c by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under GNU GPL v2+
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "sysemu/sysemu.h"

#define TYPE_ARTPMU       "moxa_art.pmu"
#define ARTPMU_CHIPID      0x03300000  /*  */

typedef struct ArtPmuRegs {
	uint32_t idr;      /* ID register */
	uint32_t rsvd0;
	uint32_t osccr;    /* OSC control register */
	uint32_t pmr;      /* Power mode register */

	uint32_t pmcr;     /* Power manager control register */
	uint32_t peer;     /* Power manager edge detection enable register */
	uint32_t pesr;     /* Power manager edge detection status register */
	uint32_t rsvd1;

	uint32_t pmsr;     /* Power manager status register */
	uint32_t pgsr;     /* Power manager GPIO sleep state register */
	uint32_t rsvd2;
	uint32_t mcr;      /* Misc. control register */

	uint32_t pdcr;     /* PLL/DLL control register */
	uint32_t rsvd3[7];

	uint32_t pspr[16]; /* Power manager scratch pad register */

	uint32_t rsvd4[3];
	uint32_t jssr;     /* Jumper setting status register */
} ArtPmuRegs;

#define OSCCR_STABLE    (1 << 9) /* OSCH is stable */
#define OSCCR_DISABLE   (1 << 8) /* Disable OSCH */

#define PMR_FCS         (1 << 2) /* Activate freq. change sequence */
#define PMR_TURBO       (1 << 1) /* Activate CPU turbo mode (2 x AHB) */
#define PMR_SLEEP       (1 << 0) /* Activate system sleep */

#define PMCR_PWLOW      (1 << 5) /* Mask X_powerlow_b pin source */
#define PMCR_WAIT       (1 << 4) /* Enable countdown for CPU power-off */
#define PMCR_WDT        (1 << 3) /* Use WatchDog reset */
#define PMCR_RTC        (1 << 2) /* Enable system wake-up from RTC alarm */
#define PMCR_GPIO1      (1 << 1) /* Enable system wake-up from GPIO1 */
#define PMCR_GPIO0      (1 << 0) /* Enable system wake-up from GPIO0 */

#define PEER_GPIO1_RE   (1 << 9) /* Wake-up upon Rising-Edge of GPIO1 */
#define PEER_GPIO0_RE   (1 << 8) /* Wake-up upon Rising-Edge of GPIO0 */
#define PEER_GPIO1_FE   (1 << 1) /* Wake-up upon Falling-Edge of GPIO1 */
#define PEER_GPIO0_FE   (1 << 0) /* Wake-up upon Falling-Edge of GPIO0 */

#define PESR_RTC        (1 << 2) /* Wake-up from RTC */
#define PESR_GPIO1      (1 << 1) /* Wake-up from GPIO1 */
#define PESR_GPIO0      (1 << 0) /* Wake-up from GPIO0 */

#define PMSR_PWLOW      (1 << 19)/* X_powerlow_b is pulled low */
#define PMSR_ISR_PWLOW  (1 << 18)/* Interrupt Status: X_powerlow_b */
#define PMSR_ISR_FCS    (1 << 17)/* Interrupt Status: FCS */
#define PMSR_ISR_TURBO  (1 << 16)/* Interrupt Status: TURBO */
#define PMSR_WAKEUP     (1 << 10)/* Wake-up from sleep mode */
#define PMSR_WDT        (1 << 9) /* Reset by WatchDog */
#define PMSR_HW         (1 << 8) /* Reset by Hardware */
#define PMSR_NOGPI      (1 << 2) /* Disable GPIO input mode */
#define PMSR_GPIO_HELD  (1 << 1) /* GPIO are held in sleep mode */

#define PDCR_WAITDLLS   (1 << 23)/* Wait until DLL stable */
#define PDCR_DLLS       (1 << 22)/* DLL is stable */
#define PDCR_DDLL       (1 << 21)/* Disable DLL */
#define PDCR_PLL1MS(x)  (((x) & 0xf) << 11)/* PLL1 MS */
#define PDCR_PLL1NS(x)  (((x) & 0x3f) << 3)/* PLL1 NS */
#define PDCR_WAITPLL1S  (1 << 2) /* Wait until PLL1 stable */
#define PDCR_PLL1S      (1 << 1) /* PLL1 is stable */
#define PDCR_DPLL1      (1 << 0) /* Disable PLL1 */

#define JSSR_PLL1NS(x)  (((x) & 0x3f) << 2)/* PLL1 NS */
#define JSSR_OSC        (1 << 0) /* PLL1/DLL are disabled */

#define ARTPMU_REG_SIZE    sizeof(ArtPmuRegs)

typedef struct ArtPmuState {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    MemoryRegion iomem;

    /* HW registers */
    uint32_t regs[ARTPMU_REG_SIZE >> 2];
} ArtPmuState;

#define ARTPMU(obj) \
    OBJECT_CHECK(ArtPmuState, obj, TYPE_ARTPMU)

static uint64_t artpmu_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    ArtPmuState *s = ARTPMU(opaque);
    uint64_t ret = 0;

    if (addr >= ARTPMU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "moxa_art.pmu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(ArtPmuRegs, idr):
        ret = ARTPMU_CHIPID;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    fprintf(stderr, "moxa_art.pmu r@0x%02" HWADDR_PRIx " val=0x%08" PRIx64 "\n", addr, ret);

    return ret;
}

static void artpmu_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    ArtPmuState *s = ARTPMU(opaque);

    fprintf(stderr, "moxa_art.pmu w@0x%02" HWADDR_PRIx " val=0x%08" PRIx64 "\n", addr, val);

    if (addr >= ARTPMU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "moxa_art.pmu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    s->regs[addr >> 2] = (uint32_t)val;
}

static const MemoryRegionOps artpmu_mmio_ops = {
    .read  = artpmu_mmio_read,
    .write = artpmu_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void artpmu_reset(DeviceState *dev)
{
    ArtPmuState *s = ARTPMU(dev);
    ArtPmuRegs *regs = (ArtPmuRegs *)s->regs;

    memset(s->regs, 0, sizeof(s->regs));

    /*
     * None of the following stuff are implemented in this model,
     * so we only reset all of them to the hardware defaults.
     */
    regs->osccr = 0x00000206;
    regs->pmr = 0x00000002;
    regs->pmcr = 0x000000E1;
    regs->peer = 0x00000011;
    regs->pesr = 0x00000000;
    regs->pmsr = 0x00000104;
    regs->pdcr = 0x00058282;
    regs->jssr = 0x00000652;
}

static void artpmu_realize(DeviceState *dev, Error **errp)
{
    ArtPmuState *s = ARTPMU(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &artpmu_mmio_ops,
                          s,
                          TYPE_ARTPMU,
                          ARTPMU_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
}

static const VMStateDescription vmstate_artpmu = {
    .name = TYPE_ARTPMU,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, ArtPmuState, ARTPMU_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    }
};

static void artpmu_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc  = TYPE_ARTPMU;
    dc->vmsd  = &vmstate_artpmu;
    dc->reset = artpmu_reset;
    dc->realize = artpmu_realize;
    dc->no_user = 1;
}

static const TypeInfo artpmu_info = {
    .name          = TYPE_ARTPMU,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(ArtPmuState),
    .class_init    = artpmu_class_init,
};

static void artpmu_register_types(void)
{
    type_register_static(&artpmu_info);
}

type_init(artpmu_register_types)
