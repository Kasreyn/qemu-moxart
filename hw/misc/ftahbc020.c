/*
 * Faraday FTAHBC020 AHB Controller
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under GNU GPL v2+
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"
#include "qemu/bitops.h"

#define TYPE_FTAHBC020        "ftahbc020"
#define FTAHBC020_REVISION    FARADAY_REVISION(0x01, 0x11, 0x00)

typedef struct Ftahbc020Regs {
    uint32_t sbsr[32];  /* slave base & size register */
    uint32_t plr;       /* priority level register */
    uint32_t icr;       /* idle count register */
    uint32_t cr;        /* control register */
    uint32_t revr;      /* revision register */
    uint32_t brer;      /* bus request enable register */
} Ftahbc020Regs;

#define FTAHBC020_REG_SIZE      sizeof(Ftahbc020Regs)

/* slave base & size register */
#define FTAHBC020_SBSR_BASE(x)  ((uint32_t)(x) & 0xfff00000)
#define FTAHBC020_SBSR_SIZE(x)  (1 << (20 + extract32((uint32_t)(x), 16, 4)))

/* control register */
#define FTAHBC020_CR_IS             (1 << 24) /* interrupt status */
#define FTAHBC020_CR_IE             (1 << 16) /* enable interrupt */
#define FTAHBC020_CR_BURST(x)       (1 + extract32((uint32_t)(x), 8, 8))
#define FTAHBC020_CR_REMAP          (1 << 0)  /* activate AHB remap */
#define FTAHBC020_CR_DEFAULT(burst) ((((burst) - 1) & 0xff) << 8)

typedef struct Ftahbc020State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    DeviceState *soc;

    /* HW registers */
    uint32_t regs[FTAHBC020_REG_SIZE >> 2];
} Ftahbc020State;

#define FTAHBC020(obj) \
    OBJECT_CHECK(Ftahbc020State, obj, TYPE_FTAHBC020)

static uint64_t ftahbc020_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftahbc020State *s = FTAHBC020(opaque);
    uint64_t ret = 0;

    if (addr >= FTAHBC020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftahbc020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftahbc020Regs, revr):
        ret = FTAHBC020_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftahbc020_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftahbc020State *s = FTAHBC020(opaque);
    FaradaySocState *soc = (FaradaySocState *)s->soc;

    if (addr >= FTAHBC020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftahbc020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftahbc020Regs, cr):
        if ((s->regs[addr >> 2] ^ val) & FTAHBC020_CR_REMAP) {
            if (soc && soc->ahbc_remap) {
                soc->ahbc_remap(soc, !!(val & FTAHBC020_CR_REMAP));
            }
        }
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    case offsetof(Ftahbc020Regs, plr):
    case offsetof(Ftahbc020Regs, brer):
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    default:
        break;
    }
}

static const MemoryRegionOps ftahbc020_mmio_ops = {
    .read  = ftahbc020_mmio_read,
    .write = ftahbc020_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void ftahbc020_reset(DeviceState *dev)
{
    FaradaySocState *soc;
    Ftahbc020State *s = FTAHBC020(dev);
    Ftahbc020Regs *regs = (Ftahbc020Regs *)s->regs;

    memset(s->regs, 0, sizeof(s->regs));
    regs->cr = FTAHBC020_CR_DEFAULT(16);

    s->soc = DEVICE(object_property_get_link(OBJECT(s), "soc", NULL));
    if (s->soc) {
        soc = (FaradaySocState *)s->soc;
        if (soc->ahbc_init) {
            soc->ahbc_init(soc, s->regs);
        }
        if (soc->ahbc_remap) {
            soc->ahbc_remap(soc, false);
        }
    }
}

static void ftahbc020_realize(DeviceState *dev, Error **errp)
{
    Ftahbc020State *s = FTAHBC020(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &ftahbc020_mmio_ops,
                          s,
                          TYPE_FTAHBC020,
                          FTAHBC020_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
}

static const VMStateDescription vmstate_ftahbc020 = {
    .name = TYPE_FTAHBC020,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftahbc020State, FTAHBC020_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    }
};

static void ftahbc020_instance_init(Object *obj)
{
    Ftahbc020State *s = FTAHBC020(obj);

    object_property_add_link(obj, "soc", TYPE_DEVICE,
        (Object **)&s->soc, NULL);
}

static void ftahbc020_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc  = TYPE_FTAHBC020;
    dc->vmsd  = &vmstate_ftahbc020;
    dc->reset = ftahbc020_reset;
    dc->realize = ftahbc020_realize;
    dc->no_user = 1;
}

static const TypeInfo ftahbc020_info = {
    .name          = TYPE_FTAHBC020,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftahbc020State),
    .instance_init = ftahbc020_instance_init,
    .class_init    = ftahbc020_class_init,
};

static void ftahbc020_register_types(void)
{
    type_register_static(&ftahbc020_info);
}

type_init(ftahbc020_register_types)
