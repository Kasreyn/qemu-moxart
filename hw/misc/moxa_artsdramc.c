/*
 * Moxa ART SDRAM Controller.
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/bitops.h"

#define TYPE_ARTSDRAMC      "moxa_art.sdramc"

typedef struct ArtSdramcRegs {
    uint32_t tpr[2]; /* timing parameter register */
    uint32_t cr[2]; /* configuration register */
    uint32_t xbsr[4]; /* external bank base & size register */
	uint32_t rsvd0[4]; 
    uint32_t agr; /* arbitration group register */
    uint32_t frr; /* flush request register */
	uint32_t rsvd1;
} ArtSdramcRegs;

#define ARTSDRAMC_REG_SIZE  sizeof(ArtSdramcRegs)

typedef struct ArtSdramcState {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;

    /* HW register caches */
    uint32_t regs[ARTSDRAMC_REG_SIZE >> 2];
} ArtSdramcState;

#define ARTSDRAMC(obj) \
    OBJECT_CHECK(ArtSdramcState, obj, TYPE_ARTSDRAMC)

static uint64_t artsdramc_mmio_read(void *opaque, hwaddr addr, unsigned size)
{

    ArtSdramcState *s = ARTSDRAMC(opaque);
    uint64_t ret = 0;

    if (addr >= ARTSDRAMC_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "moxa_art.sdramc: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    ret = s->regs[addr >> 2];

    /*fprintf(stderr, "moxa_art.sdramc r@0x%02" HWADDR_PRIx " val=0x%08" PRIx64 "\n", addr, ret);*/

    return ret;
}

static void artsdramc_mmio_write(void *opaque, hwaddr addr,
        uint64_t val, unsigned size)
{
    ArtSdramcState *s = ARTSDRAMC(opaque);

    if (addr >= ARTSDRAMC_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "moxa_art.sdramc: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    /*fprintf(stderr, "moxa_art.sdramc w@0x%02" HWADDR_PRIx " val=0x%08" PRIx64 "\n", addr, val);*/

    switch (addr) {
    case offsetof(ArtSdramcRegs, cr[1]):
        break;
    /* read-only registers */
    case offsetof(ArtSdramcRegs, rsvd0[0]) ... offsetof(ArtSdramcRegs, rsvd0[3]):
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = artsdramc_mmio_read,
    .write = artsdramc_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static VMStateDescription vmstate_artsdramc = {
    .name = TYPE_ARTSDRAMC,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, ArtSdramcState, ARTSDRAMC_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    },
};

static void artsdramc_reset(DeviceState *dev)
{
    ArtSdramcState *s = ARTSDRAMC(dev);
    ArtSdramcRegs *regs = (ArtSdramcRegs *)s->regs;

    memset(s->regs, 0, sizeof(s->regs));

    /*
     * None of the following stuff are implemented in this model,
     * so we only reset all of them to the hardware defaults.
     */
    regs->agr = 0x44444444;
}

static void artsdramc_realize(DeviceState *dev, Error **errp)
{
    ArtSdramcState *s = ARTSDRAMC(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_ARTSDRAMC,
                          ARTSDRAMC_REG_SIZE);

    sysbus_init_mmio(sbd, &s->iomem);
}

static void artsdramc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_artsdramc;
    dc->reset = artsdramc_reset;
    dc->realize = artsdramc_realize;
    dc->no_user = 1;
}

static const TypeInfo artsdramc_info = {
    .name          = TYPE_ARTSDRAMC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(ArtSdramcState),
    .class_init    = artsdramc_class_init,
};

static void artsdramc_register_types(void)
{
    type_register_static(&artsdramc_info);
}

type_init(artsdramc_register_types)
