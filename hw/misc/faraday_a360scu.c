/*
 * Faraday A360 System Control Unit
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * The system control unit (SCU) is responsible for
 * power, clock and pinmux management.
 *
 * This code is licensed under GNU GPL v2+
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "sysemu/sysemu.h"

#define TYPE_A360SCU            "a360.scu"

#define A360SCU_CHIPID          0x33600000 /* FIE3360 */

/*
 * A360 clock defaults
 * 1. Enable all the AHB devices
 * 2. Enable all the APB devices
 */
#define A360SCU_HCER_DEFAULT    0x0000ffff
#define A360SCU_PCER_DEFAULT    0x0000ffff

/*
 * A360 pinmux defaults
 * 1. FTSSP010: Audio mode, clock = 12.288 Hz
 * 2. Disable UART1
 * 3. Disable LVBC
 */
#define A360SCU_IOMUX0_DEFAULT  0x800002aa
#define A360SCU_IOMUX1_DEFAULT  0x82aaaaaa
#define A360SCU_IOMUX2_DEFAULT  0x00000000
#define A360SCU_IOMUX3_DEFAULT  0x00555500

typedef struct A360ScuRegs {
    uint32_t idr;      /* 0x00: ID Register */
    uint32_t gcr;      /* 0x04: General Control Register */
    uint32_t ccr;      /* 0x08: Clock Configuration Register */
    uint32_t hcer;     /* 0x0C: AHB Clock Enable Register */
    uint32_t pcer;     /* 0x10: APB Clock Enable Register */
    uint32_t csr;      /* 0x14: Configuration Strap Register */
    uint32_t iomcr[4]; /* IO Mux Control Register */
    uint32_t rsvd[6];  /* reserved */
} A360ScuRegs;

#define A360SCU_REG_SIZE    sizeof(A360ScuRegs)

#define GCR_LVBC_IRQ3       (1 << 25) /* LVBC interrupt 3 propagation */
#define GCR_LVBC_IRQ2       (1 << 24) /* LVBC interrupt 2 propagation */
#define GCR_LVTX_RATE(x)    (((x) & 0x1f) << 16) /* LVTX clock rate */
#define GCR_LVTX_CLK_OUT    (1 << 13) /* Enable LVTX clock out */
#define GCR_USBH1_PLL_ALIVE (1 << 11) /* USB Host PLL alive */
#define GCR_USBH0_PLL_ALIVE (1 << 10) /* USB Host PLL alive */
#define GCR_USBH1_CLK_OUT   (1 << 9) /* Enable USB Host clock out */
#define GCR_USBH0_CLK_OUT   (1 << 8) /* Enable USB Host clock out */
#define GCR_DEBUG           (1 << 7) /* Enable debug mode */
#define GCR_DEBUG_SW        (1 << 6) /* Enable software debug mode */
#define GCR_IM              (1 << 5) /* Interrupt mask */
#define GCR_RESET           (1 << 4) /* Software reset */

#define CCR_LVDSTX_DSEL(x)  (((x) & 0x1f) << 21) /* LVDS Tx DLL Select */
#define CCR_LVDSRX_DSEL(x)  (((x) & 0x1f) << 16) /* LVDS Rx DLL Select */
#define CCR_SSP1_CKFQ(x)    (((x) & 0xf) << 12) /* SSP1 Clock Freq. */
#define CCR_SSP0_CKFQ(x)    (((x) & 0xf) << 8) /* SSP0 Clock Freq. */
#define CCR_SSP1_EXTCLK     (1 << 7) /* SSP1 use external clock */
#define CCR_SSP1_PCLK       (0 << 7) /* SSP1 use APB clock */
#define CCR_SSP0_EXTCLK     (1 << 6) /* SSP0 use external clock */
#define CCR_SSP0_PCLK       (0 << 6) /* SSP0 use APB clock */
#define CCR_LVDS_HCLK       (0 << 4) /* LVDS Tx clock select */
#define CCR_LVDS_PCLK       (1 << 4)
#define CCR_LVDS_EXTCLK     (2 << 4)
#define CCR_SDC_HCLK        (2 << 2) /* SD/MMC clock select */
#define CCR_SDC_MCLK        (1 << 2)
#define CCR_SDC_DCLK        (0 << 2)
#define CCR_LCD_EXTCLK      (2 << 0) /* LCD clock select */
#define CCR_LCD_MCLK        (1 << 0)
#define CCR_LCD_HCLK        (0 << 0)

#define CSR_PLL_PRESCALE    (1 << 9)
#define CSR_PCIE_MODE1      (1 << 8)
#define CSR_PCIE_MODE0      (1 << 7)
#define CSR_DBG_SW          (1 << 6)
#define CSR_DBG_EN          (1 << 5)
#define CSR_NAND_LP         (1 << 4)         /* NAND: Large Page */
#define CSR_NAND_AL(x)      (((x) & 3) << 2) /* NAND: Address Length */
#define CSR_NAND_16X        (1 << 1)         /* NAND: 16-bits */
#define CSR_SPIBOOT         (1 << 0)         /* Boot from SPI(1)/NAND(0) */

typedef struct A360ScuState {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    MemoryRegion iomem;

    /* HW registers */
    uint32_t regs[A360SCU_REG_SIZE >> 2];
} A360ScuState;

#define A360SCU(obj) \
    OBJECT_CHECK(A360ScuState, obj, TYPE_A360SCU)

static uint64_t a360scu_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    A360ScuState *s = A360SCU(opaque);
    uint64_t ret = 0;

    if (addr >= A360SCU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "a360scu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(A360ScuRegs, idr):
        ret = A360SCU_CHIPID;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void a360scu_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    A360ScuState *s = A360SCU(opaque);

    if (addr >= A360SCU_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "a360scu: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    s->regs[addr >> 2] = (uint32_t)val;
}

static const MemoryRegionOps a360scu_mmio_ops = {
    .read  = a360scu_mmio_read,
    .write = a360scu_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void a360scu_reset(DeviceState *dev)
{
    A360ScuState *s = A360SCU(dev);
    A360ScuRegs *regs = (A360ScuRegs *)s->regs;

    memset(s->regs, 0, sizeof(s->regs));

    /*
     * None of the following stuff are implemented in this model,
     * so we only reset all of them to the hardware defaults.
     */
    regs->gcr  = GCR_LVBC_IRQ3 | GCR_LVBC_IRQ2 | GCR_LVTX_RATE(1)
        | GCR_LVTX_CLK_OUT | GCR_USBH1_PLL_ALIVE | GCR_USBH0_PLL_ALIVE
        | GCR_USBH1_CLK_OUT | GCR_USBH0_CLK_OUT;
    regs->ccr  = CCR_LVDSRX_DSEL(1) | CCR_SSP1_CKFQ(10) | CCR_SSP0_CKFQ(10);
    regs->hcer = A360SCU_HCER_DEFAULT;
    regs->pcer = A360SCU_PCER_DEFAULT;
    regs->csr  = CSR_NAND_LP | CSR_NAND_AL(1);
    regs->iomcr[0] = A360SCU_IOMUX0_DEFAULT;
    regs->iomcr[1] = A360SCU_IOMUX1_DEFAULT;
    regs->iomcr[2] = A360SCU_IOMUX2_DEFAULT;
    regs->iomcr[3] = A360SCU_IOMUX3_DEFAULT;
}

static void a360scu_realize(DeviceState *dev, Error **errp)
{
    A360ScuState *s = A360SCU(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &a360scu_mmio_ops,
                          s,
                          TYPE_A360SCU,
                          A360SCU_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
}

static const VMStateDescription vmstate_a360scu = {
    .name = TYPE_A360SCU,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, A360ScuState, A360SCU_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    }
};

static void a360scu_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->desc  = TYPE_A360SCU;
    dc->vmsd  = &vmstate_a360scu;
    dc->reset = a360scu_reset;
    dc->realize = a360scu_realize;
    dc->no_user = 1;
}

static const TypeInfo a360scu_info = {
    .name          = TYPE_A360SCU,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(A360ScuState),
    .class_init    = a360scu_class_init,
};

static void a360scu_register_types(void)
{
    type_register_static(&a360scu_info);
}

type_init(a360scu_register_types)
