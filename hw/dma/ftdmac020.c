/*
 * Faraday FTDMAC020 DMA controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+
 *
 * Note: The descending address mode is not supported.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "sysemu/dma.h"
#include "sysemu/sysemu.h"
#include "sysemu/blockdev.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"

#define TYPE_FTDMAC020      "ftdmac020"

#ifndef CONFIG_MOXART
#define FTDMAC020_REVISION  FARADAY_REVISION(1, 13, 0)
#endif

typedef struct Ftdmac020ChanRegs {
    uint32_t ctr;   /* Control Register */
    uint32_t cfr;   /* Configuration Register */
    uint32_t sar;   /* Source Address Register */
    uint32_t dar;   /* Destination Address Register */
    uint32_t llr;   /* Linked-List Register */
    uint32_t dlr;   /* Data Length Register */
    uint32_t rsvd[2];
} Ftdmac020ChanRegs;

#define FTDMAC020_CHAN_REG_SIZE sizeof(Ftdmac020ChanRegs)

typedef struct Ftdmac020Regs {
    uint32_t isr;   /* Interrupt Status Register */
    uint32_t tcisr; /* Terminal Count Interrupt Status Register */
    uint32_t tciscr;/* Terminal Count Interrupt Status Clear Register */
    uint32_t eaisr; /* Error/Abort Interrupt Status Register */

    uint32_t eaiscr;/* Error/Abort Interrupt Status Clear Register */
    uint32_t tcsr;  /* Terminal Count Status Register */
    uint32_t easr;  /* Error/Abort Status Register */
    uint32_t cesr;  /* Channel Enabled Status Register */

    uint32_t cbsr;  /* Channel Busy Status Register */
    uint32_t csr;   /* Configuration Status Register */
    uint32_t sync;  /* Synchronization Register */
#ifndef CONFIG_MOXART
    uint32_t rsvd0[1];

    uint32_t revr;  /* Revision Register */
    uint32_t fear;  /* Feature Register */
    uint32_t rsvd1[50];
#else
    uint32_t rsvd0[53];
#endif

    Ftdmac020ChanRegs chan[8];
} Ftdmac020Regs;

#define FTDMAC020_REG_SIZE      sizeof(Ftdmac020Regs)

/*
 * Interrupt Status Register (TC|ABORT|ERROR)
 */
#define FTDMAC020_ISR_CHAN(x)   (1 << ((x) & 0x7))
#define FTDMAC020_ISR_MASK      0xff

/*
 * Terminal Count Interrupt Status Register
 */
#define FTDMAC020_TCISR_CHAN(x) FTDMAC020_ISR_CHAN(x)
#define FTDMAC020_TCISR_MASK    FTDMAC020_ISR_MASK

/*
 * Error/Abort Interrupt Status Register
 */
#define FTDMAC020_EAISR_CABT(x) (((x) >> 16) & 0xff) /* channel abort */
#define FTDMAC020_EAISR_CERR(x) (((x) >> 0) & 0xff) /* channel error */
#define FTDMAC020_EAISR_MASK    0x00ff00ff

/*
 * Terminal Count Status Register
 */
#define FTDMAC020_TCSR_CHAN(x)  FTDMAC020_ISR_CHAN(x)
#define FTDMAC020_TCSR_MASK     FTDMAC020_ISR_MASK

/*
 * Error/Abort Status Register
 */
#define FTDMAC020_EASR_CABT(x)  FTDMAC020_EAISR_CABT(x)
#define FTDMAC020_EASR_CERR(x)  FTDMAC020_EAISR_CERR(x)
#define FTDMAC020_EASR_MASK     FTDMAC020_EAISR_MASK

/*
 * Channel Enabled Status Register
 */
#define FTDMAC020_CESR_CHAN(x)  FTDMAC020_ISR_CHAN(x)
#define FTDMAC020_CESR_MASK     FTDMAC020_ISR_MASK

/*
 * Channel Busy Status Register
 */
#define FTDMAC020_CBSR_CHAN(x)  FTDMAC020_ISR_CHAN(x)
#define FTDMAC020_CBSR_MASK     FTDMAC020_ISR_MASK

/*
 * Configuration Status Register
 */
#define FTDMAC020_CSR_BE_AHB1   (1 << 2) /* AHB1 is big-endian */
#define FTDMAC020_CSR_BE_AHB0   (1 << 1) /* AHB0 is big-endian */
#define FTDMAC020_CSR_EN_DMA    (1 << 0) /* Enable dma function */

/*
 * Synchronization register
 */
#define FTDMAC020_SYNC_CHAN(x)  FTDMAC020_ISR_CHAN(x)
#define FTDMAC020_SYNC_MASK     FTDMAC020_ISR_MASK

/*
 * Feature register
 */
#define FTDMAC020_FEAR_NUM_CHAN(x)  (((x) & 0xf) << 12) /* num. of channels */
#define FTDMAC020_FEAR_HAS_BR       (1 << 10) /* has built-in birdge */
#define FTDMAC020_FEAR_HAS_2AHB     (1 << 9)  /* has dual AHB bus */
#define FTDMAC020_FEAR_SUPP_LLP     (1 << 8)  /* support linked-list */
#define FTDMAC020_FEAR_BITS_FIFO(x) ((x) & 0xf) /* FIFO address width = 2^N */

/*
 * FTDMAC020 channel registers
 */
#define FTDMAC020_CHAN_REG_START        offsetof(Ftdmac020Regs, chan)
#define FTDMAC020_CHAN_REG_END          (FTDMAC020_REG_SIZE - 4)
#define FTDMAC020_CHAN_ID(addr)         (((addr) - 0x100) >> 5)

/*
 * Channel control register
 */
#define FTDMAC020_CHAN_CTR_START        (1 << 0)
/* dma addressing mode */
#define FTDMAC020_CHAN_CTR_DST_ASC      (0 << 3)/* next = ++curr */
#define FTDMAC020_CHAN_CTR_DST_DES      (1 << 3)/* next = --curr */
#define FTDMAC020_CHAN_CTR_DST_IO       (2 << 3)/* next = curr */
#define FTDMAC020_CHAN_CTR_DST_RSVD     (3 << 3)/* resvered */
#define FTDMAC020_CHAN_CTR_SRC_ASC      (0 << 5)/* next = ++curr */
#define FTDMAC020_CHAN_CTR_SRC_DES      (1 << 5)/* next = --curr */
#define FTDMAC020_CHAN_CTR_SRC_IO       (2 << 5)/* next = curr */
#define FTDMAC020_CHAN_CTR_SRC_RSVD     (2 << 5)/* reserved */
#define FTDMAC020_CHAN_CTR_DMAHS        (1 << 7)
/* dma data width */
#define FTDMAC020_CHAN_CTR_DST_DW(x)    (8 << extract32(x, 8, 2))
#define FTDMAC020_CHAN_CTR_SRC_DW(x)    (8 << extract32(x, 11, 2))
#define FTDMAC020_CHAN_CTR_ABORT        (1 << 15)
#define FTDMAC020_CHAN_CTR_BURST(x) \
    (1 << (extract32(x, 16, 3) ? (extract32(x, 16, 3) + 1) : 0))
#define FTDMAC020_CHAN_CTR_PRIVILEGED   (1 << 19)
#define FTDMAC020_CHAN_CTR_BUFFERABLE   (1 << 20)
#define FTDMAC020_CHAN_CTR_CACHEABLE    (1 << 21)
/* dma priority */
#define FTDMAC020_CHAN_CTR_PRI0         (0 << 22)
#define FTDMAC020_CHAN_CTR_PRI1         (1 << 22)
#define FTDMAC020_CHAN_CTR_PRI2         (2 << 22)
#define FTDMAC020_CHAN_CTR_PRI3         (3 << 22)
/* dma fifo threshold */
#define FTDMAC020_CHAN_CTR_FFTH(x)      extract32(x, 24, 3)
/* no tc status update */
#define FTDMAC020_CHAN_CTR_NOTC         (1 << 31)

/*
 * While updating CTR from LLP descriptor, only the following fields
 * would be reloaded with the value from descriptor.
 *
 * 1. bus select (AHB0 or AHB1)
 * 2. addressing mode
 * 3. data width
 * 4. burst size
 * 5. fifo threshold
 * 6. no tc status update control bit
 */
#define FTDMAC020_CHAN_CTR_LLP_MASK     0x78f8c081

/*
 * Channel configuration register
 */
#define FTDMAC020_CHAN_CFR_NOTCI        (1 << 0) /* no tc interrupt */
#define FTDMAC020_CHAN_CFR_NOERRI       (1 << 1) /* no error interrupt */
#define FTDMAC020_CHAN_CFR_NOABTI       (1 << 2) /* no abort interrupt */
#define FTDMAC020_CHAN_CFR_SRC_HSID(x)  extract32(x, 3, 4) /* handshake id */
#define FTDMAC020_CHAN_CFR_SRC_HSEN     (1 << 7) /* enable dma handshake */
#define FTDMAC020_CHAN_CFR_BUSY         (1 << 8)
#define FTDMAC020_CHAN_CFR_DST_HSID(x)  extract32(x, 9, 4) /* handshake id */
#define FTDMAC020_CHAN_CFR_DST_HSEN     (1 << 13) /* enable dma handshake */
#define FTDMAC020_CHAN_CFR_NLLP(x)  \
    (((x) & 0xf) << 16) /* num. of llp processed */
#define FTDMAC020_CHAN_CFR_MASK         0x3eff

/*
 * Channel data length register
 */
#define FTDMAC020_CHAN_DLR_MASK         0x3fffff

typedef enum {
    IRQ_ALL = 0,
    IRQ_TC,
    IRQ_EA,
    IRQ_NUM,
} Ftdmac020Irq;

typedef struct Ftdmac020State Ftdmac020State;

/**
 * struct Ftdmac020Desc - hardware link list descriptor.
 * @src: source physical address
 * @dst: destination physical addr
 * @next: phsical address to the next link list descriptor
 * @ctrl: control field
 * @size: transfer size
 *
 * should be word aligned
 */
typedef struct {
    uint32_t src;
    uint32_t dst;
    uint32_t next;
    uint32_t ctrl;
    uint32_t size;
} Ftdmac020Desc;

typedef struct {
    int id;
    int burst;
    int llp_nr; /* num. of llp processed */
    int src_bits;
    int dst_bits;
    Ftdmac020State *chip;

    /* hw registers */
    Ftdmac020ChanRegs *regs;
} Ftdmac020Chan;

struct Ftdmac020State {
    SysBusDevice busdev;
    MemoryRegion iomem;
    qemu_irq irq[IRQ_NUM];

    Ftdmac020Chan chan[8];
    qemu_irq      ack[16];
    uint32_t      req;

    QEMUTimer *qtimer;
    DMAContext *dma;

    /* hw registers */
    uint32_t regs[FTDMAC020_REG_SIZE >> 2];
};

#define FTDMAC020(obj) \
    OBJECT_CHECK(Ftdmac020State, obj, TYPE_FTDMAC020)

static void ftdmac020_irq_update(Ftdmac020State *s)
{
    Ftdmac020Regs *regs = (Ftdmac020Regs *)s->regs;
    uint32_t tc = regs->tcisr & FTDMAC020_TCISR_MASK;
    uint32_t ea = regs->eaisr & FTDMAC020_EAISR_MASK;

    /* check TC interrupts */
    qemu_set_irq(s->irq[IRQ_TC], !!tc);

    /* check Error/Abort interrupts */
    qemu_set_irq(s->irq[IRQ_EA], !!ea);

    /* check interrupt summary (TC | Error | Abort) */
    qemu_set_irq(s->irq[IRQ_ALL], !!(tc || ea));
}

static void ftdmac020_chan_setup(Ftdmac020Chan *c, Ftdmac020Desc *d)
{
    Ftdmac020ChanRegs *chan = c->regs;
    uint32_t ctr, set;

    if (d) {
        set = le32_to_cpu(d->ctrl);

        chan->sar = le32_to_cpu(d->src);
        chan->dar = le32_to_cpu(d->dst);
        chan->llr = le32_to_cpu(d->next) & 0xfffffffc;
        chan->dlr = le32_to_cpu(d->size) & 0x003fffff;

        /* clear CTR for incoming linked-list description */
        ctr = chan->ctr & FTDMAC020_CHAN_CTR_LLP_MASK;
        /* tc mask */
        ctr = (set & BIT(28)) ? FTDMAC020_CHAN_CTR_NOTC : 0;
        /* fifo threshold */
        ctr = deposit32(ctr, 24, 3, extract32(set, 29, 3));
        /* source data width */
        ctr = deposit32(ctr, 11, 3, extract32(set, 25, 3));
        /* destination data width */
        ctr = deposit32(ctr, 8, 3, extract32(set, 22, 3));
        /* source addressing mode */
        ctr = deposit32(ctr, 5, 2, extract32(set, 20, 2));
        /* destination addressing mode */
        ctr = deposit32(ctr, 3, 2, extract32(set, 18, 2));

        chan->ctr = ctr;
    } else {
        ctr = chan->ctr;
    }

    /* decode burst size */
    c->burst = FTDMAC020_CHAN_CTR_BURST(ctr);

    /* decode source width */
    c->src_bits = FTDMAC020_CHAN_CTR_SRC_DW(ctr);

    /* decode destination width */
    c->dst_bits = FTDMAC020_CHAN_CTR_DST_DW(ctr);
}

static void ftdmac020_chan_run(Ftdmac020Chan *c)
{
    Ftdmac020State *s = c->chip;
    Ftdmac020Regs *chip = (Ftdmac020Regs *)s->regs;
    Ftdmac020ChanRegs *chan = c->regs;
    Ftdmac020Desc desc;
    hwaddr src, dst;
    uint8_t buf[2048];
    int i, len, delta, src_hs, dst_hs;

    if (!(chan->ctr & FTDMAC020_CHAN_CTR_START)) {
        return;
    }

    ftdmac020_chan_setup(c, NULL);

    /* update channel busy status register */
    chip->cbsr = FTDMAC020_CBSR_CHAN(c->id);
    chan->cfr |= FTDMAC020_CHAN_CFR_BUSY;

    /* src/dst address */
    src = chan->sar;
    dst = chan->dar;

    /* DMA hardware handshake id */
    src_hs = -1;
    dst_hs = -1;
    if (chan->cfr & FTDMAC020_CHAN_CFR_SRC_HSEN) {
        src_hs = FTDMAC020_CHAN_CFR_SRC_HSID(chan->cfr);
    }
    if (chan->cfr & FTDMAC020_CHAN_CFR_DST_HSEN) {
        dst_hs = FTDMAC020_CHAN_CFR_DST_HSID(chan->cfr);
    }

    while (chan->dlr > 0) {
        /*
         * If the corresponding dma request is not yet asserted,
         * then postpone this DMA action.
         */
        if ((src_hs >= 0) && !(s->req & (1 << src_hs))) {
            break;
        }
        if ((dst_hs >= 0) && !(s->req & (1 << dst_hs))) {
            break;
        }

        delta = c->src_bits >> 3;
        len = MIN(sizeof(buf), c->burst * delta);

        /* load data from source into local buffer */
        if (chan->ctr & FTDMAC020_CHAN_CTR_SRC_IO) {
            for (i = 0; i < len; i += delta) {
                dma_memory_read(s->dma, src, buf + i, delta);
            }
        } else {
            dma_memory_read(s->dma, src, buf, len);
            src += len;
        }

        /* DMA hardware handshake */
        if (src_hs >= 0) {
            qemu_set_irq(s->ack[src_hs], 1);
        }

        /* store data into destination from local buffer */
        if (chan->ctr & FTDMAC020_CHAN_CTR_DST_IO) {
            delta = c->dst_bits >> 3;
            for (i = 0; i < len; i += delta) {
                dma_memory_write(s->dma, dst, buf + i, delta);
            }
        } else {
            dma_memory_write(s->dma, dst, buf, len);
            dst += len;
        }

        /* DMA hardware handshake */
        if (dst_hs >= 0) {
            qemu_set_irq(s->ack[dst_hs], 1);
        }

        /* update the channel transfer size/data length */
        chan->dlr -= MIN(len / (c->src_bits >> 3), chan->dlr);

        if (!chan->dlr) {
            /* update the channel transfer status */
            if (!(chan->ctr & FTDMAC020_CHAN_CTR_NOTC)) {
                chip->tcsr |= FTDMAC020_TCSR_CHAN(c->id);
                if (!(chan->cfr & FTDMAC020_CHAN_CFR_NOTCI)) {
                    chip->tcisr |= FTDMAC020_TCISR_CHAN(c->id);
                }
                ftdmac020_irq_update(s);
            }
            c->llp_nr += 1;
            /* load next llp descriptor */
            if (chan->llr & 0xfffffffc) {
                dma_memory_read(s->dma, chan->llr & 0xfffffffc,
                    &desc, sizeof(desc));
                ftdmac020_chan_setup(c, &desc);

                src = chan->sar;
                dst = chan->dar;
            } else {
                /* clear dma start bit */
                chan->ctr &= ~FTDMAC020_CHAN_CTR_START;
            }
        }
    }

    /* update dma src/dst address */
    chan->sar = src;
    chan->dar = dst;

    /* update channel busy status register */
    chan->cfr &= ~FTDMAC020_CHAN_CFR_BUSY;
    chip->cbsr = 0;
}

static void ftdmac020_chan_reset(Ftdmac020Chan *c)
{
    memset(c->regs, 0, FTDMAC020_CHAN_REG_SIZE);
    c->src_bits = 0;
    c->dst_bits = 0;
    c->llp_nr = 0;
    c->burst = 0;
}

static void ftdmac020_timer(void *opaque)
{
    Ftdmac020State *s = FTDMAC020(opaque);
    Ftdmac020Chan *c;
    Ftdmac020ChanRegs *chan;
    int i, jobs, done;

    jobs = 0;
    done = 0;
    for (i = 0; i < 8; ++i) {
        c = s->chan + i;
        chan = c->regs;
        if (chan->ctr & FTDMAC020_CHAN_CTR_START) {
            jobs += 1;
            ftdmac020_chan_run(c);
            done += (chan->ctr & FTDMAC020_CHAN_CTR_START) ? 0 : 1;
        }
    }

    /*
     * Devices those with an unlimited FIFO (always ready for R/W)
     * would trigger next DMA handshake transaction right here.
     * (e.g. ftnandc021, ftsdc010)
     */
    if ((jobs - done) && s->req) {
        qemu_mod_timer(s->qtimer, qemu_get_clock_ns(vm_clock) + 1);
    }
}

static void ftdmac020_dmahs_req(void *opaque, int line, int level)
{
    Ftdmac020State *s = FTDMAC020(opaque);

    if (level) {
        if (!(s->req & BIT(line))) {
        	s->req |= BIT(line);
            qemu_mod_timer(s->qtimer, qemu_get_clock_ns(vm_clock) + 1);
        }
    } else {
        s->req &= ~BIT(line);
        qemu_set_irq(s->ack[line], 0);
    }
}

static void ftdmac020_chip_reset(Ftdmac020State *s)
{
    int i;

    qemu_del_timer(s->qtimer);

    memset(s->regs, 0, FTDMAC020_REG_SIZE);

    for (i = 0; i < 8; ++i) {
        ftdmac020_chan_reset(s->chan + i);
    }

    /* We can assume our GPIO have been wired up now */
    for (i = 0; i < 16; ++i) {
        qemu_set_irq(s->ack[i], 0);
    }
    s->req = 0;
}

static uint64_t ftdmac020_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftdmac020State *s = FTDMAC020(opaque);
    Ftdmac020Regs *chip = (Ftdmac020Regs *)s->regs;
    Ftdmac020Chan  *c = NULL;
    uint32_t i, ret = 0;

    if (addr >= FTDMAC020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftdmac020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case offsetof(Ftdmac020Regs, isr):
        /* Checking TC interrupts */
        ret |= chip->tcisr & FTDMAC020_TCISR_MASK;
        /* Checking Error interrupts */
        ret |= FTDMAC020_EAISR_CERR(chip->eaisr);
        /* Checking Abort interrupts */
        ret |= FTDMAC020_EAISR_CABT(chip->eaisr);
        break;
    case offsetof(Ftdmac020Regs, cesr):
        for (i = 0; i < 8; ++i) {
            c = s->chan + i;
            ret |= (c->regs->ctr & FTDMAC020_CHAN_CTR_START) ? BIT(i) : 0;
        }
        break;
#ifndef CONFIG_MOXART
    case offsetof(Ftdmac020Regs, revr):
        return FTDMAC020_REVISION;
    case offsetof(Ftdmac020Regs, fear):
        return FTDMAC020_FEAR_NUM_CHAN(8) | FTDMAC020_FEAR_SUPP_LLP
            | FTDMAC020_FEAR_BITS_FIFO(5);
#endif
    case FTDMAC020_CHAN_REG_START ... FTDMAC020_CHAN_REG_END:
        c = s->chan + FTDMAC020_CHAN_ID(addr);
        switch (addr & 0x1f) {
        case offsetof(Ftdmac020ChanRegs, cfr):
            ret = s->regs[addr >> 2] | ((c->llp_nr & 0x0f) << 16);
            break;
        default:
            ret = s->regs[addr >> 2];
            break;
        }
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftdmac020_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftdmac020State *s = FTDMAC020(opaque);
    Ftdmac020Regs *chip = (Ftdmac020Regs *)s->regs;
    Ftdmac020Chan  *c = NULL;
    uint32_t tmp;

    if (addr >= FTDMAC020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftdmac020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftdmac020Regs, tciscr):
        chip->tcisr &= ~((uint32_t)val);
        chip->tcsr &= ~((uint32_t)val);
        ftdmac020_irq_update(s);
        break;
    case offsetof(Ftdmac020Regs, eaiscr):
        chip->eaisr &= ~((uint32_t)val);
        chip->easr &= ~((uint32_t)val);
        ftdmac020_irq_update(s);
        break;
    case FTDMAC020_CHAN_REG_START ... FTDMAC020_CHAN_REG_END:
        c = s->chan + FTDMAC020_CHAN_ID(addr);
        switch (addr & 0x1f) {
        case offsetof(Ftdmac020ChanRegs, ctr):
            tmp = s->regs[addr >> 2];
            s->regs[addr >> 2] = (uint32_t)val;
            if ((tmp ^ val) & FTDMAC020_CHAN_CTR_START) {
                if (val & FTDMAC020_CHAN_CTR_START) {
                    c->llp_nr = 0;
                    /* kick-off DMA engine */
                    qemu_mod_timer(s->qtimer, qemu_get_clock_ns(vm_clock) + 1);
                }
            }
            break;
        case offsetof(Ftdmac020ChanRegs, cfr):
            s->regs[addr >> 2] = (uint32_t)val & FTDMAC020_CHAN_CFR_MASK;
            break;
        case offsetof(Ftdmac020ChanRegs, dlr):
            s->regs[addr >> 2] = (uint32_t)val & FTDMAC020_CHAN_DLR_MASK;
            break;
        default:
            s->regs[addr >> 2] = (uint32_t)val;
            break;
        }
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftdmac020_mmio_read,
    .write = ftdmac020_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftdmac020_reset(DeviceState *dev)
{
    ftdmac020_chip_reset(FTDMAC020(dev));
}

static void ftdmac020_realize(DeviceState *dev, Error **errp)
{
    Ftdmac020State *s = FTDMAC020(dev);
    Ftdmac020Regs *regs = (Ftdmac020Regs *)s->regs;
    Ftdmac020Chan *c;
    int i;

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_FTDMAC020,
                          FTDMAC020_REG_SIZE);
    sysbus_init_mmio(SYS_BUS_DEVICE(dev), &s->iomem);
    for (i = 0; i < 3; ++i) {
        sysbus_init_irq(SYS_BUS_DEVICE(dev), &s->irq[i]);
    }
    qdev_init_gpio_in(&s->busdev.qdev, ftdmac020_dmahs_req, 16);
    qdev_init_gpio_out(&s->busdev.qdev, s->ack, 16);

    s->dma = &dma_context_memory;
    s->qtimer = qemu_new_timer_ns(vm_clock, ftdmac020_timer, s);
    for (i = 0; i < 8; ++i) {
        c = s->chan + i;
        c->id = i;
        c->chip = s;
        c->regs = &regs->chan[i];
    }
}

static const VMStateDescription vmstate_ftdmac020 = {
    .name = TYPE_FTDMAC020,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftdmac020State, FTDMAC020_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static void ftdmac020_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftdmac020;
    dc->reset   = ftdmac020_reset;
    dc->realize = ftdmac020_realize;
    dc->no_user = 1;
}

static const TypeInfo ftdmac020_info = {
    .name           = TYPE_FTDMAC020,
    .parent         = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(Ftdmac020State),
    .class_init     = ftdmac020_class_init,
};

static void ftdmac020_register_types(void)
{
    type_register_static(&ftdmac020_info);
}

type_init(ftdmac020_register_types)
