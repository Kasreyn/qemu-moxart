/*
 * Faraday FTAPBBRG020 DMA Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 *
 * Note: The descending address mode is not supported.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "sysemu/dma.h"
#include "sysemu/sysemu.h"
#include "sysemu/blockdev.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"

#define TYPE_FTAPBBRG020        "ftapbbrg020"

#ifndef CONFIG_MOXART
#define FTAPBBRG020_REVISION    FARADAY_REVISION(1, 6, 0)
#endif

typedef struct Ftapbbrg020ChanRegs {
    uint32_t sar;   /* source address register */
    uint32_t dar;   /* destination address register */
    uint32_t dlr;   /* data length register */
    uint32_t cr;    /* control register */
} Ftapbbrg020ChanRegs;

#define FTAPBBRG020_CHAN_REG_SIZE   sizeof(Ftapbbrg020ChanRegs)

typedef struct Ftapbbrg020Regs {
    uint32_t rsvd0[32];
    Ftapbbrg020ChanRegs chan[4];
#ifndef CONFIG_MOXART
    uint32_t rsvd1[2];
    uint32_t revr;  /* revision register */
#endif
} Ftapbbrg020Regs;

#define FTAPBBRG020_REG_SIZE        sizeof(Ftapbbrg020Regs)

#define FTAPBBRG020_CHAN_ID(addr)   (((addr) - 0x80) >> 4)
#define FTAPBBRG020_CHAN_REG_START  0x80
#define FTAPBBRG020_CHAN_REG_END    0xbc

/* Channel Control Register */
#define FTAPBBRG020_CR_START        (1 << 0)
#define FTAPBBRG020_CR_IRQ_DAT      (1 << 1) /* data transfered interrupt */
#define FTAPBBRG020_CR_IRQ_DAT_EN   (1 << 2)
#define FTAPBBRG020_CR_BURST(x)     (((x) & 8) ? 4 : 1)
#define FTAPBBRG020_CR_IRQ_ERR      (1 << 4) /* error interrupt */
#define FTAPBBRG020_CR_IRQ_ERR_EN   (1 << 5)
#define FTAPBBRG020_CR_IRQ  \
    (FTAPBBRG020_CR_IRQ_DAT | FTAPBBRG020_CR_IRQ_ERR)
#define FTAPBBRG020_CR_SAS(x)    \
    (extract32(x, 8, 3) > 2 ? 4 : extract32(x, 8, 3)) /* src address stride */
#define FTAPBBRG020_CR_DAS(x)    \
    (extract32(x, 12, 3) > 2 ? 4 : extract32(x, 12, 3))/* dst address stride */
#define FTAPBBRG020_CR_DREQ(x)  \
    extract32(x, 16, 4) /* dst dma req. id for hardware handshake */
#define FTAPBBRG020_CR_DW(x)        \
    (8 << (2 - MIN(2, extract32(x, 20, 2))))    /* data width (bits) */
#define FTAPBBRG020_CR_SREQ(x)   \
    extract32(x, 24, 4) /* src dma req. id for hardware handshake */

typedef struct Ftapbbrg020State Ftapbbrg020State;

typedef struct Ftapbbrg020Chan {
    int id;
    int burst;
    int src_bits;
    int src_stride;
    int dst_bits;
    int dst_stride;
    Ftapbbrg020State *chip;

    /* hw registers */
    Ftapbbrg020ChanRegs *regs;
} Ftapbbrg020Chan;

struct Ftapbbrg020State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    qemu_irq irq;

    Ftapbbrg020Chan chan[4];
    qemu_irq ack[16];
    uint32_t req;

    QEMUTimer *qtimer;
    DMAContext *dma;

    /* hw registers */
    uint32_t regs[FTAPBBRG020_REG_SIZE >> 2];
};

#define FTAPBBRG020(obj) \
    OBJECT_CHECK(Ftapbbrg020State, obj, TYPE_FTAPBBRG020)

static void ftapbbrg020_irq_update(Ftapbbrg020State *s)
{
    int i, level = 0;
    uint32_t is, im;
    Ftapbbrg020Chan *ch;

    for (i = 0; i < 4; ++i) {
        ch = s->chan + i;
        is = ch->regs->cr & FTAPBBRG020_CR_IRQ;
        im = (ch->regs->cr >> 1) & FTAPBBRG020_CR_IRQ;
        if (is & im) {
            level = 1;
            break;
        }
    }

    qemu_set_irq(s->irq, level);
}

static void ftapbbrg020_chan_prepare(Ftapbbrg020Chan *c)
{
    /* decode burst size */
    c->burst = FTAPBBRG020_CR_BURST(c->regs->cr);

    /* decode source/destination width */
    c->src_bits = c->dst_bits = FTAPBBRG020_CR_DW(c->regs->cr);

    /* decode source address stride */
    c->src_stride = FTAPBBRG020_CR_SAS(c->regs->cr);

    /* decode destination address stride */
    c->dst_stride = FTAPBBRG020_CR_DAS(c->regs->cr);
}

static void ftapbbrg020_chan_run(Ftapbbrg020Chan *c)
{
    Ftapbbrg020State *s = c->chip;
    Ftapbbrg020ChanRegs *regs = c->regs;
    hwaddr src, dst;
    uint8_t buf[2048];
    int i, len, stride, sreq, dreq;

    if (!(regs->cr & FTAPBBRG020_CR_START)) {
        return;
    }

    ftapbbrg020_chan_prepare(c);

    /* DMA src/dst address */
    src = regs->sar;
    dst = regs->dar;

    /* DMA hardware handshake id */
    sreq = FTAPBBRG020_CR_SREQ(regs->cr);
    dreq = FTAPBBRG020_CR_DREQ(regs->cr);

    while (regs->dlr > 0) {
        /*
         * If the corresponding dma request is not yet asserted,
         * then postpone this DMA action.
         */
        if (sreq && !(s->req & (1 << sreq))) {
            break;
        }
        if (dreq && !(s->req & (1 << dreq))) {
            break;
        }

        len = MIN(sizeof(buf), c->burst * (c->src_bits >> 3));

        /* load data from source into local buffer */
        if (c->src_stride) {
            dma_memory_read(s->dma, src, buf, len);
            src += len;
        } else {
            stride = c->src_bits >> 3;
            for (i = 0; i < len; i += stride) {
                dma_memory_read(s->dma, src, buf + i, stride);
            }
        }

        /* dma hardware handshake */
        if (sreq) {
            qemu_set_irq(s->ack[sreq], 1);
        }

        /* store data into destination from local buffer */
        if (c->dst_stride) {
            dma_memory_write(s->dma, dst, buf, len);
            dst += len;
        } else {
            stride = c->dst_bits >> 3;
            for (i = 0; i < len; i += stride) {
                dma_memory_write(s->dma, dst, buf + i, stride);
            }
        }

        /* dma hardware handshake */
        if (dreq) {
            qemu_set_irq(s->ack[dreq], 1);
        }

        /* update the channel transfer size */
        regs->dlr -= MIN(len / (c->src_bits >> 3), regs->dlr);

        if (!regs->dlr) {
            /* update channel interrupt status */
            regs->cr |= FTAPBBRG020_CR_IRQ_DAT;
            /* update chip interrupt status */
            ftapbbrg020_irq_update(s);
            /* clear start bit */
            regs->cr &= ~FTAPBBRG020_CR_START;
        }
    }

    /* update src/dst address */
    regs->sar = src;
    regs->dar = dst;
}

static void ftapbbrg020_timer(void *opaque)
{
    Ftapbbrg020State *s = FTAPBBRG020(opaque);
    Ftapbbrg020Chan *c;
    int i, jobs, done;

    jobs = 0;
    done = 0;
    for (i = 0; i < 4; ++i) {
        c = s->chan + i;
        if (c->regs->cr & FTAPBBRG020_CR_START) {
            jobs += 1;
            ftapbbrg020_chan_run(c);
            done += (c->regs->cr & FTAPBBRG020_CR_START) ? 0 : 1;
        }
    }

    /*
     * Devices those with an unlimited FIFO (always ready for R/W)
     * would trigger next DMA handshake transaction right here.
     * (e.g. ftnandc021, ftsdc010)
     */
    if ((jobs - done) && s->req) {
        qemu_mod_timer(s->qtimer, qemu_get_clock_ns(vm_clock) + 1);
    }
}

static void ftapbbrg020_dmahs_req(void *opaque, int line, int level)
{
    Ftapbbrg020State *s = FTAPBBRG020(opaque);

    if (level) {
        if (!(s->req & BIT(line))) {
            s->req |= BIT(line);
            qemu_mod_timer(s->qtimer, qemu_get_clock_ns(vm_clock) + 1);
        }
    } else {
        s->req &= ~BIT(line);
        qemu_set_irq(s->ack[line], 0);
    }
}

static void ftapbbrg020_chip_reset(Ftapbbrg020State *s)
{
    int i;

    s->req = 0;
    memset(s->regs, 0, FTAPBBRG020_REG_SIZE);

    /* We can assume our GPIO have been wired up now */
    for (i = 0; i < 16; ++i) {
        qemu_set_irq(s->ack[i], 0);
    }
}

static uint64_t ftapbbrg020_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftapbbrg020State *s = FTAPBBRG020(opaque);
    uint32_t ret = 0;

    if (addr >= FTAPBBRG020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftapbbrg020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case FTAPBBRG020_CHAN_REG_START ... FTAPBBRG020_CHAN_REG_END:
        ret = s->regs[addr >> 2];
        break;
#ifndef CONFIG_MOXART
    case offsetof(Ftapbbrg020Regs, revr):
        ret = FTAPBBRG020_REVISION;
        break;
#endif
    /* Don't care registers */
    default:
        break;
    }

    return ret;
}

static void ftapbbrg020_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftapbbrg020State *s = FTAPBBRG020(opaque);
    uint32_t tmp;

    if (addr >= FTAPBBRG020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftapbbrg020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case FTAPBBRG020_CHAN_REG_START ... FTAPBBRG020_CHAN_REG_END:
        switch (addr & 0xf) {
        case offsetof(Ftapbbrg020ChanRegs, cr):
            tmp = s->regs[addr >> 2];
            s->regs[addr >> 2] = (uint32_t)val;
            if (((tmp ^ val) & FTAPBBRG020_CR_START)
                && (val & FTAPBBRG020_CR_START)) {
                /* kick-off DMA engine */
                qemu_mod_timer(s->qtimer, qemu_get_clock_ns(vm_clock) + 1);
            }
            ftapbbrg020_irq_update(s);
            break;
        default:
            s->regs[addr >> 2] = (uint32_t)val;
            break;
        }
        break;
    default:
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftapbbrg020_mmio_read,
    .write = ftapbbrg020_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftapbbrg020_reset(DeviceState *dev)
{
    ftapbbrg020_chip_reset(FTAPBBRG020(dev));
}

static void ftapbbrg020_realize(DeviceState *dev, Error **errp)
{
    Ftapbbrg020State *s = FTAPBBRG020(dev);
    Ftapbbrg020Regs *regs = (Ftapbbrg020Regs *)s->regs;
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    Ftapbbrg020Chan *c;
    int i;

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_FTAPBBRG020,
                          FTAPBBRG020_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    sysbus_init_irq(sbd, &s->irq);
    qdev_init_gpio_in(&sbd->qdev, ftapbbrg020_dmahs_req, 16);
    qdev_init_gpio_out(&sbd->qdev, s->ack, 16);

    s->dma = &dma_context_memory;
    s->qtimer = qemu_new_timer_ns(vm_clock, ftapbbrg020_timer, s);
    for (i = 0; i < 4; ++i) {
        c = s->chan + i;
        c->id = i;
        c->chip = s;
        c->regs = &regs->chan[i];
    }
}

static const VMStateDescription vmstate_ftapbbrg020 = {
    .name = TYPE_FTAPBBRG020,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftapbbrg020State, FTAPBBRG020_REG_SIZE / 4),
        VMSTATE_END_OF_LIST()
    }
};

static void ftapbbrg020_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftapbbrg020;
    dc->reset   = ftapbbrg020_reset;
    dc->realize = ftapbbrg020_realize;
    dc->no_user = 1;
}

static const TypeInfo ftapbbrg020_info = {
    .name          = TYPE_FTAPBBRG020,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftapbbrg020State),
    .class_init    = ftapbbrg020_class_init,
};

static void ftapbbrg020_register_types(void)
{
    type_register_static(&ftapbbrg020_info);
}

type_init(ftapbbrg020_register_types)
