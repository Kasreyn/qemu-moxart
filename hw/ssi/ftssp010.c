/*
 * Faraday FTSSP010 I2S/SPI Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/i2c/i2c.h"
#include "hw/ssi.h"
#include "audio/audio.h"
#include "qemu/bitops.h"
#include "qemu/fifo8.h"
#include "sysemu/sysemu.h"

#include "hw/arm/faraday.h"

#define TYPE_FTSSP010       "ftssp010"
#define FTSSP010_REVISION   FARADAY_REVISION(0x01, 0x19, 0x01)
#define FTSSP010_FIFO_SIZE  16
#define FTSSP010_FLASH_NUM  4

typedef struct Ftssp010Regs {
    uint32_t cr0;       /* control register 0 */
    uint32_t cr1;       /* control register 1 */
    uint32_t cr2;       /* control register 2 */
    uint32_t sr;        /* status register */
    uint32_t icr;       /* interrupt control register */
    uint32_t isr;       /* interrupt status register */
    uint32_t dr;        /* data register */
    uint32_t rsvd[17];
    uint32_t revr;      /* revision register */
    uint32_t fear;      /* feature register */
} Ftssp010Regs;

#define FTSSP010_REG_SIZE   sizeof(Ftssp010Regs)

/*
 * Control register 0
 */
#define FTSSP010_CR0_FFMT(x)        extract32((uint32_t)(x), 12, 3)
#define FTSSP010_CR0_FFMT_SSP       0
#define FTSSP010_CR0_FFMT_SPI       1
#define FTSSP010_CR0_FFMT_MICROWIRE 2
#define FTSSP010_CR0_FFMT_I2S       3
#define FTSSP010_CR0_FLASH          (1 << 11)
#define FTSSP010_CR0_FSDIST(x)      extract32((uint32_t)(x), 8, 2)
#define FTSSP010_CR0_LBM            (1 << 7)  /* Loopback Mode */
#define FTSSP010_CR0_LSB            (1 << 6)
#define FTSSP010_CR0_FSPO           (1 << 5)  /* FS: low active */
#define FTSSP010_CR0_FSJUSTIFY      (1 << 4)
#define FTSSP010_CR0_OPM_SLAVE      (0 << 2)
#define FTSSP010_CR0_OPM_MASTER     (3 << 2)
#define FTSSP010_CR0_OPM_I2S(x)     extract32((uint32_t)(x), 2, 2)
#define FTSSP010_CR0_OPM_I2S_MSST   3  /* Master Stereo */
#define FTSSP010_CR0_OPM_I2S_MSMO   2  /* Master MONO */
#define FTSSP010_CR0_OPM_I2S_SLST   1  /* Slave Stereo */
#define FTSSP010_CR0_OPM_I2S_SLMO   0  /* Slave MONO */
#define FTSSP010_CR0_SCLKPO         (1 << 1)  /* SCLK Polarity */
#define FTSSP010_CR0_SCLKPH         (1 << 0)  /* SLCK Phase */

/*
 * Control Register 1
 */
#define FTSSP010_CR1_PDL(x) \
    extract32((uint32_t)(x), 24, 8) /* padding length for I2S */
#define FTSSP010_CR1_SDL(x) \
    (extract32((uint32_t)(x), 16, 7) + 1)   /* serial data length */
#define FTSSP010_CR1_CLKDIV(x)  ((x) & 0xffff)  /* clock divider */

/*
 * Control Register 2
 */
#define FTSSP010_CR2_FSOS(x)    \
    extract32((uint32_t)(x), 10, 2) /* FS/CS output select */
#define FTSSP010_CR2_FS         (1 << 9)    /* FS/CS pull high */
#define FTSSP010_CR2_TXEN       (1 << 8)    /* Tx Enable */
#define FTSSP010_CR2_RXEN       (1 << 7)    /* Rx Enable */
#define FTSSP010_CR2_RESET      (1 << 6)    /* Chip Reset */
#define FTSSP010_CR2_TXCLR      (1 << 3)    /* TX FIFO Clear */
#define FTSSP010_CR2_RXCLR      (1 << 2)    /* RX FIFO Clear */
#define FTSSP010_CR2_TXDOE      (1 << 1)    /* TX Data Output Enable */
#define FTSSP010_CR2_EN         (1 << 0)    /* Chip Enable */

/*
 * Status Register
 */
#define FTSSP010_SR_TFVE(x)     (((x) & 0x1F) << 12)
#define FTSSP010_SR_RFVE(x)     (((x) & 0x1F) << 4)
#define FTSSP010_SR_BUSY        (1 << 2)
#define FTSSP010_SR_TFNF        (1 << 1)    /* Tx FIFO Not Full */
#define FTSSP010_SR_RFF         (1 << 0)    /* Rx FIFO Full */

/*
 * Interrupt Control Register
 */
#define FTSSP010_ICR_TXTHRES(x) \
    extract32((uint32_t)(x), 12, 5) /* Tx FIFO Threshold */
#define FTSSP010_ICR_RXTHRES(x) \
    extract32((uint32_t)(x), 7, 5)  /* Rx FIFO Threshold */
#define FTSSP010_ICR_TXDMA      0x20      /* TX DMA enable */
#define FTSSP010_ICR_RXDMA      0x10      /* RX DMA enable */
#define FTSSP010_ICR_TX         0x08      /* TX interrupt enable */
#define FTSSP010_ICR_RX         0x04      /* RX interrupt enable */
#define FTSSP010_ICR_TXUR       0x02      /* TX under-run interrupt enable */
#define FTSSP010_ICR_RXOR       0x01      /* RX over-run interrupt enable */

/*
 * Interrupt Status Register
 */
#define FTSSP010_ISR_TX         0x08      /* TX interrupt */
#define FTSSP010_ISR_RX         0x04      /* RX interrupt */
#define FTSSP010_ISR_TXUR       0x02      /* TX under-run interrupt */
#define FTSSP010_ISR_RXOR       0x01      /* RX over-run interrupt */

/*
 * Feature Register
 */
#define FTSSP010_FEAR_FLNR(x)   ((((x) - 1) & 3) << 29) /* num of FS/CS */
#define FTSSP010_FEAR_SUPP_SPI  (1 << 26) /* support SPI */
#define FTSSP010_FEAR_SUPP_I2S  (1 << 25) /* support I2S */
#define FTSSP010_FEAR_TXFIFO(x) ((((x) - 1) & 0xff) << 16) /* tx fifo size */
#define FTSSP010_FEAR_RXFIFO(x) ((((x) - 1) & 0xff) << 8) /* rx fifo size */
#define FTSSP010_FEAR_FIFOBW(x) (((x) - 1) & 0xff) /* fifo width in bits */

typedef struct Ftssp010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    qemu_irq irq;
    SSIBus *spi;
    DeviceState *codec;

    int bits;
    qemu_irq *cs_lines;

    struct {
        Fifo8 fifo;
        int thres;
    } tx;

    struct {
        Fifo8 fifo;
        int thres;
    } rx;

    /* DMA hardware handshake */
    qemu_irq req[2];    /* 0 - Tx, 1 - Rx */

    /* hw registers */
    uint32_t regs[FTSSP010_REG_SIZE >> 2];
} Ftssp010State;

#define FTSSP010(obj) \
    OBJECT_CHECK(Ftssp010State, obj, TYPE_FTSSP010)

/* Update interrupts */
static void ftssp010_irq_update(Ftssp010State *s)
{
    Ftssp010Regs *regs = (Ftssp010Regs *)s->regs;

    if (!(regs->cr2 & FTSSP010_CR2_EN)) {
        return;
    }

    /* update tx status & dma request */
    if (s->tx.thres) {
        if ((s->tx.fifo.num / (s->bits >> 3)) <= s->tx.thres) {
            regs->isr |= FTSSP010_ISR_TX;
            if (regs->icr & FTSSP010_ICR_TXDMA) {
                qemu_set_irq(s->req[0], 1);
            }
        }
    } else {
        regs->isr &= ~FTSSP010_ISR_TX;
    }

    /* update rx status & dma request */
    if (s->rx.thres) {
        switch (FTSSP010_CR0_FFMT(regs->cr0)) {
        case FTSSP010_CR0_FFMT_SPI:
            regs->isr |= FTSSP010_ISR_RX;
            if (regs->icr & FTSSP010_ICR_RXDMA) {
                qemu_set_irq(s->req[1], 1);
            }
            break;
        case FTSSP010_CR0_FFMT_I2S:
            if ((s->rx.fifo.num / (s->bits >> 3)) >= s->rx.thres) {
                regs->isr |= FTSSP010_ISR_RX;
                if (regs->icr & FTSSP010_ICR_RXDMA) {
                    qemu_set_irq(s->req[1], 1);
                }
            }
            break;
        default:
            break;
        }
    } else {
        regs->isr &= ~FTSSP010_ISR_RX;
    }

    /* update the interrupt signal */
    qemu_set_irq(s->irq, !!(regs->icr & regs->isr));
}

static void ftssp010_dmahs_ack(void *opaque, int line, int level)
{
    Ftssp010State *s = FTSSP010(opaque);
    Ftssp010Regs *regs = (Ftssp010Regs *)s->regs;
    int bytes = s->bits >> 3;

    /* Tx DMA Handshake */
    if ((line == 0) && (regs->icr & FTSSP010_ICR_TXDMA)) {
        if (level) {
            qemu_set_irq(s->req[0], 0);
        } else if (s->tx.thres && ((s->tx.fifo.num / bytes) <= s->tx.thres)) {
            qemu_set_irq(s->req[0], 1);
        }
    }

    /* Rx DMA Handshake */
    if ((line == 1) && (regs->icr & FTSSP010_ICR_RXDMA)) {
        if (level) {
            qemu_set_irq(s->req[1], 0);
        } else {
            switch (FTSSP010_CR0_FFMT(regs->cr0)) {
            case FTSSP010_CR0_FFMT_SPI:
                qemu_set_irq(s->req[1], 1);
                break;
            case FTSSP010_CR0_FFMT_I2S:
                if (s->rx.thres && ((s->rx.fifo.num / bytes) >= s->rx.thres)) {
                    qemu_set_irq(s->req[1], 1);
                }
                break;
            default:
                break;
            }
        }
    }
}

void ftssp010_i2s_data_req(void *opaque, int tx, int rx)
{
    int i, len;
    uint32_t sample, mask;
    Ftssp010State *s = FTSSP010(opaque);
    Ftssp010Regs *regs = (Ftssp010Regs *)s->regs;

    if (!s->codec) {
        return;
    }

    if (!(regs->cr2 & FTSSP010_CR2_EN)) {
        return;
    }

    if (FTSSP010_CR0_FFMT(regs->cr0) != FTSSP010_CR0_FFMT_I2S) {
        return;
    }

    mask = FTSSP010_CR2_TXEN | FTSSP010_CR2_TXDOE;
    if ((regs->cr2 & mask) == mask) {
        len = tx * (s->bits >> 3);
        while (!fifo8_is_empty(&s->tx.fifo) && len > 0) {
            sample = 0;
            for (i = 0; i < (s->bits >> 3) && len > 0; i++, len--) {
                sample = deposit32(sample, i * 8, 8, fifo8_pop(&s->tx.fifo));
            }
            wm8731_dac_dat(s->codec, sample);
        }

        if (fifo8_is_empty(&s->tx.fifo) && len > 0) {
            regs->isr |= FTSSP010_ISR_TXUR;
        }
    }

    mask = FTSSP010_CR2_RXEN;
    if ((regs->cr2 & mask) == mask) {
        len = rx * (s->bits >> 3);
        while (!fifo8_is_full(&s->rx.fifo) && len > 0) {
            sample = wm8731_adc_dat(s->codec);
            for (i = 0; i < (s->bits >> 3) && len > 0; i++, len--) {
                fifo8_push(&s->rx.fifo, extract32(sample, i * 8, 8));
            }
        }

        if (fifo8_is_full(&s->rx.fifo) && len > 0) {
            regs->isr |= FTSSP010_ISR_RXOR;
        }
    }

    ftssp010_irq_update(s);
}

static void ftssp010_spi_tx(Ftssp010State *s)
{
    Ftssp010Regs *regs = (Ftssp010Regs *)s->regs;

    if (!(regs->cr2 & FTSSP010_CR2_TXEN)) {
        return;
    }

    if (fifo8_is_empty(&s->tx.fifo)) {
        if (s->tx.thres) {
            regs->isr |= FTSSP010_ISR_TX;
            if (regs->icr & FTSSP010_ICR_TXDMA) {
                qemu_set_irq(s->req[0], 1);
            }
        }
    }

    while (!fifo8_is_empty(&s->tx.fifo)) {
        ssi_transfer(s->spi, fifo8_pop(&s->tx.fifo));
    }
}

static void ftssp010_chip_reset(Ftssp010State *s)
{
    int i;

    for (i = 0; i < 4; ++i) {
        qemu_set_irq(s->cs_lines[i], 1);
    }

    fifo8_reset(&s->tx.fifo);
    fifo8_reset(&s->rx.fifo);

    memset(s->regs, 0, FTSSP010_REG_SIZE);

    ftssp010_irq_update(s);
}

static uint64_t ftssp010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftssp010State *s = FTSSP010(opaque);
    Ftssp010Regs *regs = (Ftssp010Regs *)s->regs;
    uint32_t i, ret = 0;

    if (addr & 3) {
        qemu_log_mask(LOG_GUEST_ERROR,"ftssp010: "
            "Although the byte/half-word access is supported, "
            "the address still needs to be word aligned\n");
        return 0;
    }

    if (addr + size > FTSSP010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftssp010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftssp010Regs, sr):
        /* tx status */
        ret |= FTSSP010_SR_TFVE(s->tx.fifo.num / (s->bits >> 3));
        if (!fifo8_is_full(&s->tx.fifo)) {
            ret |= FTSSP010_SR_TFNF;
        }
        /* rx status */
        switch (FTSSP010_CR0_FFMT(regs->cr0)) {
        case FTSSP010_CR0_FFMT_SPI:
            ret |= FTSSP010_SR_RFF | FTSSP010_SR_RFVE(FTSSP010_FIFO_SIZE);
            break;
        case FTSSP010_CR0_FFMT_I2S:
            ret |= FTSSP010_SR_RFVE(s->rx.fifo.num / (s->bits >> 3));
            if (fifo8_is_full(&s->rx.fifo)) {
                ret |= FTSSP010_SR_RFF;
            }
            break;
        default:
            break;
        }
        break;
    case offsetof(Ftssp010Regs, isr):
        ret = regs->isr;
        regs->isr &= FTSSP010_ISR_RX; /* clear everything except rx irq */
        ftssp010_irq_update(s);
        break;
    case offsetof(Ftssp010Regs, dr):
        i = FTSSP010_CR2_EN | FTSSP010_CR2_RXEN;
        if ((regs->cr2 & i) != i) {
            break;
        }
        switch (FTSSP010_CR0_FFMT(regs->cr0)) {
        case FTSSP010_CR0_FFMT_SPI:
            for (i = 0; i < (s->bits >> 3); i++) {
                ret = deposit32(ret, i * 8, 8, ssi_transfer(s->spi, 0));
            }
            break;
        case FTSSP010_CR0_FFMT_I2S:
            for (i = 0; i < (s->bits >> 3); i++) {
                if (fifo8_is_empty(&s->rx.fifo)) {
                    break;
                }
                ret = deposit32(ret, i * 8, 8, fifo8_pop(&s->rx.fifo));
            }
            break;
        default:
            break;
        }
        ftssp010_irq_update(s);
        break;
    case offsetof(Ftssp010Regs, revr):
        ret = FTSSP010_REVISION;
        break;
    case offsetof(Ftssp010Regs, fear):
        ret = FTSSP010_FEAR_FLNR(FTSSP010_FLASH_NUM)
            | FTSSP010_FEAR_SUPP_SPI
            | FTSSP010_FEAR_SUPP_I2S
            | FTSSP010_FEAR_TXFIFO(FTSSP010_FIFO_SIZE)
            | FTSSP010_FEAR_RXFIFO(FTSSP010_FIFO_SIZE)
            | FTSSP010_FEAR_FIFOBW(32);
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftssp010_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftssp010State *s = FTSSP010(opaque);
    Ftssp010Regs *regs = (Ftssp010Regs *)s->regs;
    int i;

    if (addr & 3) {
        qemu_log_mask(LOG_GUEST_ERROR, "ftssp010: "
            "Although the byte/half-word access is supported, "
            "the address still needs to be word aligned\n");
        return;
    }

    if (addr + size > FTSSP010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftssp010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftssp010Regs, cr0):
        switch (FTSSP010_CR0_FFMT(val)) {
        case FTSSP010_CR0_FFMT_SPI:
        case FTSSP010_CR0_FFMT_I2S:
            break;
        default:
            qemu_log_mask(LOG_GUEST_ERROR, "ftssp010: unknown protocol=%d\n",
                (int)FTSSP010_CR0_FFMT(val));
            break;
        }
        regs->cr0 = (uint32_t)val;
        break;
    case offsetof(Ftssp010Regs, cr1):
        s->bits = FTSSP010_CR1_SDL(val);
        regs->cr1 = (uint32_t)val;
        break;
    case offsetof(Ftssp010Regs, cr2):
        if (val & FTSSP010_CR2_RESET) {
            ftssp010_chip_reset(s);
            break;
        }
        if (val & FTSSP010_CR2_TXCLR) {
            fifo8_reset(&s->tx.fifo);
            val &= ~FTSSP010_CR2_TXCLR;
        }
        if (val & FTSSP010_CR2_RXCLR) {
            fifo8_reset(&s->rx.fifo);
            val &= ~FTSSP010_CR2_RXCLR;
        }
        if (regs->cr0 & FTSSP010_CR0_FLASH) {
            qemu_set_irq(s->cs_lines[FTSSP010_CR2_FSOS(val)],
                !!(val & FTSSP010_CR2_FS));
        }
        regs->cr2 = (uint32_t)val;
        if (val & FTSSP010_CR2_EN) {
            switch (FTSSP010_CR0_FFMT(regs->cr0)) {
            case FTSSP010_CR0_FFMT_SPI:
                ftssp010_spi_tx(s);
                break;
            case FTSSP010_CR0_FFMT_I2S:
                /* nothing needs to be done */
                break;
            default:
                break;
            }
        }
        ftssp010_irq_update(s);
        break;
    case offsetof(Ftssp010Regs, icr):
        s->tx.thres = FTSSP010_ICR_TXTHRES(val);
        s->rx.thres = FTSSP010_ICR_RXTHRES(val);
        regs->icr = (uint32_t)val;
        break;
    case offsetof(Ftssp010Regs, dr):
        if (!(regs->cr2 & FTSSP010_CR2_EN)) {
            break;
        }
        for (i = 0; i < (s->bits >> 3) && !fifo8_is_full(&s->tx.fifo); i++) {
            fifo8_push(&s->tx.fifo, extract32((uint32_t)val, i * 8, 8));
        }
        switch (FTSSP010_CR0_FFMT(regs->cr0)) {
        case FTSSP010_CR0_FFMT_SPI:
            ftssp010_spi_tx(s);
            break;
        case FTSSP010_CR0_FFMT_I2S:
            /* nothing needs to be done */
            break;
        default:
            break;
        }
        ftssp010_irq_update(s);
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftssp010_mmio_read,
    .write = ftssp010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 1,
        .max_access_size = 4
    }
};

static void ftssp010_reset(DeviceState *dev)
{
    Ftssp010State *s = FTSSP010(dev);
    Error *errp = NULL;

    s->codec = DEVICE(object_property_get_link(OBJECT(s), "codec", &errp));
    if (errp) {
        fprintf(stderr, "ftssp010: Unable to get codec link\n");
        abort();
    }

    ftssp010_chip_reset(s);
}

static void ftssp010_realize(DeviceState *dev, Error **errp)
{
    Ftssp010State *s = FTSSP010(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    int i;

    s->spi = ssi_create_bus(&sbd->qdev, "spi");

    fifo8_create(&s->tx.fifo, FTSSP010_FIFO_SIZE * 4);
    fifo8_create(&s->rx.fifo, FTSSP010_FIFO_SIZE * 4);

    memory_region_init_io(&s->mmio, &mmio_ops, s,
        TYPE_FTSSP010, FTSSP010_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);

    s->cs_lines = g_new0(qemu_irq, FTSSP010_FLASH_NUM);
    ssi_auto_connect_slaves(dev, s->cs_lines, s->spi);
    for (i = 0; i < FTSSP010_FLASH_NUM; ++i) {
        sysbus_init_irq(sbd, &s->cs_lines[i]);
    }

    /* DMA hardware handshake */
    qdev_init_gpio_in(&sbd->qdev, ftssp010_dmahs_ack, 2);
    qdev_init_gpio_out(&sbd->qdev, s->req, 2);
}

static const VMStateDescription vmstate_ftssp010 = {
    .name = TYPE_FTSSP010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftssp010State, FTSSP010_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static void ftssp010_instance_init(Object *obj)
{
    Ftssp010State *s = FTSSP010(obj);

    object_property_add_link(obj, "codec", TYPE_DEVICE,
        (Object **)&s->codec, NULL);
}

static void ftssp010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftssp010;
    dc->reset   = ftssp010_reset;
    dc->realize = ftssp010_realize;
    dc->no_user = 1;
}

static const TypeInfo ftssp010_info = {
    .name          = TYPE_FTSSP010,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftssp010State),
    .instance_init = ftssp010_instance_init,
    .class_init    = ftssp010_class_init,
};

static void ftssp010_register_types(void)
{
    type_register_static(&ftssp010_info);
}

type_init(ftssp010_register_types)
