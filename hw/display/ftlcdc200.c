/*
 * Faraday FTLCDC200 Color LCD Controller
 *
 * base is pl110.c
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This code is licensed under the GNU GPL v2+
 */

#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "ui/console.h"
#include "ui/pixel_ops.h"
#include "qemu/bitops.h"
#include "framebuffer.h"

#define TYPE_FTLCDC200      "ftlcdc200"

typedef struct Ftlcdc200Regs {
	/* 0x000 ~ 0x0ff */
	uint32_t fer;  /* 0x000: Function Enable Register */
	uint32_t ppr;  /* 0x004: Panel Pixel Register */
	uint32_t ier;  /* 0x008: Interrupt Enable Register */
	uint32_t iscr; /* 0x00C: Interrupt Status Clear Register */
	uint32_t isr;  /* 0x010: Interrupt Status Register */
	uint32_t rsvd0[1];
	uint32_t fbr0; /* 0x018: Framebuffer Base Register 0 */
	uint32_t rsvd1[2];
	uint32_t fbr1; /* 0x024: Framebuffer Base Register 1 */
	uint32_t rsvd2[2];
	uint32_t fbr2; /* 0x030: Framebuffer Base Register 2 */
	uint32_t rsvd3[2];
	uint32_t fbr3; /* 0x03C: Framebuffer Base Register 3 */
	uint32_t rsvd4[2];
	uint32_t pgr;  /* 0x048: Pattern Generator */
	uint32_t fifo; /* 0x04C: FIFO Threshold */
	uint32_t gpio; /* 0x050: GPIO */
	uint32_t rsvd5[43];

	/* 0x100 ~ 0x1ff */
	uint32_t htcr;    /* Horizontal Timing Control Register */
	uint32_t vtcr[2]; /* Vertical Timing Control Register */
	uint32_t pcr;     /* Polarity Control Register */
	uint32_t rsvd6[60];

	/* 0x200 ~ 0x2ff */
	uint32_t sppr;    /* Serial Panel Pixel Register */
	uint32_t ccir;    /* CCIR565 Register */
	uint32_t rsvd7[62];

	/* 0x300 ~ 0x3ff */
	uint32_t pipr;    /* Picture-In-Picture Register */
	uint32_t pip1pos; /* Sub-picture 1 position */
	uint32_t pip1dim; /* Sub-picture 1 dimension */
	uint32_t pip2pos; /* Sub-picture 2 position */
	uint32_t pip2dim; /* Sub-picture 2 dimension */
	uint32_t rsvd8[59];

	/* 0x400 ~ 0x5ff */
	uint32_t cmnt[4]; /* Color Management */
	uint32_t rsvd9[124];

	/* 0x600 ~ 0x6ff */
	uint32_t gamma_r[64]; /* RED - Gamma Correct */

	/* 0x700 ~ 0x7ff */
	uint32_t gamma_g[64]; /* GREEN - Gamma Correct */

	/* 0x800 ~ 0x8ff */
	uint32_t gamma_b[64]; /* BLUE - Gamma Correct */

	/* 0x900 ~ 0x9ff */
	uint32_t rsvd10[64];

	/* 0xa00 ~ 0xbff */
	uint32_t palette[128];  /* Palette Write Port */

	/* 0xc00 ~ 0xcff */
	uint32_t cstn_cr;       /* CSTN Control Register */
	uint32_t cstn_pr;       /* CSTN Parameter Register */
	uint32_t rsvd11[62];

	/* 0xd00 ~ 0xdff */
	uint32_t cstn_bmap[16]; /* CSTN bitmap write port */
	uint32_t rsvd12[48];
} Ftlcdc200Regs;

#define FTLCDC200_REG_SIZE          sizeof(Ftlcdc200Regs)

#define FTLCDC200_PALETTE_REG_SATRT offsetof(Ftlcdc200Regs, palette)
#define FTLCDC200_PALETTE_REG_END   (offsetof(Ftlcdc200Regs, cstn_cr) - 4)

/* LCD Function Enable Register */
#define FTLCDC200_FER_YCbCr     (1 << 3)
#define FTLCDC200_FER_ON        (1 << 1) /* power-on screen */
#define FTLCDC200_FER_EN        (1 << 0) /* enable chip */

/* LCD Panel Pixel Register */
#define FTLCDC200_PPR_BPP1      0
#define FTLCDC200_PPR_BPP2      1
#define FTLCDC200_PPR_BPP4      2
#define FTLCDC200_PPR_BPP8      3
#define FTLCDC200_PPR_BPP16     4
#define FTLCDC200_PPR_BPP24     5
#define FTLCDC200_PPR_BPPMASK   7
#define FTLCDC200_PPR_RGB1      FTLCDC200_PPR_BPP1
#define FTLCDC200_PPR_RGB2      FTLCDC200_PPR_BPP2
#define FTLCDC200_PPR_RGB4      FTLCDC200_PPR_BPP4
#define FTLCDC200_PPR_RGB8      FTLCDC200_PPR_BPP8
#define FTLCDC200_PPR_RGB12     (FTLCDC200_PPR_BPP16 | (2 << 7))
#define FTLCDC200_PPR_RGB555    (FTLCDC200_PPR_BPP16 | (1 << 7))
#define FTLCDC200_PPR_RGB565    (FTLCDC200_PPR_BPP16 | (0 << 7))
#define FTLCDC200_PPR_RGB24     FTLCDC200_PPR_BPP24
#define FTLCDC200_PPR_RGB32     FTLCDC200_PPR_BPP24
#define FTLCDC200_PPR_RGBMASK   (FTLCDC200_PPR_BPPMASK | (3 << 7))

/* LCD Interrupt Enable Register */
#define FTLCDC200_IER_FIFO      (1 << 0) /* fifo under-run */
#define FTLCDC200_IER_NEXT      (1 << 1) /* next frame base loaded */
#define FTLCDC200_IER_VS        (1 << 2) /* V Status */
#define FTLCDC200_IER_BUS       (1 << 3) /* bus error */

/* LCD Interrupt Status Register */
#define FTLCDC200_ISR_FIFO      (1 << 0)
#define FTLCDC200_ISR_NEXT      (1 << 1)
#define FTLCDC200_ISR_VS        (1 << 2)
#define FTLCDC200_ISR_BUS       (1 << 3)

/* LCD Horizontal Timing Control Register */
#define FTLCDC200_HTCR_PL(x)    \
    ((extract32((uint32_t)(x), 0, 8) + 1) << 4) /* pixels per line */

/* LCD Vertical Timing Control Register 0 */
#define FTLCDC200_VTCR0_LF(x)   \
    (extract32((uint32_t)(x), 0, 12) + 1) /* lines per frame */

typedef enum {
    IRQ_COMBINED = 0,
    IRQ_VS,
    IRQ_NEXT,
    IRQ_FIFO,
    IRQ_BUS,
    IRQ_NUMBER,
} Ftlcdc200IRQ;

typedef enum {
    BPP_1 = 0,
    BPP_2,
    BPP_4,
    BPP_8,
    BPP_16,
    BPP_32,
    BPP_16_565,
    BPP_12,
} Ftlcdc200Bpp;

typedef struct Ftlcdc200State {
    SysBusDevice busdev;
    MemoryRegion iomem;
    QemuConsole *con;

    qemu_irq irq[IRQ_NUMBER];
    Ftlcdc200Bpp bpp;
    int cols;
    int rows;
    int invalidate;
    uint32_t palette[256];

    /* hw registers */
    uint32_t regs[FTLCDC200_REG_SIZE << 2];
} Ftlcdc200State;

#define FTLCDC200(obj) \
    OBJECT_CHECK(Ftlcdc200State, obj, TYPE_FTLCDC200)

#define BITS 8
#include "ftlcdc200_template.h"
#define BITS 15
#include "ftlcdc200_template.h"
#define BITS 16
#include "ftlcdc200_template.h"
#define BITS 24
#include "ftlcdc200_template.h"
#define BITS 32
#include "ftlcdc200_template.h"

static int ftlcdc200_enabled(Ftlcdc200State *s)
{
    Ftlcdc200Regs *regs = (Ftlcdc200Regs *)s->regs;

    return (regs->fer & FTLCDC200_FER_EN)
        && (regs->fer & FTLCDC200_FER_ON)
        && s->bpp
        && s->cols
        && s->rows
        && regs->fbr0;
}

/* Update interrupts.  */
static void ftlcdc200_irq_update(Ftlcdc200State *s)
{
    Ftlcdc200Regs *regs = (Ftlcdc200Regs *)s->regs;
    uint32_t mask = regs->ier & regs->isr;

    if (mask) {
        qemu_set_irq(s->irq[IRQ_COMBINED], 1);
        qemu_set_irq(s->irq[IRQ_FIFO],  !!(mask & FTLCDC200_ISR_FIFO));
        qemu_set_irq(s->irq[IRQ_NEXT], !!(mask & FTLCDC200_ISR_NEXT));
        qemu_set_irq(s->irq[IRQ_VS], !!(mask & FTLCDC200_ISR_VS));
        qemu_set_irq(s->irq[IRQ_BUS],  !!(mask & FTLCDC200_ISR_BUS));
    } else {
        qemu_set_irq(s->irq[IRQ_COMBINED], 0);
        qemu_set_irq(s->irq[IRQ_VS], 0);
        qemu_set_irq(s->irq[IRQ_NEXT], 0);
        qemu_set_irq(s->irq[IRQ_FIFO], 0);
        qemu_set_irq(s->irq[IRQ_BUS], 0);
    }
}

static void ftlcdc200_update_display(void *opaque)
{
    Ftlcdc200State *s = FTLCDC200(opaque);
    Ftlcdc200Regs *regs = (Ftlcdc200Regs *)s->regs;
    DisplaySurface *surface = qemu_console_surface(s->con);
    drawfn *fntable;
    drawfn fn;
    int dst_width;
    int src_width;
    int bpp_offset;
    int first;
    int last;

    if (!ftlcdc200_enabled(s)) {
        return;
    }

    switch (surface_bits_per_pixel(surface)) {
    case 8:
        fntable = ftlcdc200_draw_fn_8;
        dst_width = 1;
        break;
    case 15:
        fntable = ftlcdc200_draw_fn_15;
        dst_width = 2;
        break;
    case 16:
        fntable = ftlcdc200_draw_fn_16;
        dst_width = 2;
        break;
    case 24:
        fntable = ftlcdc200_draw_fn_24;
        dst_width = 3;
        break;
    case 32:
        fntable = ftlcdc200_draw_fn_32;
        dst_width = 4;
        break;
    default:
        qemu_log_mask(LOG_GUEST_ERROR, "ftlcdc200: invalid bpp=%d\n",
            (int)surface_bits_per_pixel(surface));
        return;
    }

    bpp_offset = 0;
    fn = fntable[s->bpp + bpp_offset];

    src_width = s->cols;
    switch (s->bpp) {
    case BPP_1:
        src_width >>= 3;
        break;
    case BPP_2:
        src_width >>= 2;
        break;
    case BPP_4:
        src_width >>= 1;
        break;
    case BPP_8:
        break;
    case BPP_16:
    case BPP_16_565:
    case BPP_12:
        src_width <<= 1;
        break;
    case BPP_32:
        src_width <<= 2;
        break;
    }
    dst_width *= s->cols;
    first = 0;
    framebuffer_update_display(surface, sysbus_address_space(&s->busdev),
                               regs->fbr0, s->cols, s->rows,
                               src_width, dst_width, 0,
                               s->invalidate,
                               fn, s->palette,
                               &first, &last);
    if (regs->ier & (FTLCDC200_IER_VS | FTLCDC200_IER_NEXT)) {
        regs->isr |= (FTLCDC200_ISR_VS | FTLCDC200_ISR_NEXT);
        ftlcdc200_irq_update(s);
    }
    if (first >= 0) {
        dpy_gfx_update(s->con, 0, first, s->cols, last - first + 1);
    }
    s->invalidate = 0;
}

static void ftlcdc200_invalidate_display(void *opaque)
{
    Ftlcdc200State *s = FTLCDC200(opaque);

    s->invalidate = 1;
    if (ftlcdc200_enabled(s)) {
        qemu_console_resize(s->con, s->cols, s->rows);
    }
}

static void ftlcdc200_update_palette(Ftlcdc200State *s, int n)
{
    DisplaySurface *surface = qemu_console_surface(s->con);
    Ftlcdc200Regs *regs = (Ftlcdc200Regs *)s->regs;
    uint32_t i, r, g, b, raw;

    raw = regs->palette[n];
    n <<= 1;
    for (i = 0; i < 2; i++) {
        r = extract32(raw,  0, 5) << 3;
        g = extract32(raw,  5, 5) << 3;
        b = extract32(raw, 10, 5) << 3;
        /* The I bit is ignored.  */
        raw >>= 6;
        switch (surface_bits_per_pixel(surface)) {
        case 8:
            s->palette[n] = rgb_to_pixel8(r, g, b);
            break;
        case 15:
            s->palette[n] = rgb_to_pixel15(r, g, b);
            break;
        case 16:
            s->palette[n] = rgb_to_pixel16(r, g, b);
            break;
        case 24:
        case 32:
            s->palette[n] = rgb_to_pixel32(r, g, b);
            break;
        default:
            break;
        }
        n++;
    }
}

static void ftlcdc200_resize(Ftlcdc200State *s, int width, int height)
{
    if (width != s->cols || height != s->rows) {
        if (ftlcdc200_enabled(s)) {
            qemu_console_resize(s->con, width, height);
        }
    }
    s->cols = width;
    s->rows = height;
}

static uint64_t ftlcdc200_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftlcdc200State *s = FTLCDC200(opaque);
    uint32_t ret = 0;

    if (addr >= FTLCDC200_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftlcdc200: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    /* write-only registers */
    case offsetof(Ftlcdc200Regs, iscr):
    case FTLCDC200_PALETTE_REG_SATRT ... FTLCDC200_PALETTE_REG_END:
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftlcdc200_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftlcdc200State *s = FTLCDC200(opaque);
    Ftlcdc200Regs *regs = (Ftlcdc200Regs *)s->regs;

    if (addr >= FTLCDC200_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftlcdc200: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    /* For simplicity invalidate the display whenever a control register
       is written to.  */
    s->invalidate = 1;

    switch (addr) {
    case offsetof(Ftlcdc200Regs, fer):
        s->regs[addr >> 2] = (uint32_t)val;
        if (ftlcdc200_enabled(s)) {
            qemu_console_resize(s->con, s->cols, s->rows);
        }
        if (val & FTLCDC200_FER_YCbCr) {
            qemu_log_mask(LOG_GUEST_ERROR,
                "ftlcdc200: YCbCr is not yet implemented\n");
        }
        break;
    case offsetof(Ftlcdc200Regs, ppr):
        s->regs[addr >> 2] = (uint32_t)val;
        switch (val & FTLCDC200_PPR_RGBMASK) {
        case FTLCDC200_PPR_RGB1:
            s->bpp = BPP_1;
            break;
        case FTLCDC200_PPR_RGB2:
            s->bpp = BPP_2;
            break;
        case FTLCDC200_PPR_RGB4:
            s->bpp = BPP_4;
            break;
        case FTLCDC200_PPR_RGB8:
            s->bpp = BPP_8;
            break;
        case FTLCDC200_PPR_RGB12:
            s->bpp = BPP_12;
            break;
        case FTLCDC200_PPR_RGB555:
            s->bpp = BPP_16;
            break;
        case FTLCDC200_PPR_RGB565:
            s->bpp = BPP_16_565;
            break;
        default:
            s->bpp = BPP_32;
            break;
        }
        if (ftlcdc200_enabled(s)) {
            qemu_console_resize(s->con, s->cols, s->rows);
        }
        break;
    case offsetof(Ftlcdc200Regs, ier):
        s->regs[addr >> 2] = (uint32_t)val;
        ftlcdc200_irq_update(s);
        break;
    case offsetof(Ftlcdc200Regs, iscr):
        regs->isr &= (uint32_t)(~val);
        ftlcdc200_irq_update(s);
        break;
    case offsetof(Ftlcdc200Regs, htcr):
        s->regs[addr >> 2] = (uint32_t)val;
        ftlcdc200_resize(s, FTLCDC200_HTCR_PL(val), s->rows);
        break;
    case offsetof(Ftlcdc200Regs, vtcr):
        s->regs[addr >> 2] = (uint32_t)val;
        ftlcdc200_resize(s, s->cols, FTLCDC200_VTCR0_LF(val));
        break;
    case FTLCDC200_PALETTE_REG_SATRT ... FTLCDC200_PALETTE_REG_END:
        s->regs[addr >> 2] = (uint32_t)val;
        ftlcdc200_update_palette(s, (addr - FTLCDC200_PALETTE_REG_SATRT) / 4);
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftlcdc200_mmio_read,
    .write = ftlcdc200_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void ftlcdc200_reset(DeviceState *dev)
{
    Ftlcdc200State *s = FTLCDC200(dev);
    int i;

    memset(s->regs, 0, FTLCDC200_REG_SIZE);
    memset(s->palette, 0, sizeof(s->palette));
    s->bpp = 0;
    s->cols = 0;
    s->rows = 0;
    s->invalidate = 1;

    for (i = 0; i < IRQ_NUMBER; ++i) {
        qemu_set_irq(s->irq[i], 0);
    }

    /* Make sure we redraw, and at the right size */
    ftlcdc200_invalidate_display(s);
}

static const GraphicHwOps ftlcdc200_gfx_ops = {
    .invalidate  = ftlcdc200_invalidate_display,
    .gfx_update  = ftlcdc200_update_display,
};

static void ftlcdc200_realize(DeviceState *dev, Error **errp)
{
    Ftlcdc200State *s = FTLCDC200(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    int i;

    memory_region_init_io(&s->iomem, &mmio_ops, s,
        TYPE_FTLCDC200, FTLCDC200_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    for (i = 0; i < IRQ_NUMBER; ++i) {
        sysbus_init_irq(sbd, &s->irq[i]);
    }
    s->con = graphic_console_init(dev, &ftlcdc200_gfx_ops, s);
}

static int ftlcdc200_post_load(void *opaque, int version_id)
{
    /* Make sure we redraw, and at the right size */
    ftlcdc200_invalidate_display(FTLCDC200(opaque));
    return 0;
}

static const VMStateDescription ftlcdc200_vmst = {
    .name = TYPE_FTLCDC200,
    .version_id = 1,
    .minimum_version_id = 1,
    .post_load = ftlcdc200_post_load,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftlcdc200State, FTLCDC200_REG_SIZE << 2),
        VMSTATE_UINT32_ARRAY(palette, Ftlcdc200State, 256),
        VMSTATE_END_OF_LIST()
    }
};

static void ftlcdc200_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset   = ftlcdc200_reset;
    dc->vmsd    = &ftlcdc200_vmst;
    dc->realize = ftlcdc200_realize;
    dc->no_user = 1;
}

static const TypeInfo ftlcdc200_info = {
    .name          = TYPE_FTLCDC200,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftlcdc200State),
    .class_init    = ftlcdc200_class_init,
};

static void ftlcdc200_register_types(void)
{
    type_register_static(&ftlcdc200_info);
}

type_init(ftlcdc200_register_types)
