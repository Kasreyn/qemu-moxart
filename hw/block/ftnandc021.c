/*
 * QEMU model for Faraday FTNANDC021 NAND Flash Controller
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "hw/devices.h"
#include "hw/block/flash.h"
#include "hw/arm/faraday.h"
#include "sysemu/blockdev.h"
#include "qemu/bitops.h"

#define TYPE_FTNANDC021     "ftnandc021"

#define FTNANDC021_REVISION FARADAY_REVISION(1, 1, 0)

typedef struct Ftnandc021Regs {
    /* 0x000 ~ 0x0fc */
    uint32_t eccpr[4];/* ECC Parity Register */
    uint32_t eccsr;   /* ECC Status Register */
    uint32_t rsvd0[59];

    /* 0x100 ~ 0x1fc */
    uint32_t sr;    /* Status Register */
    uint32_t acr;   /* Access Control Register */
    uint32_t fcr;   /* Flow Control Register */
    uint32_t pir;   /* Page Index Register */
    uint32_t mcr;   /* Memory Configuration Register */
    uint32_t atr[2];/* AC Timing Register */
    uint32_t rsvd1[1];
    uint32_t idr[2];/* Device ID Register */
    uint32_t ier;   /* Interrupt Enable Register */
    uint32_t iscr;  /* Interrupt Status Clear Register */
    uint32_t rsvd2[4];
    uint32_t bbiwr; /* Bad Block Info Write */
    uint32_t lsn;   /* LSN Initialize */
    uint32_t crcwr; /* LSN CRC Write */
    uint32_t lsnwr; /* LSN Write */
    uint32_t bbird; /* Bad Block Info Read */
    uint32_t lsnrd; /* LSN Read */
    uint32_t crcrd; /* CRC Read */
    uint32_t rsvd3[41];

    /* 0x200 ~ 0x2fc */
    uint32_t rsvd4[1];
    uint32_t icr;   /* BMC Interrupt Control Register */
    uint32_t ior;   /* BMC PIO Status Register */
    uint32_t bcr;   /* BMC Burst Control Register */
    uint32_t rsvd5[60];

    /* 0x300 ~ 0x3fc */
    uint32_t dr;    /* MLC Data Register */
    uint32_t isr;   /* MLC Interrupt Status Register */
    uint32_t pcr;   /* Page Count Register, it's always 1 */
    uint32_t srr;   /* MLC Software Reset Register */
    uint32_t rsvd7[58];
    uint32_t revr;  /* Revision Register */
    uint32_t cfgr;  /* Configuration Register */
} Ftnandc021Regs;

#define FTNANDC021_REG_SIZE     sizeof(Ftnandc021Regs)

/* ECC Status Register */
#define FTNANDC021_ESR_CE       (1 << 3)  /* ecc correction error */
#define FTNANDC021_ESR_ERR      (1 << 2)  /* ecc error */
#define FTNANDC021_ESR_DEC      (1 << 1)  /* ecc decode finished */
#define FTNANDC021_ESR_ENC      (1 << 0)  /* ecc encode finished */

/* Status Register */
#define FTNANDC021_SR_BLANK     (1 << 7)  /* blanking check failed */
#define FTNANDC021_SR_ECC       (1 << 6)  /* ecc timeout */
#define FTNANDC021_SR_ST        (1 << 4)  /* status error */
#define FTNANDC021_SR_CRC       (1 << 3)  /* crc error */
#define FTNANDC021_SR_CMD       (1 << 2)  /* command finished */
#define FTNANDC021_SR_READY     (1 << 1)  /* chip ready/busy */
#define FTNANDC021_SR_EN        (1 << 0)  /* chip enabled */
#define FTNANDC021_SR_TO_IER(x) (((x) >> 2) & 0xf)

/* Access Control Register */
#define FTNANDC021_ACR_CMD(x)   extract32((uint32_t)(x), 8, 5)
#define FTNANDC021_ACR_START    (1 << 7)  /* command start */

/* Flow Control Register */
#define FTNANDC021_FCR_SWCRC    (1 << 8)  /* CRC controlled by Software */
#define FTNANDC021_FCR_IGNCRC   (1 << 7)  /* Bypass/Ignore CRC checking */
#define FTNANDC021_FCR_16BIT    (1 << 4)  /* 16 bit data bus */
#define FTNANDC021_FCR_WPROT    (1 << 3)  /* write protected */
#define FTNANDC021_FCR_NOSC     (1 << 2)  /* bypass status check error */
#define FTNANDC021_FCR_MICRON   (1 << 1)  /* Micron 2-plane command */
#define FTNANDC021_FCR_NOBC     (1 << 0)  /* skip blanking check error */

/* Interrupt Enable Register */
#define FTNANDC021_IER_IRQ      (1 << 7)  /* interrupt enabled */
#define FTNANDC021_IER_ECC      (1 << 3)  /* ecc error timeout */
#define FTNANDC021_IER_ST       (1 << 2)  /* status error */
#define FTNANDC021_IER_CRC      (1 << 1)  /* crc error */
#define FTNANDC021_IER_CMD      (1 << 0)  /* command finished */
#define FTNANDC021_IER_MASK     0x8f
#define FTNANDC021_IER_TO_SR(x) (((x) & 0xf) << 2)

/* Interrupt Status Clear Register */
#define FTNANDC021_ISCR_ECC         FTNANDC021_IER_ECC
#define FTNANDC021_ISCR_ST          FTNANDC021_IER_ST
#define FTNANDC021_ISCR_CRC         FTNANDC021_IER_CRC
#define FTNANDC021_ISCR_CMD         FTNANDC021_IER_CMD
#define FTNANDC021_ISCR_TO_SR(x)    FTNANDC021_IER_TO_SR(x)

/* BMC PIO Status Register */
#define FTNANDC021_IOR_READY        (1 << 0)  /* PIO ready */

/* MLC Software Reset Register */
#define FTNANDC021_SRR_ECC          (1 << 8)  /* ECC enabled */
#define FTNANDC021_SRR_NC_RESET     (1 << 2)  /* NANDC reset */
#define FTNANDC021_SRR_BMC_RESET    (1 << 1)  /* BMC reset */
#define FTNANDC021_SRR_ECC_RESET    (1 << 0)  /* ECC reset */
#define FTNANDC021_SRR_RESET        (FTNANDC021_SRR_NC_RESET \
    | FTNANDC021_SRR_BMC_RESET | FTNANDC021_SRR_ECC_RESET)

/* Memory Configuration Register */
#define FTNANDC021_MCR_BS16P        (0 << 16) /* page count per block */
#define FTNANDC021_MCR_BS32P        (1 << 16)
#define FTNANDC021_MCR_BS64P        (2 << 16)
#define FTNANDC021_MCR_BS128P       (3 << 16)
#define FTNANDC021_MCR_1PLANE       (0 << 14) /* memory architecture */
#define FTNANDC021_MCR_2PLANE       (1 << 14)
#define FTNANDC021_MCR_SERIAL       (0 << 12) /* interleaving: off, 2 flash, 4 flash */
#define FTNANDC021_MCR_IL2          (1 << 12)
#define FTNANDC021_MCR_IL4          (2 << 12)
#define FTNANDC021_MCR_ALEN3        (0 << 10) /* address length */
#define FTNANDC021_MCR_ALEN4        (1 << 10)
#define FTNANDC021_MCR_ALEN5        (2 << 10)
#define FTNANDC021_MCR_PS512        (0 << 8)  /* size per page (bytes) */
#define FTNANDC021_MCR_PS2048       (1 << 8)
#define FTNANDC021_MCR_PS4096       (2 << 8)
#define FTNANDC021_MCR_16MB         (0 << 4)  /* flash size */
#define FTNANDC021_MCR_32MB         (1 << 4)
#define FTNANDC021_MCR_64MB         (2 << 4)
#define FTNANDC021_MCR_128MB        (3 << 4)
#define FTNANDC021_MCR_256MB        (4 << 4)
#define FTNANDC021_MCR_512MB        (5 << 4)
#define FTNANDC021_MCR_1GB          (6 << 4)
#define FTNANDC021_MCR_2GB          (7 << 4)
#define FTNANDC021_MCR_4GB          (8 << 4)
#define FTNANDC021_MCR_8GB          (9 << 4)
#define FTNANDC021_MCR_16GB         (10 << 4)
#define FTNANDC021_MCR_32GB         (11 << 4)
#define FTNANDC021_MCR_ME(n)        (1 << ((n) & 0x3)) /* module enable */

/* AC Timing Register */
#define FTNANDC021_ATR0_DEFAULT     0x0fff0fff  /* worst but safe */
#define FTNANDC021_ATR1_DEFAULT     0xffffffff  /* worst but safe */

/* Configuration Register */
#define FTNANDC021_CFGR_ECC_RS      (4 << 16)
#define FTNANDC021_CFGR_ECC_BCH     (8 << 16)
#define FTNANDC021_CFGR_8BITS       (8 << 8)
#define FTNANDC021_CFGR_16BITS      (16 << 8)
#define FTNANDC021_CFGR_FLNR(x)     ((x) & 0xf) /* number of flash chips */

/* FTNANDC021 built-in command set */
#define FTNANDC021_CMD_RDID         0x01   /* read id */
#define FTNANDC021_CMD_RESET        0x02   /* flash reset */
#define FTNANDC021_CMD_RDST         0x04   /* read status */
#define FTNANDC021_CMD_RDPG         0x05   /* read page (data + oob) */
#define FTNANDC021_CMD_RDOOB        0x06   /* read oob */
#define FTNANDC021_CMD_WRPG         0x10   /* write page (data + oob) */
#define FTNANDC021_CMD_ERBLK        0x11   /* erase block */
#define FTNANDC021_CMD_WROOB        0x13   /* write oob */

typedef struct Ftnandc021State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    qemu_irq irq;
    DeviceState *flash;

    /* DMA hardware handshake */
    qemu_irq req;

    int cmd;        /* current FTNANDC021 command code */
    int len;        /* length of data buffer */
    int bw;         /* bus width (8-bits, 16-bits) */

    int alen;       /* address length (cycle) */
    int pgsz;       /* page size (Bytes) */
    int bksz;       /* block size (Bytes) */
    uint64_t size;  /* flash size (maximum access range) */

    /* hw registers */
    uint32_t regs[FTNANDC021_REG_SIZE >> 2];
} Ftnandc021State;

#define FTNANDC021(obj) \
    OBJECT_CHECK(Ftnandc021State, obj, TYPE_FTNANDC021)

static void ftnandc021_irq_update(Ftnandc021State *s)
{
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;

    if (regs->ier & FTNANDC021_IER_IRQ) {
        qemu_set_irq(s->irq, !!(FTNANDC021_IER_TO_SR(regs->ier) & regs->sr));
    }
}

static void ftnandc021_set_idle(Ftnandc021State *s)
{
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;

    /* CLE=0, ALE=0, CS=1 */
    nand_setpins(s->flash, 0, 0, 1, 1, 0);

    /* Set command compelete */
    regs->sr |= FTNANDC021_SR_CMD;

    /* Update IRQ signal */
    ftnandc021_irq_update(s);
}

static void ftnandc021_set_cmd(Ftnandc021State *s, int cmd)
{
    /* CLE=1, ALE=0, CS=0 */
    nand_setpins(s->flash, 1, 0, 0, 1, 0);

    /* Write out command code */
    nand_setio(s->flash, (uint8_t)cmd);
}

static void ftnandc021_set_addr(Ftnandc021State *s, int col, int row)
{
    /* CLE=0, ALE=1, CS=0 */
    nand_setpins(s->flash, 0, 1, 0, 1, 0);

    if (col < 0 && row < 0) {
        /* special case for READ_ID (0x90) */
        nand_setio(s->flash, 0);
    } else {
        /* column address */
        if (col >= 0) {
            nand_setio(s->flash, extract32(col, 0, 8));
            nand_setio(s->flash, extract32(col, 8, 8));
        }
        /* row address */
        if (row >= 0) {
            nand_setio(s->flash, extract32(row, 0, 8));
            if (s->alen >= 4) {
                nand_setio(s->flash, extract32(row, 8, 8));
            }
            if (s->alen >= 5) {
                nand_setio(s->flash, extract32(row, 16, 8));
            }
        }
    }
}

static void ftnandc021_handle_ack(void *opaque, int line, int level)
{
    Ftnandc021State *s = FTNANDC021(opaque);
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;

    if (!regs->bcr) {
        return;
    }

    if (level) {
        qemu_set_irq(s->req, 0);
    } else if (s->len > 0) {
        qemu_set_irq(s->req, 1);
    }
}

static void ftnandc021_cmd_rdst(Ftnandc021State *s)
{
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;

    ftnandc021_set_cmd(s, 0x70);
    nand_setpins(s->flash, 0, 0, 0, 1, 0);
    regs->idr[1] = nand_getio(s->flash);
}

static void ftnandc021_get_oob(Ftnandc021State *s)
{
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;
    uint8_t oob[128]; /* max. page size=4096 -> max. oob size=128 */
    uint16_t val;
    int i;

    memset(oob, 0xff, sizeof(oob));

    for (i = 0; i < 16 * (s->pgsz / 512); i += (s->bw >> 3)) {
        if (s->bw == 16) {
            val = (uint16_t)nand_getio(s->flash);
            oob[i] = (uint8_t)(val >> 0);
            oob[i + 1] = (uint8_t)(val >> 8);
        } else {
            oob[i] = (uint8_t)nand_getio(s->flash);
        }
    }

    regs->bbird = oob[0];
    regs->crcrd = oob[8] | (oob[9] << 8);
    if (s->pgsz > 2048) {
        regs->crcrd |= oob[12] << 16;
        regs->crcrd |= oob[13] << 24;
    }
    regs->lsnrd = oob[10] | (oob[11] << 8);
    if (s->pgsz > 2048) {
        regs->lsnrd |= oob[14] << 16;
        regs->lsnrd |= oob[15] << 24;
    }
}

static void ftnandc021_set_oob(Ftnandc021State *s)
{
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;
    uint8_t oob[128]; /* max. page size=4096 -> max. oob size=128 */
    int i;

    memset(oob, 0xff, sizeof(oob));

    oob[8] = (uint8_t)(regs->crcwr >> 0);
    oob[9] = (uint8_t)(regs->crcwr >> 8);
    if (s->pgsz > 2048) {
        oob[12] = (uint8_t)(regs->crcwr >> 16);
        oob[13] = (uint8_t)(regs->crcwr >> 24);
    }
    oob[10] = (uint8_t)(regs->lsnwr >> 0);
    oob[11] = (uint8_t)(regs->lsnwr >> 8);
    if (s->pgsz > 2048) {
        oob[14] = (uint8_t)(regs->lsnwr >> 16);
        oob[15] = (uint8_t)(regs->lsnwr >> 24);
    }

    for (i = 0; i < 16 * (s->pgsz / 512); i += (s->bw >> 3)) {
        if (s->bw == 16) {
            nand_setio(s->flash, oob[i] | (oob[i + 1] << 8));
        } else {
            nand_setio(s->flash, oob[i]);
        }
    }
}

static void ftnandc021_command(Ftnandc021State *s, int cmd)
{
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;

    s->cmd = cmd;
    regs->sr &= ~FTNANDC021_SR_CMD;

    switch (cmd) {
    case FTNANDC021_CMD_RDID:    /* read id */
        ftnandc021_set_cmd(s, 0x90);
        ftnandc021_set_addr(s, -1, -1);
        nand_setpins(s->flash, 0, 0, 0, 1, 0);
        if (s->bw == 8) {
            regs->idr[0] = (nand_getio(s->flash) << 0)
                | (nand_getio(s->flash) << 8)
                | (nand_getio(s->flash) << 16)
                | (nand_getio(s->flash) << 24);
            regs->idr[1] = (nand_getio(s->flash) << 0);
        } else {
            regs->idr[0] = (nand_getio(s->flash) << 0)
                | (nand_getio(s->flash) << 16);
            regs->idr[1] = (nand_getio(s->flash) << 0);
        }
        break;
    case FTNANDC021_CMD_RESET:    /* reset */
        ftnandc021_set_cmd(s, 0xff);
        break;
    case FTNANDC021_CMD_RDST:    /* read status */
        ftnandc021_cmd_rdst(s);
        break;
    case FTNANDC021_CMD_RDPG:    /* read page */
        ftnandc021_set_cmd(s, 0x00);
        ftnandc021_set_addr(s, 0, regs->pir);
        ftnandc021_set_cmd(s, 0x30);
        nand_setpins(s->flash, 0, 0, 0, 1, 0);
        s->len = s->pgsz;
        break;
    case FTNANDC021_CMD_RDOOB:    /* read oob */
        ftnandc021_set_cmd(s, 0x00);
        ftnandc021_set_addr(s, s->pgsz, regs->pir);
        ftnandc021_set_cmd(s, 0x30);
        nand_setpins(s->flash, 0, 0, 0, 1, 0);
        ftnandc021_get_oob(s);
        break;
    case FTNANDC021_CMD_WRPG:    /* write page + read status */
        ftnandc021_set_cmd(s, 0x80);
        ftnandc021_set_addr(s, 0, regs->pir);
        /* data phase */
        nand_setpins(s->flash, 0, 0, 0, 1, 0);
        s->len = s->pgsz;
        break;
    case FTNANDC021_CMD_ERBLK:    /* erase block + read status */
        ftnandc021_set_cmd(s, 0x60);
        ftnandc021_set_addr(s, -1, regs->pir);
        ftnandc021_set_cmd(s, 0xd0);
        /* read status */
        ftnandc021_cmd_rdst(s);
        break;
    case FTNANDC021_CMD_WROOB:    /* write oob + read status */
        ftnandc021_set_cmd(s, 0x80);
        ftnandc021_set_addr(s, s->pgsz, regs->pir);
        /* data phase */
        nand_setpins(s->flash, 0, 0, 0, 1, 0);
        ftnandc021_set_oob(s);
        ftnandc021_set_cmd(s, 0x10);
        /* read status */
        ftnandc021_cmd_rdst(s);
        break;
    default:
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftnandc021: unknow command=0x%02x\n", cmd);
        break;
    }

    /* if cmd is not page read/write, then return to idle mode */
    switch (s->cmd) {
    case FTNANDC021_CMD_RDPG:
    case FTNANDC021_CMD_WRPG:
        if (regs->bcr && (s->len > 0)) {
            qemu_set_irq(s->req, 1);
        }
        break;
    default:
        ftnandc021_set_idle(s);
        break;
    }
}

static uint64_t ftnandc021_mem_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftnandc021State *s = FTNANDC021(opaque);
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;
    uint32_t i, ret = 0;

    if (addr >= FTNANDC021_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftnandc021: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftnandc021Regs, dr):
        if ((s->cmd == FTNANDC021_CMD_RDPG) && (s->len > 0)) {
            if (s->bw == 8) {
                for (i = 0; i < 4 && s->len > 0; i++, s->len--) {
                    ret = deposit32(ret, i * 8, 8, nand_getio(s->flash));
                }
            } else {
                for (i = 0; i < 2 && s->len > 1; i++, s->len -= 2) {
                    ret = deposit32(ret, i * 16, 16, nand_getio(s->flash));
                }
            }
            if (s->len <= 0) {
                ftnandc021_get_oob(s);
                ftnandc021_set_idle(s);
            }
        }
        break;
    case offsetof(Ftnandc021Regs, sr):
        ret = regs->sr | FTNANDC021_SR_READY
            | ((regs->mcr & FTNANDC021_MCR_ME(0)) ? FTNANDC021_SR_EN : 0);
        break;
    case offsetof(Ftnandc021Regs, ior):
        ret = FTNANDC021_IOR_READY;    /* Always ready for RW */
        break;
    case offsetof(Ftnandc021Regs, revr):
        ret = FTNANDC021_REVISION;
        break;
    case offsetof(Ftnandc021Regs, cfgr):
        ret = FTNANDC021_CFGR_ECC_BCH | FTNANDC021_CFGR_16BITS
            | FTNANDC021_CFGR_FLNR(1);
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftnandc021_mem_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    uint32_t i;
    Ftnandc021State *s = FTNANDC021(opaque);
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;

    if (addr >= FTNANDC021_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftnandc021: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftnandc021Regs, dr):
        if (s->cmd == FTNANDC021_CMD_WRPG && s->len > 0) {
            if (s->bw == 8) {
                for (i = 0; i < 4 && s->len > 0; i++, s->len--) {
                    nand_setio(s->flash,
                               extract32((uint32_t)val, i * 8, 8));
                }
            } else {
                for (i = 0; i < 2 && s->len > 1; i++, s->len -= 2) {
                    nand_setio(s->flash,
                               extract32((uint32_t)val, i * 16, 16));
                }
            }
            if (s->len <= 0) {
                ftnandc021_set_oob(s);
                ftnandc021_set_cmd(s, 0x10);
                /* read status */
                ftnandc021_cmd_rdst(s);
            }
        }
        break;
    case offsetof(Ftnandc021Regs, acr):
        if (val & FTNANDC021_ACR_START) {
            ftnandc021_command(s, FTNANDC021_ACR_CMD(val));
            val &= ~FTNANDC021_ACR_START;
        }
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    case offsetof(Ftnandc021Regs, fcr):
        s->bw = (val & FTNANDC021_FCR_16BIT) ? 16 : 8;
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    case offsetof(Ftnandc021Regs, mcr):
        s->regs[addr >> 2] = (uint32_t)val;
        /* page size */
        s->pgsz = 1 << (10 + extract32((uint32_t)val, 8, 2));
        if (s->pgsz < 2048) {
            s->pgsz = 512;
        } else if (s->pgsz > 2048) {
            s->pgsz = 4096;
        }
        /* block size */
        s->bksz = s->pgsz * (1 << (4 + extract32((uint32_t)val, 16, 2)));
        /* address length (cycle) */
        s->alen = 3 + extract32((uint32_t)val, 10, 2);
        /* flash size */
        s->size = 1ULL << (24 + extract32((uint32_t)val, 4, 4));
        break;
    case offsetof(Ftnandc021Regs, ier):
        s->regs[addr >> 2] = (uint32_t)val & FTNANDC021_IER_MASK;
        ftnandc021_irq_update(s);
        break;
    case offsetof(Ftnandc021Regs, iscr):
        regs->sr &= ~FTNANDC021_ISCR_TO_SR(val);
        ftnandc021_irq_update(s);
        break;
    case offsetof(Ftnandc021Regs, srr):
        s->regs[addr >> 2] = (uint32_t)val & ~FTNANDC021_SRR_RESET;
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftnandc021_mem_read,
    .write = ftnandc021_mem_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftnandc021_reset(DeviceState *dev)
{
    Ftnandc021State *s = FTNANDC021(dev);
    Ftnandc021Regs *regs = (Ftnandc021Regs *)s->regs;
    Error *local_errp = NULL;

    s->flash = DEVICE(object_property_get_link(OBJECT(s),
                                               "flash",
                                               &local_errp));
    if (local_errp) {
        fprintf(stderr, "ftnandc021: Unable to get flash link\n");
        abort();
    }

    memset(s->regs, 0, FTNANDC021_REG_SIZE);
    regs->atr[0] = FTNANDC021_ATR0_DEFAULT;
    regs->atr[1] = FTNANDC021_ATR1_DEFAULT;

    /* We can assume our GPIO outputs have been wired up now */
    qemu_set_irq(s->req, 0);
}

static void ftnandc021_realize(DeviceState *dev, Error **errp)
{
    Ftnandc021State *s = FTNANDC021(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->mmio,
                          &mmio_ops,
                          s,
                          TYPE_FTNANDC021,
                          FTNANDC021_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq);

    qdev_init_gpio_in(&sbd->qdev, ftnandc021_handle_ack, 1);
    qdev_init_gpio_out(&sbd->qdev, &s->req, 1);
}

static const VMStateDescription vmstate_ftnandc021 = {
    .name = TYPE_FTNANDC021,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftnandc021State, FTNANDC021_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static void ftnandc021_instance_init(Object *obj)
{
    Ftnandc021State *s = FTNANDC021(obj);

    object_property_add_link(obj,
                             "flash",
                             TYPE_DEVICE,
                             (Object **) &s->flash,
                             NULL);
}

static void ftnandc021_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftnandc021;
    dc->reset   = ftnandc021_reset;
    dc->realize = ftnandc021_realize;
    dc->no_user = 1;
}

static const TypeInfo ftnandc021_info = {
    .name          = TYPE_FTNANDC021,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftnandc021State),
    .instance_init = ftnandc021_instance_init,
    .class_init    = ftnandc021_class_init,
};

static void ftnandc021_register_types(void)
{
    type_register_static(&ftnandc021_info);
}

type_init(ftnandc021_register_types)
