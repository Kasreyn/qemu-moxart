/*
 * Faraday FTSPI020 SPI Flash Controller
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/ssi.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"
#include "qemu/bitops.h"

#define TYPE_FTSPI020       "ftspi020"
#define FTSPI020_REVISION   FARADAY_REVISION(1, 0, 1)
#define FTSPI020_NR_CSLINES 1

typedef struct Ftspi020Regs {
    uint32_t cmd[4];/* Command Queue Registers */
    uint32_t cr;    /* Control Register */
    uint32_t atr;   /* AC Timing Register */
    uint32_t sr;    /* Status Register */
    uint32_t rsvd0[1];
    uint32_t icr;   /* Interrupt Control Register */
    uint32_t isr;   /* Interrupt Status Register */
    uint32_t rdsr;  /* Read Status Register (Flash Status) */
    uint32_t rsvd1[9];
    uint32_t revr;  /* Revision Register */
    uint32_t fear;  /* Feature Register */
    uint32_t rsvd2[42];
    uint32_t dr;    /* Data Register */
} Ftspi020Regs;

#define FTSPI020_REG_SIZE       sizeof(Ftspi020Regs)

#define FTSPI020_CMD_ADDRESS(x) \
    ((x)[0])                    /* Flash address */
#define FTSPI020_CMD_OPCLEN(x)  \
    extract32((x)[1], 24, 2)    /* OP code length */
#define FTSPI020_CMD_DCLOCK(x)  \
    extract32((x)[1], 16, 8)    /* Dummy clock length */
#define FTSPI020_CMD_ALEN(x)    \
    extract32((x)[1], 0, 3)     /* Address length */
#define FTSPI020_CMD_LENGTH(x)  \
    ((x)[2])                    /* Data length */
#define FTSPI020_CMD_OPC(x) \
    extract32((x)[3], 24, 8)    /* OP code */
#define FTSPI020_CMD_CS(x)  \
    extract32((x)[3], 8, 2)     /* Chip Select */
#define FTSPI020_CMD_RDSR_SW(x) \
    ((x)[3] & (1 << 3))         /* Enable SW polling RDSR */
#define FTSPI020_CMD_RDSR(x)    \
    ((x)[3] & (1 << 2))         /* RDSR command */
#define FTSPI020_CMD_WRITE(x)   \
    ((x)[3] & (1 << 1))         /* WRITE command */
#define FTSPI020_CMD_IRQ(x)     \
    ((x)[3] & (1 << 0))         /* Enable interrupt */

#define FTSPI020_CR_WIP(x)      extract32((uint32_t)(x), 16, 3) /* WIP shift */
#define FTSPI020_CR_ABORT       (1 << 8)
#define FTSPI020_CR_MODE0       (0 << 4)    /* SPI MODE0 */
#define FTSPI020_CR_MODE3       (1 << 4)    /* SPI MODE3 */
#define FTSPI020_CR_CLKDIV(n)   ((n) & 0x03)/* Clock divider = 2 * (n + 1) */

#define FTSPI020_SR_RX          (1 << 1)  /* Rx Ready */
#define FTSPI020_SR_TX          (1 << 0)  /* Tx Ready */

#define FTSPI020_ICR_DMA        (1 << 0)  /* Enable DMA HW handshake */

#define FTSPI020_ISR_CMD        (1 << 0)  /* Command interrupt */

#define FTSPI020_FEAR_SYNC      (1 << 25) /* clock mode = sync */
#define FTSPI020_FEAR_SUPP_DTR  (1 << 24) /* support double transfer rate */
#define FTSPI020_FEAR_CMDQ(x)   (((x) & 0x07) << 16) /* cmd queue depth */
#define FTSPI020_FEAR_RXFIFO(x) (((x) & 0xff) << 8)  /* rx fifo size */
#define FTSPI020_FEAR_TXFIFO(x) (((x) & 0xff) << 0)  /* tx fifo size */

typedef struct Ftspi020State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    qemu_irq irq;

    SSIBus *spi;
    qemu_irq *cs_lines;

    /* DMA hardware handshake */
    qemu_irq req;

    /* hw registers */
    uint32_t regs[FTSPI020_REG_SIZE >> 2];
} Ftspi020State;

#define FTSPI020(obj) \
    OBJECT_CHECK(Ftspi020State, obj, TYPE_FTSPI020)

static void ftspi020_irq_update(Ftspi020State *s)
{
    Ftspi020Regs *regs = (Ftspi020Regs *)s->regs;

    qemu_set_irq(s->irq, !!regs->isr);
}

static void ftspi020_dmahs_ack(void *opaque, int line, int level)
{
    Ftspi020State *s = FTSPI020(opaque);
    Ftspi020Regs *regs = (Ftspi020Regs *)s->regs;

    if (!(regs->icr & FTSPI020_ICR_DMA)) {
        return;
    }

    if (level) {
        qemu_set_irq(s->req, 0);
    } else if (regs->cmd[2]) {
        qemu_set_irq(s->req, 1);
    }
}

static int ftspi020_do_command(Ftspi020State *s)
{
    Ftspi020Regs *regs = (Ftspi020Regs *)s->regs;
    uint32_t cs = FTSPI020_CMD_CS(regs->cmd);
    uint32_t cmd = FTSPI020_CMD_OPC(regs->cmd);
    uint32_t ilen = MIN(2, FTSPI020_CMD_OPCLEN(regs->cmd));
    uint32_t dclk = FTSPI020_CMD_DCLOCK(regs->cmd);
    uint32_t alen = MIN(4, FTSPI020_CMD_ALEN(regs->cmd));
    uint32_t wip = FTSPI020_CR_WIP(regs->cr);
    int i;

    if (dclk & 0x7) {
        fprintf(stderr, "ftspi020: bad dummy cycle = %u\n", dclk);
        abort();
    }

    /* activate spi flash */
    qemu_set_irq(s->cs_lines[cs], 0);

    /* read spi flash status */
    if (FTSPI020_CMD_RDSR(regs->cmd) && !FTSPI020_CMD_WRITE(regs->cmd)) {
        ssi_transfer(s->spi, cmd);
        do {
            regs->rdsr = ssi_transfer(s->spi, 0x00);
            if (FTSPI020_CMD_RDSR_SW(regs->cmd)) {
                break;
            }
        } while (regs->rdsr & (1 << wip));
    } else {
        /* command cycles */
        for (i = 0; i < ilen; ++i) {
            ssi_transfer(s->spi, cmd);
        }
        /* address cycles */
        for (i = alen - 1; i >= 0; --i) {
            ssi_transfer(s->spi,
                extract32(FTSPI020_CMD_ADDRESS(regs->cmd), i * 8, 8));
        }
        /* dummy cycles */
        for (i = 0; i < (dclk >> 3); ++i) {
            ssi_transfer(s->spi, 0x00);
        }
    }

    if (!FTSPI020_CMD_LENGTH(regs->cmd)) {
        /* de-activate spi flash */
        qemu_set_irq(s->cs_lines[cs], 1);
    } else if (regs->icr & FTSPI020_ICR_DMA) {
        /* DMA hardware handshake */
        qemu_set_irq(s->req, 1);
    }

    /* update interrupt signal */
    if (FTSPI020_CMD_IRQ(regs->cmd)) {
        regs->isr |= FTSPI020_ISR_CMD;
    }
    ftspi020_irq_update(s);

    return 0;
}

static void ftspi020_chip_reset(Ftspi020State *s)
{
    int i;

    memset(s->regs, 0, FTSPI020_REG_SIZE);

    /* clear interrupt signal */
    qemu_set_irq(s->irq, 0);

    /* de-activate spi flashes */
    for (i = 0; i < FTSPI020_NR_CSLINES; ++i) {
        qemu_set_irq(s->cs_lines[i], 1);
    }
}

static uint64_t ftspi020_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftspi020State *s = FTSPI020(opaque);
    Ftspi020Regs *regs = (Ftspi020Regs *)s->regs;
    uint32_t ret = 0;
    int cs, i;

    if (addr >= FTSPI020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftspi020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftspi020Regs, dr):
        if (!FTSPI020_CMD_WRITE(regs->cmd)) {
            cs = FTSPI020_CMD_CS(regs->cmd);
            for (i = 0; i < 4 && FTSPI020_CMD_LENGTH(regs->cmd); i++) {
                ret = deposit32(ret, i * 8, 8,
                    ssi_transfer(s->spi, 0x00) & 0xff);
                --FTSPI020_CMD_LENGTH(regs->cmd);
            }
            if (!FTSPI020_CMD_LENGTH(regs->cmd)) {
                /* de-activate spi flash */
                qemu_set_irq(s->cs_lines[cs], 1);
                if (FTSPI020_CMD_IRQ(regs->cmd)) {
                    regs->isr |= FTSPI020_ISR_CMD;
                }
                ftspi020_irq_update(s);
            }
        }
        break;
    case offsetof(Ftspi020Regs, sr):
        /* In QEMU, the data fifo is always ready for read/write */
        ret = FTSPI020_SR_RX | FTSPI020_SR_TX;
        break;
    case offsetof(Ftspi020Regs, revr):
        ret = FTSPI020_REVISION;
        break;
    case offsetof(Ftspi020Regs, fear):
        ret = FTSPI020_FEAR_SYNC | FTSPI020_FEAR_CMDQ(2)
            | FTSPI020_FEAR_RXFIFO(32) | FTSPI020_FEAR_TXFIFO(32);
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftspi020_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftspi020State *s = FTSPI020(opaque);
    Ftspi020Regs *regs = (Ftspi020Regs *)s->regs;
    int i, cs;

    if (addr >= FTSPI020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftspi020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftspi020Regs, cmd) + 0x0C:
        regs->cmd[3] = (uint32_t)val;
        ftspi020_do_command(s);
        break;
    case offsetof(Ftspi020Regs, cr):
        if (val & FTSPI020_CR_ABORT) {
            ftspi020_chip_reset(s);
            val &= ~FTSPI020_CR_ABORT;
        }
        regs->cr = (uint32_t)val;
        break;
    case offsetof(Ftspi020Regs, dr):
        if (FTSPI020_CMD_WRITE(regs->cmd)) {
            cs = FTSPI020_CMD_CS(regs->cmd);
            for (i = 0; i < 4 && FTSPI020_CMD_LENGTH(regs->cmd); i++) {
                ssi_transfer(s->spi, extract32((uint32_t)val, i * 8, 8));
                --FTSPI020_CMD_LENGTH(regs->cmd);
            }
            if (!FTSPI020_CMD_LENGTH(regs->cmd)) {
                /* de-activate spi flash */
                qemu_set_irq(s->cs_lines[cs], 1);
                if (FTSPI020_CMD_IRQ(regs->cmd)) {
                    regs->isr |= FTSPI020_ISR_CMD;
                }
                ftspi020_irq_update(s);
            }
        }
        break;
    case offsetof(Ftspi020Regs, isr):
        regs->isr &= ~((uint32_t)val);
        ftspi020_irq_update(s);
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftspi020_mmio_read,
    .write = ftspi020_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void ftspi020_reset(DeviceState *dev)
{
    ftspi020_chip_reset(FTSPI020(dev));
}

static void ftspi020_realize(DeviceState *dev, Error **errp)
{
    Ftspi020State *s = FTSPI020(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    int i;

    memory_region_init_io(&s->iomem, &mmio_ops, s,
        TYPE_FTSPI020, FTSPI020_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    sysbus_init_irq(sbd, &s->irq);

    s->spi = ssi_create_bus(&sbd->qdev, "spi");
    s->cs_lines = g_new0(qemu_irq, FTSPI020_NR_CSLINES);
    ssi_auto_connect_slaves(dev, s->cs_lines, s->spi);
    for (i = 0; i < FTSPI020_NR_CSLINES; ++i) {
        sysbus_init_irq(sbd, &s->cs_lines[i]);
    }

    qdev_init_gpio_in(&sbd->qdev, ftspi020_dmahs_ack, 1);
    qdev_init_gpio_out(&sbd->qdev, &s->req, 1);
}

static const VMStateDescription ftspi020_vmst = {
    .name = TYPE_FTSPI020,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftspi020State, FTSPI020_REG_SIZE / 4),
        VMSTATE_END_OF_LIST(),
    }
};

static void ftspi020_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &ftspi020_vmst;
    dc->reset   = ftspi020_reset;
    dc->realize = ftspi020_realize;
    dc->no_user = 1;
}

static const TypeInfo ftspi020_info = {
    .name          = TYPE_FTSPI020,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftspi020State),
    .class_init    = ftspi020_class_init,
};

static void ftspi020_register_types(void)
{
    type_register_static(&ftspi020_info);
}

type_init(ftspi020_register_types)
