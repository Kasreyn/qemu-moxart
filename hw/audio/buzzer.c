/*
 * Buzzer
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * Code based on pcspk.c by Joachim Henke
 *
 * This code is licensed under the GNU GPL v2+
 */

#include "hw/sysbus.h"
#include "hw/audio/audio.h"
#include "audio/audio.h"

#include <math.h>

#define TYPE_BUZZER        "buzzer"

#define PIT_FREQ 2000

#define BUZZER_BUF_LEN 1792
#define BUZZER_SAMPLE_RATE 32000
#define BUZZER_MAX_FREQ (BUZZER_SAMPLE_RATE >> 1)
#define BUZZER_MIN_COUNT ((PIT_FREQ + BUZZER_MAX_FREQ - 1) / BUZZER_MAX_FREQ)

typedef struct BuzzerState {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/

    QEMUSoundCard card;
    SWVoiceOut *voice;

    int samples;

    uint8_t *sample_buf;
    int play_pos;
} BuzzerState;

#define BUZZER(obj) \
    OBJECT_CHECK(BuzzerState, obj, TYPE_BUZZER)

static inline void generate_samples(BuzzerState *s)
{
    int i;

    const uint32_t m = BUZZER_SAMPLE_RATE * 1;
    const uint32_t n = ((uint64_t)PIT_FREQ << 32) / m;

    /* multiple of wavelength for gapless looping */
    s->samples = (BUZZER_BUF_LEN * PIT_FREQ / m * m / (PIT_FREQ >> 1) + 1) >> 1;

    for (i = 0; i < s->samples; ++i)
        s->sample_buf[i] = (64 & (n * i >> 25)) - 32;



}

static void buzzer_callback(void *opaque, int free)
{
    BuzzerState *s = BUZZER(opaque);

    int n;

    while (free > 0) {
        n = audio_MIN(s->samples - s->play_pos, free);
        n = AUD_write(s->voice, &s->sample_buf[s->play_pos], n);
        if (!n)
            break;
        s->play_pos = (s->play_pos + n) % s->samples;
        free -= n;
    }
}

static void buzzer_signal(void *opaque, int line, int level)
{
    BuzzerState *s = BUZZER(opaque);

    if (s->voice) {
        AUD_set_active_out(s->voice, level);
    }
}

static void buzzer_reset(DeviceState *dev)
{
    BuzzerState *s = BUZZER(dev);

    if (s->voice) {
        AUD_set_active_out(s->voice, 0);
        AUD_close_out(&s->card, s->voice);
        s->voice = NULL;
    }

    if (s->sample_buf) {
        g_free(s->sample_buf);
    }

    struct audsettings as = {BUZZER_SAMPLE_RATE, 1, AUD_FMT_U8, AUDIO_HOST_ENDIANNESS};

    s->voice = AUD_open_out(&s->card, s->voice, TYPE_BUZZER, s, buzzer_callback, &as);

    if (!s->voice) {
        fprintf(stderr, "Could not open voice\n");
        return;
    }

    s->samples = AUD_get_buffer_size_out(s->voice);

    s->sample_buf = g_malloc0(s->samples);

    generate_samples(s);
}

static void buzzer_realize(DeviceState *dev, Error **errp)
{
    BuzzerState *s = BUZZER(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    AUD_register_card(TYPE_BUZZER, &s->card);

    qdev_init_gpio_in(&sbd->qdev, buzzer_signal, 1);
}

static const VMStateDescription vmstate_buzzer = {
    .name = TYPE_BUZZER,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_END_OF_LIST()
    }
};

static Property buzzer_properties[] = {
    DEFINE_PROP_END_OF_LIST()
};

static void buzzer_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_buzzer;
    dc->props   = buzzer_properties;
    dc->reset   = buzzer_reset;
    dc->realize = buzzer_realize;
    dc->no_user = 1;
}

static const TypeInfo buzzer_info = {
    .name          = TYPE_BUZZER,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(BuzzerState),
    .class_init    = buzzer_class_init,
};

static void buzzer_register_types(void)
{
    type_register_static(&buzzer_info);
}

type_init(buzzer_register_types)


