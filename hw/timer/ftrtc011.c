/*
 * Faraday FTRTC011 RTC Timer
 *
 * Copyright (C) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This file is licensed under GNU GPL v2+.
 */

#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "sysemu/sysemu.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"

#define TYPE_FTRTC011       "ftrtc011"
#define FTRTC011_REVISION   FARADAY_REVISION(1, 0, 0)

typedef struct Ftrtc011Regs {
    uint32_t sec;
    uint32_t min;
    uint32_t hour;
    uint32_t day;

    uint32_t alarm_sec;
    uint32_t alarm_min;
    uint32_t alarm_hour;
    uint32_t rsvd0;

    uint32_t cr;    /* control register */
    uint32_t wsec;  /* write sec register */
    uint32_t wmin;  /* write min register */
    uint32_t whour; /* write hour register */

    uint32_t wday;  /* write day register */
    uint32_t isr;   /* interrupt status register */
    uint32_t rsvd1;
    uint32_t revr;  /* revision register */

    uint32_t sr;    /* status register */
    uint32_t ctime; /* current time */
    uint32_t stime; /* sleep time */
} Ftrtc011Regs;

#define FTRTC011_REG_SIZE       sizeof(Ftrtc011Regs)

#define FTRTC011_CR_RELOAD      (1 << 6) /* Reload counters from Wxxx */
#define FTRTC011_CR_IRQ_ALARM   (1 << 5) /* Enable Alarm interrupt */
#define FTRTC011_CR_IRQ_DAY     (1 << 4) /* Enable DAY interrupt */
#define FTRTC011_CR_IRQ_HOUR    (1 << 3) /* Enable HOUR interrupt */
#define FTRTC011_CR_IRQ_MIN     (1 << 2) /* Enable MIN interrupt */
#define FTRTC011_CR_IRQ_SEC     (1 << 1) /* Enable SEC interrupt */
#define FTRTC011_CR_EN          (1 << 0) /* Enable the chip */

#define FTRTC011_CR_IRQ_MASK    0x3e
#define FTRTC011_CR_TO_ISR(x)   (((x) & 0x3e) >> 1)

#define FTRTC011_ISR_ALARM      (1 << 4)
#define FTRTC011_ISR_DAY        (1 << 3)
#define FTRTC011_ISR_HOUR       (1 << 2)
#define FTRTC011_ISR_MIN        (1 << 1)
#define FTRTC011_ISR_SEC        (1 << 0)
#define FTRTC011_ISR_MASK       0x1f

typedef enum Ftrtc011Irq {
    IRQ_ALARM_LEVEL = 0,
    IRQ_ALARM_EDGE,
    IRQ_SEC,
    IRQ_MIN,
    IRQ_HOUR,
    IRQ_DAY,
    IRQ_NUMBER,
} Ftrtc011Irq;

typedef struct Ftrtc011State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion mmio;

    qemu_irq irq[IRQ_NUMBER];

    QEMUTimer *qtimer;
    int64_t base;
    int64_t sync;
    int64_t start;

    /* HW register caches */
    uint32_t regs[FTRTC011_REG_SIZE >> 2];
} Ftrtc011State;

#define FTRTC011(obj) \
    OBJECT_CHECK(Ftrtc011State, obj, TYPE_FTRTC011)

/* Update interrupts.  */
static void ftrtc011_irq_update(Ftrtc011State *s)
{
    Ftrtc011Regs *regs = (Ftrtc011Regs *)s->regs;
    uint32_t mask = FTRTC011_CR_TO_ISR(regs->cr) & regs->isr;

    qemu_set_irq(s->irq[IRQ_ALARM_LEVEL], !!(mask & FTRTC011_ISR_ALARM));

    if (mask & FTRTC011_ISR_SEC) {
        qemu_irq_pulse(s->irq[IRQ_SEC]);
    }
    if (mask & FTRTC011_ISR_MIN) {
        qemu_irq_pulse(s->irq[IRQ_MIN]);
    }
    if (mask & FTRTC011_ISR_HOUR) {
        qemu_irq_pulse(s->irq[IRQ_HOUR]);
    }
    if (mask & FTRTC011_ISR_DAY) {
        qemu_irq_pulse(s->irq[IRQ_DAY]);
    }
    if (mask & FTRTC011_ISR_ALARM) {
        qemu_irq_pulse(s->irq[IRQ_ALARM_EDGE]);
    }
}

static void ftrtc011_start(Ftrtc011State *s)
{
    Ftrtc011Regs *regs = (Ftrtc011Regs *)s->regs;
    int64_t base = regs->sec + (60LL * regs->min) + (3600LL * regs->hour)
        + (86400LL * regs->day);

    s->base = base;
    s->sync = base;
    s->start = qemu_get_clock_ms(rtc_clock);

    if (regs->cr & FTRTC011_CR_IRQ_MASK) {
        qemu_mod_timer(s->qtimer, s->start + 1000);
    }
}

static void ftrtc011_sync(Ftrtc011State *s)
{
    Ftrtc011Regs *regs = (Ftrtc011Regs *)s->regs;
    int64_t curr, sec, min, hour, day;

    /* check if RTC is enabled */
    if (!(regs->cr & FTRTC011_CR_EN)) {
        return;
    }

    curr = s->base + (qemu_get_clock_ms(rtc_clock) - s->start) / 1000LL;

    if (s->sync == curr) {
        return;
    }
    s->sync = curr;

    day = curr / 86400LL;
    curr %= 86400LL;

    hour = curr / 3600LL;
    curr %= 3600LL;

    min = curr / 60LL;
    curr %= 60LL;

    sec = curr;

    /* sec interrupt */
    if ((regs->sec != sec) && (regs->cr & FTRTC011_CR_IRQ_SEC)) {
        regs->isr |= FTRTC011_ISR_SEC;
    }
    /* min interrupt */
    if ((regs->min != min) && (regs->cr & FTRTC011_CR_IRQ_MIN)) {
        regs->isr |= FTRTC011_ISR_SEC;
        regs->isr |= FTRTC011_ISR_MIN;
    }
    /* hour interrupt */
    if ((regs->hour != hour) && (regs->cr & FTRTC011_CR_IRQ_HOUR)) {
        regs->isr |= FTRTC011_ISR_SEC;
        regs->isr |= FTRTC011_ISR_MIN;
        regs->isr |= FTRTC011_ISR_HOUR;
    }
    /* day interrupt */
    if ((regs->day != day) && (regs->cr & FTRTC011_CR_IRQ_DAY)) {
        regs->isr |= FTRTC011_ISR_SEC;
        regs->isr |= FTRTC011_ISR_MIN;
        regs->isr |= FTRTC011_ISR_HOUR;
        regs->isr |= FTRTC011_ISR_DAY;
    }

    /* update counter registers */
    regs->sec = (uint32_t)sec;
    regs->min = (uint32_t)min;
    regs->hour = (uint32_t)hour;
    regs->day = (uint32_t)day;

    /* alarm interrupt */
    if ((regs->cr & FTRTC011_CR_IRQ_ALARM)
        && (regs->sec == regs->alarm_sec
        && regs->min == regs->alarm_min
        && regs->hour == regs->alarm_hour)) {
        regs->isr |= FTRTC011_ISR_ALARM;
    }

    /* set interrupt signal */
    ftrtc011_irq_update(s);
}

static uint64_t ftrtc011_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftrtc011State *s = FTRTC011(opaque);
    Ftrtc011Regs *regs = (Ftrtc011Regs *)s->regs;
    uint32_t ret = 0;

    if (addr >= FTRTC011_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftrtc011: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    ftrtc011_sync(s);

    switch (addr) {
    case offsetof(Ftrtc011Regs, revr):
        ret = FTRTC011_REVISION;
        break;
    case offsetof(Ftrtc011Regs, ctime):
        ret = deposit32(ret, 17, 15, regs->day);
        ret = deposit32(ret, 12, 5, regs->hour);
        ret = deposit32(ret, 6, 6, regs->min);
        ret = deposit32(ret, 0, 6, regs->sec);
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftrtc011_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Ftrtc011State *s = FTRTC011(opaque);
    Ftrtc011Regs *regs = (Ftrtc011Regs *)s->regs;
    uint32_t old;

    if (addr >= FTRTC011_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftrtc011: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftrtc011Regs, cr):
        if (val & FTRTC011_CR_RELOAD) {
            regs->sec = regs->wsec;
            regs->min = regs->wmin;
            regs->hour = regs->whour;
            regs->day = regs->wday;
            val &= ~FTRTC011_CR_RELOAD;
        }
        old = s->regs[addr >> 2];
        s->regs[addr >> 2] = (uint32_t)val;
        if ((old ^ val) & FTRTC011_CR_EN) {
            if (val & FTRTC011_CR_EN) {
                ftrtc011_start(s);
            } else {
                ftrtc011_sync(s);
                qemu_del_timer(s->qtimer);
            }
        }
        ftrtc011_irq_update(s);
        break;
    case offsetof(Ftrtc011Regs, isr):
        regs->isr &= ~((uint32_t)val);
        ftrtc011_irq_update(s);
        break;
    case offsetof(Ftrtc011Regs, alarm_sec):
    case offsetof(Ftrtc011Regs, wsec):
    case offsetof(Ftrtc011Regs, alarm_min):
    case offsetof(Ftrtc011Regs, wmin):
        if (val > 59) {
            qemu_log_mask(LOG_GUEST_ERROR,
                "ftrtc011: %u is invalid for SEC/MIN\n",
                (uint32_t)val);
        } else {
            s->regs[addr >> 2] = (uint32_t)val;
        }
        break;
    case offsetof(Ftrtc011Regs, alarm_hour):
    case offsetof(Ftrtc011Regs, whour):
        if (val > 23) {
            qemu_log_mask(LOG_GUEST_ERROR,
                "ftrtc011: %u is invalid for HOUR\n",
                (uint32_t)val);
        } else {
            s->regs[addr >> 2] = (uint32_t)val;
        }
        break;
    case offsetof(Ftrtc011Regs, wday):
        s->regs[addr >> 2] = (uint32_t)(val & 0x7fff);
        break;
    default:
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftrtc011_mmio_read,
    .write = ftrtc011_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4
    }
};

static void ftrtc011_timer(void *opaque)
{
    Ftrtc011State *s = FTRTC011(opaque);
    Ftrtc011Regs *regs = (Ftrtc011Regs *)s->regs;

    if (regs->cr & FTRTC011_CR_EN) {
        ftrtc011_sync(s);
        if (regs->cr & FTRTC011_CR_IRQ_MASK) {
            qemu_mod_timer(s->qtimer, qemu_get_clock_ms(rtc_clock) + 1000);
        }
    }
}

static void ftrtc011_reset(DeviceState *dev)
{
    Ftrtc011State *s = FTRTC011(dev);

    qemu_del_timer(s->qtimer);
    memset(s->regs, 0, sizeof(s->regs));
    ftrtc011_irq_update(s);
}

static void ftrtc011_realize(DeviceState *dev, Error **errp)
{
    Ftrtc011State *s = FTRTC011(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    s->qtimer = qemu_new_timer_ms(rtc_clock, ftrtc011_timer, s);

    memory_region_init_io(&s->mmio, &mmio_ops, s,
        TYPE_FTRTC011, FTRTC011_REG_SIZE);
    sysbus_init_mmio(sbd, &s->mmio);
    sysbus_init_irq(sbd, &s->irq[IRQ_ALARM_LEVEL]);
    sysbus_init_irq(sbd, &s->irq[IRQ_ALARM_EDGE]);
    sysbus_init_irq(sbd, &s->irq[IRQ_SEC]);
    sysbus_init_irq(sbd, &s->irq[IRQ_MIN]);
    sysbus_init_irq(sbd, &s->irq[IRQ_HOUR]);
    sysbus_init_irq(sbd, &s->irq[IRQ_DAY]);
}

static const VMStateDescription ftrtc011_vmst = {
    .name = TYPE_FTRTC011,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftrtc011State, FTRTC011_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST()
    }
};

static void ftrtc011_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd = &ftrtc011_vmst;
    dc->reset = ftrtc011_reset;
    dc->realize = ftrtc011_realize;
    dc->no_user = 1;
}

static const TypeInfo ftrtc011_info = {
    .name = TYPE_FTRTC011,
    .parent = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftrtc011State),
    .class_init = ftrtc011_class_init,
};

static void ftrtc011_register_types(void)
{
    type_register_static(&ftrtc011_info);
}

type_init(ftrtc011_register_types)
