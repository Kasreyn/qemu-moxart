/*
 * Faraday FTTMR010 Timer.
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/timer.h"
#include "sysemu/sysemu.h"

#define FTTMR010_REVISION   FARADAY_REVISION(1, 8, 1)

#ifdef CONFIG_MOXART
#undef FTTMR010_REVISION
#define FTTMR010_REVISION   FARADAY_REVISION(1, 5, 0)
#endif

typedef struct Fttmr010TimerRegs {
    uint32_t counter;   /* counter value register */
    uint32_t reload;    /* reload value register */
    uint32_t match1;    /* match1 register */
    uint32_t match2;    /* match2 register */
} Fttmr010TimerRegs;

typedef struct Fttmr010Regs {
    Fttmr010TimerRegs timer[3];
    uint32_t cr;        /* control register */
    uint32_t isr;       /* interrupt status register */
    uint32_t imr;       /* interrupt mask register */
    uint32_t revr;      /* revision register */
} Fttmr010Regs;

#define FTTMR010_REG_SIZE           sizeof(Fttmr010Regs)

#define FTTMR010_TIMER_ID(addr)     ((addr) >> 4)
#define FTTMR010_TIMER_REG_MASK     0x0f
#define FTTMR010_TIMER_REG_START    0x00
#define FTTMR010_TIMER_REG_END      0x2C

/* FTTMR010 timer start/enable */
#define FTTMR010_CR_START(t)       (0x01 << ((t)->id * 3))
/* FTTMR010 timer overflow interrupt enable */
#define FTTMR010_CR_OFIE(t)        (0x04 << ((t)->id * 3))
/* FTTMR010 timer count-ascent/ascent mode */
#define FTTMR010_CR_ASCENT(t)      (0x01 << (9 + (t)->id))

/* FTTMR010 interrupt status register */
#define FTTMR010_ISR_MATCH1(t)     (0x01 << ((t)->id * 3))
#define FTTMR010_ISR_MATCH2(t)     (0x02 << ((t)->id * 3))
#define FTTMR010_ISR_OVERFLOW(t)   (0x04 << ((t)->id * 3))
#define FTTMR010_ISR_TIMER(t)  \
    (FTTMR010_ISR_MATCH1(t)    \
    | FTTMR010_ISR_MATCH2(t)   \
    | FTTMR010_ISR_OVERFLOW(t))

/* FTTMR010 interrupt mask/disable register */
#define FTTMR010_IMR_MATCH1(t)     (0x01 << ((t)->id * 3))
#define FTTMR010_IMR_MATCH2(t)     (0x02 << ((t)->id * 3))
#define FTTMR010_IMR_OVERFLOW(t)   (0x04 << ((t)->id * 3))
#define FTTMR010_IMR_TIMER(t)  \
    (FTTMR010_IMR_MATCH1(t)    \
    | FTTMR010_IMR_MATCH2(t)   \
    | FTTMR010_IMR_OVERFLOW(t))

#define TYPE_FTTMR010       "fttmr010"
#define TYPE_FTTMR010_TIMER "fttmr010_timer"

typedef struct Fttmr010State Fttmr010State;

typedef struct Fttmr010Timer {
    int id;
    qemu_irq irq;
    QEMUTimer *qtimer;
    Fttmr010State *chip;
    uint64_t timestamp;
    bool ascent;
    bool m1pass;
    bool m2pass;

    /* hw registers */
    Fttmr010TimerRegs *regs;
} Fttmr010Timer;

struct Fttmr010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    uint32_t freq;  /* emulated clock rate */
    uint64_t delta; /* get_ticks_per_sec() / freq */
    qemu_irq irq;
    MemoryRegion iomem;
    Fttmr010Timer timer[3];

    /* hw registers */
    uint32_t regs[FTTMR010_REG_SIZE >> 2];
};

#define FTTMR010(obj) \
    OBJECT_CHECK(Fttmr010State, obj, TYPE_FTTMR010)

static void fttmr010_irq_update(Fttmr010Timer *t)
{
    Fttmr010State *s = t->chip;
    Fttmr010Regs *regs = (Fttmr010Regs *)s->regs;

    if (!(regs->cr & FTTMR010_CR_START(t))) {
        return;
    }

    if ((regs->isr & ~(regs->imr)) & FTTMR010_ISR_TIMER(t)) {
        qemu_irq_pulse(s->irq);
        qemu_irq_pulse(t->irq);
    }
}

static uint32_t fttmr010_curr(Fttmr010Timer *t)
{
    Fttmr010State *s = t->chip;
    Fttmr010Regs *regs = (Fttmr010Regs *)s->regs;
    uint64_t tick = (qemu_get_clock_ns(vm_clock) - t->timestamp) / s->delta;
    uint32_t curr = t->regs->counter;

    if (!(regs->cr & FTTMR010_CR_START(t))) {
        return curr;
    }

    if (t->ascent) {
        if (0xffffffff - curr <= (uint32_t)tick) {
            curr = 0xffffffff;
        } else {
            curr += (uint32_t)tick;
        }
    } else {
        if (curr <= (uint32_t)tick) {
            curr = 0;
        } else {
            curr -= (uint32_t)tick;
        }
    }

    return curr;
}

static uint32_t fttmr010_next(Fttmr010Timer *t)
{
    uint32_t tick = 0;
    uint32_t curr = fttmr010_curr(t);

    if (t->ascent) {
        if (MIN(t->regs->match1, t->regs->match2) > curr) {
            tick = MIN(t->regs->match1, t->regs->match2) - curr;
        } else if (t->regs->match1 > curr) {
            tick = t->regs->match1 - curr;
        } else if (t->regs->match2 > curr) {
            tick = t->regs->match2 - curr;
        } else {
            tick = 0xffffffff - curr;
        }
    } else {
        if (MAX(t->regs->match1, t->regs->match2) < curr) {
            tick = curr - MAX(t->regs->match1, t->regs->match2);
        } else if (t->regs->match1 < curr) {
            tick = curr - t->regs->match1;
        } else if (t->regs->match2 < curr) {
            tick = curr - t->regs->match2;
        } else {
            tick = curr;
        }
    }

    return tick;
}

static void fttmr010_restart(Fttmr010Timer *t)
{
    Fttmr010State *s = t->chip;
    Fttmr010Regs *regs = (Fttmr010Regs *)s->regs;

    t->m1pass = false;
    t->m2pass = false;

    /* check for match1 */
    if ((t->ascent && t->regs->match1 < t->regs->counter)
        || (!t->ascent && t->regs->match1 > t->regs->counter)) {
        t->m1pass = true;
    } else if (t->regs->match1 == t->regs->counter) {
        t->m1pass = true;
        regs->isr |= FTTMR010_ISR_MATCH1(t);
    }

    /* check for match2 */
    if ((t->ascent && t->regs->match2 < t->regs->counter)
        || (!t->ascent && t->regs->match2 > t->regs->counter)) {
        t->m2pass = true;
    } else if (t->regs->match2 == t->regs->counter) {
        t->m2pass = true;
        regs->isr |= FTTMR010_ISR_MATCH2(t);
    }

    /* schedule next timer event */
    t->timestamp = qemu_get_clock_ns(vm_clock);
    qemu_mod_timer(t->qtimer,
        t->timestamp + MAX(1, fttmr010_next(t)) * s->delta);

    /* update irq signals */
    fttmr010_irq_update(t);
}

static void fttmr010_timer(void *opaque)
{
    Fttmr010Timer *t = opaque;
    Fttmr010State *s = t->chip;
    Fttmr010Regs *regs = (Fttmr010Regs *)s->regs;
    uint64_t tick;

    if (!(regs->cr & FTTMR010_CR_START(t))) {
        return;
    }

    /* calculate next timeout delay */
    tick = fttmr010_next(t);

    /* if next timeout delay == 0, restart the timer */
    if (!tick) {
        if (regs->cr & FTTMR010_CR_OFIE(t)) {
            regs->isr |= FTTMR010_ISR_OVERFLOW(t);
        }
        t->regs->counter = t->regs->reload;
        fttmr010_restart(t);
    } else {
        qemu_mod_timer(t->qtimer,
            qemu_get_clock_ns(vm_clock) + MAX(1, tick) * s->delta);
    }

    /* update irq signals */
    fttmr010_irq_update(t);
}

static uint64_t fttmr010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Fttmr010State *s = FTTMR010(opaque);
    Fttmr010Timer *t;
    uint32_t ret = 0;

    if (addr >= FTTMR010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "fttmr010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case FTTMR010_TIMER_REG_START ... FTTMR010_TIMER_REG_END:
        t = s->timer + FTTMR010_TIMER_ID(addr);
        switch (addr & FTTMR010_TIMER_REG_MASK) {
        case offsetof(Fttmr010TimerRegs, counter):
            ret = fttmr010_curr(t);
            break;
        default:
            ret = s->regs[addr >> 2];
            break;
        }
        break;
    case offsetof(Fttmr010Regs, revr):
        ret = FTTMR010_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void fttmr010_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    Fttmr010State *s = FTTMR010(opaque);
    Fttmr010Timer *t;
    uint32_t old;
    int i;

    if (addr >= FTTMR010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "fttmr010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Fttmr010Regs, cr):
        old = s->regs[addr >> 2];
        for (i = 0; i < 3; ++i) {
            t = s->timer + i;
            t->ascent = !!(val & FTTMR010_CR_ASCENT(t));
            if ((old ^ val) & FTTMR010_CR_START(t)) {
                if (val & FTTMR010_CR_START(t)) {
                    fttmr010_restart(t);
                } else {
                    t->regs->counter = fttmr010_curr(t);
                    qemu_del_timer(t->qtimer);
                }
            }
        }
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    case offsetof(Fttmr010Regs, isr):
        s->regs[addr >> 2] &= ~((uint32_t)val);
        for (i = 0; i < 3; ++i) {
            fttmr010_irq_update(s->timer + i);
        }
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = fttmr010_mmio_read,
    .write = fttmr010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void fttmr010_reset(DeviceState *dev)
{
    Fttmr010State *s = FTTMR010(dev);
    int i;

    memset(s->regs, 0, FTTMR010_REG_SIZE);

    qemu_irq_lower(s->irq);

    for (i = 0; i < 3; ++i) {
        qemu_irq_lower(s->timer[i].irq);
        qemu_del_timer(s->timer[i].qtimer);
    }
}

static void fttmr010_realize(DeviceState *dev, Error **errp)
{
    Fttmr010State *s = FTTMR010(dev);
    Fttmr010Regs *regs = (Fttmr010Regs *)s->regs;
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    int i;

    s->delta = (uint64_t)get_ticks_per_sec() / (uint64_t)s->freq;

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_FTTMR010,
                          FTTMR010_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    sysbus_init_irq(sbd, &s->irq);
    for (i = 0; i < 3; ++i) {
        s->timer[i].id = i;
        s->timer[i].chip = s;
        s->timer[i].regs = regs->timer + i;
        s->timer[i].qtimer = qemu_new_timer_ns(vm_clock,
                                        fttmr010_timer, &s->timer[i]);
        sysbus_init_irq(sbd, &s->timer[i].irq);
    }
}

static const VMStateDescription vmstate_fttmr010_timer = {
    .name = TYPE_FTTMR010_TIMER,
    .version_id = 2,
    .minimum_version_id = 2,
    .minimum_version_id_old = 2,
    .fields = (VMStateField[]) {
        VMSTATE_UINT64(timestamp, Fttmr010Timer),
        VMSTATE_BOOL(ascent, Fttmr010Timer),
        VMSTATE_BOOL(m1pass, Fttmr010Timer),
        VMSTATE_BOOL(m2pass, Fttmr010Timer),
        VMSTATE_END_OF_LIST(),
    },
};

static const VMStateDescription vmstate_fttmr010 = {
    .name = TYPE_FTTMR010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(freq, Fttmr010State),
        VMSTATE_UINT64(delta, Fttmr010State),
        VMSTATE_UINT32_ARRAY(regs, Fttmr010State,
            FTTMR010_REG_SIZE >> 2),
        VMSTATE_STRUCT_ARRAY(timer, Fttmr010State, 3, 1,
            vmstate_fttmr010_timer, Fttmr010Timer),
        VMSTATE_END_OF_LIST(),
    }
};

static Property fttmr010_properties[] = {
#ifdef CONFIG_MOXART
    DEFINE_PROP_UINT32("freq", Fttmr010State, freq, 48000000),
#else
    DEFINE_PROP_UINT32("freq", Fttmr010State, freq, 66000000),
#endif

    DEFINE_PROP_END_OF_LIST(),
};

static void fttmr010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_fttmr010;
    dc->props   = fttmr010_properties;
    dc->reset   = fttmr010_reset;
    dc->realize = fttmr010_realize;
    dc->no_user = 1;
}

static const TypeInfo fttmr010_info = {
    .name          = TYPE_FTTMR010,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Fttmr010State),
    .class_init    = fttmr010_class_init,
};

static void fttmr010_register_types(void)
{
    type_register_static(&fttmr010_info);
}

type_init(fttmr010_register_types)
