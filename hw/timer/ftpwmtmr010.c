/*
 * Faraday FTPWMTMR010 Timer.
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"
#include "sysemu/sysemu.h"

#define FTPWMTMR010_REVISION    FARADAY_REVISION(0, 0, 0)

/* FTPWMTMR010 registers */
typedef struct Ftpwmtmr010TimerRegs {
    uint32_t ctrl;    /* Control */
    uint32_t cntb;    /* Counter buffer */
    uint32_t cmpb;    /* Compare buffer */
    uint32_t cnto;    /* Counter observation */
} Ftpwmtmr010TimerRegs;

typedef struct Ftpwmtmr010Regs {
    /* 0x00: status register */
    uint32_t sr;

    /* 0x04 - 0x0C: reserved */
    uint32_t rsvd[3];

    /* 0x10 - 0x8C: timer registers */
    Ftpwmtmr010TimerRegs timer[8];

    /* 0x90: revision register */
    uint32_t revr;
} Ftpwmtmr010Regs;

#define FTPWMTMR010_REG_SIZE        sizeof(Ftpwmtmr010Regs)

#define FTPWMTMR010_TIMER_ID(addr)  (((addr) - 0x10) >> 4)
#define FTPWMTMR010_TIMER_REG_MASK  0x0f
#define FTPWMTMR010_TIMER_REG_START \
    offsetof(Ftpwmtmr010Regs, timer)
#define FTPWMTMR010_TIMER_REG_END   \
    (offsetof(Ftpwmtmr010Regs, revr) - 4)

/* FTPWMTMR010 timer control register */
#define FTPWMTMR010_TIMER_CTRL_EXTCLK   BIT(0)  /* external clock */
#define FTPWMTMR010_TIMER_CTRL_START    BIT(1)
#define FTPWMTMR010_TIMER_CTRL_UPDATE   BIT(2)
#define FTPWMTMR010_TIMER_CTRL_PERIODIC BIT(4)
#define FTPWMTMR010_TIMER_CTRL_IRQ      BIT(5)  /* interrupt enabled */
#define FTPWMTMR010_TIMER_CTRL_IRQ_EDGE BIT(6)  /* edge triggered interrupt */

#define TYPE_FTPWMTMR010        "ftpwmtmr010"
#define TYPE_FTPWMTMR010_TIMER  "ftpwmtmr010_timer"

typedef struct Ftpwmtmr010State Ftpwmtmr010State;

typedef struct Ftpwmtmr010Timer {
    int id;
    qemu_irq irq;
    QEMUTimer *qtimer;
    uint64_t timeout;
    uint64_t period;
    Ftpwmtmr010State *chip;

    /* hw registers */
    Ftpwmtmr010TimerRegs *regs;
} Ftpwmtmr010Timer;

struct Ftpwmtmr010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    uint32_t freq;  /* emulated clock rate */
    uint64_t delta; /* get_ticks_per_sec() / freq */
    MemoryRegion iomem;
    Ftpwmtmr010Timer timer[8];

    /* hw registers */
    uint32_t regs[FTPWMTMR010_REG_SIZE >> 2];
};

#define FTPWMTMR010(obj) \
    OBJECT_CHECK(Ftpwmtmr010State, obj, TYPE_FTPWMTMR010)

static uint64_t ftpwmtmr010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftpwmtmr010Timer *t;
    Ftpwmtmr010State *s = FTPWMTMR010(opaque);
    uint64_t now = qemu_get_clock_ns(vm_clock);
    uint32_t ret = 0;

    if (addr >= FTPWMTMR010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftpwmtmr010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return ret;
    }

    switch (addr) {
    case FTPWMTMR010_TIMER_REG_START ... FTPWMTMR010_TIMER_REG_END:
        t = s->timer + FTPWMTMR010_TIMER_ID(addr);
        switch (addr & FTPWMTMR010_TIMER_REG_MASK) {
        case offsetof(Ftpwmtmr010TimerRegs, cnto):
            if ((t->timeout > now)
                && (t->regs->ctrl & FTPWMTMR010_TIMER_CTRL_START)) {
                ret = (t->timeout - now) / s->delta;
            }
            break;
        default:
            ret = s->regs[addr >> 2];
            break;
        }
        break;
    case offsetof(Ftpwmtmr010Regs, revr):
        ret = FTPWMTMR010_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftpwmtmr010_mmio_write(void *opaque, hwaddr addr,
        uint64_t val, unsigned size)
{
    Ftpwmtmr010Timer *t;
    Ftpwmtmr010State *s = FTPWMTMR010(opaque);
    int i;

    if (addr >= FTPWMTMR010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftpwmtmr010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftpwmtmr010Regs, sr):
        s->regs[addr >> 2] &= ~((uint32_t)val);
        for (i = 0; i < 8; ++i) {
            if (val & BIT(i)) {
                t = s->timer + i;
                qemu_irq_lower(t->irq);
            }
        }
        break;
    case FTPWMTMR010_TIMER_REG_START ... FTPWMTMR010_TIMER_REG_END:
        t = s->timer + FTPWMTMR010_TIMER_ID(addr);
        switch (addr & FTPWMTMR010_TIMER_REG_MASK) {
        case offsetof(Ftpwmtmr010TimerRegs, ctrl):
            if (val & FTPWMTMR010_TIMER_CTRL_UPDATE) {
                t->period = (uint64_t)t->regs->cntb * s->delta;
                val &= ~FTPWMTMR010_TIMER_CTRL_UPDATE;
            }
            if ((t->regs->ctrl ^ val) & FTPWMTMR010_TIMER_CTRL_START) {
                if (val & FTPWMTMR010_TIMER_CTRL_START) {
                    t->timeout = t->period + qemu_get_clock_ns(vm_clock);
                    qemu_mod_timer(t->qtimer, t->timeout);
                } else {
                    qemu_del_timer(t->qtimer);
                }
            }
            t->regs->ctrl = (uint32_t)val;
            break;
        case offsetof(Ftpwmtmr010TimerRegs, cnto):
            break;
        default:
            s->regs[addr >> 2] = (uint32_t)val;
            break;
        }
        break;
    default:
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftpwmtmr010_mmio_read,
    .write = ftpwmtmr010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void ftpwmtmr010_timer(void *opaque)
{
    Ftpwmtmr010Timer *t = opaque;
    Ftpwmtmr010State *s = t->chip;
    Ftpwmtmr010Regs *regs = (Ftpwmtmr010Regs *)s->regs;

    /* if the timer has been enabled/started */
    if (!(t->regs->ctrl & FTPWMTMR010_TIMER_CTRL_START)) {
        return;
    }

    /* if the interrupt enabled */
    if (t->regs->ctrl & FTPWMTMR010_TIMER_CTRL_IRQ) {
        regs->sr |= BIT(t->id);
        if (t->regs->ctrl & FTPWMTMR010_TIMER_CTRL_IRQ_EDGE) {
            qemu_irq_pulse(t->irq);
        } else {
            qemu_irq_raise(t->irq);
        }
    }

    /* if auto-reload is enabled */
    if (t->regs->ctrl & FTPWMTMR010_TIMER_CTRL_PERIODIC) {
        t->timeout = t->period + qemu_get_clock_ns(vm_clock);
        qemu_mod_timer(t->qtimer, t->timeout);
    } else {
        t->regs->ctrl &= ~FTPWMTMR010_TIMER_CTRL_START;
    }
}

static void ftpwmtmr010_reset(DeviceState *dev)
{
    Ftpwmtmr010State *s = FTPWMTMR010(dev);
    int i;

    memset(s->regs, 0, FTPWMTMR010_REG_SIZE);

    for (i = 0; i < 8; ++i) {
        s->timer[i].timeout = 0;
        qemu_irq_lower(s->timer[i].irq);
        qemu_del_timer(s->timer[i].qtimer);
    }
}

static void ftpwmtmr010_realize(DeviceState *dev, Error **errp)
{
    Ftpwmtmr010State *s = FTPWMTMR010(dev);
    Ftpwmtmr010Regs *regs = (Ftpwmtmr010Regs *)s->regs;
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);
    int i;

    s->delta = (uint64_t)get_ticks_per_sec() / (uint64_t)s->freq;

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_FTPWMTMR010,
                          FTPWMTMR010_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    for (i = 0; i < 8; ++i) {
        s->timer[i].id = i;
        s->timer[i].chip = s;
        s->timer[i].regs = regs->timer + i;
        s->timer[i].qtimer = qemu_new_timer_ns(vm_clock,
            ftpwmtmr010_timer, &s->timer[i]);
        sysbus_init_irq(sbd, &s->timer[i].irq);
    }
}

static const VMStateDescription vmstate_ftpwmtmr010_timer = {
    .name = TYPE_FTPWMTMR010_TIMER,
    .version_id = 2,
    .minimum_version_id = 2,
    .minimum_version_id_old = 2,
    .fields = (VMStateField[]) {
        VMSTATE_UINT64(period, Ftpwmtmr010Timer),
        VMSTATE_UINT64(timeout, Ftpwmtmr010Timer),
        VMSTATE_END_OF_LIST(),
    },
};

static const VMStateDescription vmstate_ftpwmtmr010 = {
    .name = TYPE_FTPWMTMR010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(freq, Ftpwmtmr010State),
        VMSTATE_UINT64(delta, Ftpwmtmr010State),
        VMSTATE_UINT32_ARRAY(regs, Ftpwmtmr010State,
            FTPWMTMR010_REG_SIZE >> 2),
        VMSTATE_STRUCT_ARRAY(timer, Ftpwmtmr010State,
            8, 1, vmstate_ftpwmtmr010_timer, Ftpwmtmr010Timer),
        VMSTATE_END_OF_LIST(),
    }
};

static Property ftpwmtmr010_properties[] = {
    DEFINE_PROP_UINT32("freq", Ftpwmtmr010State, freq, 66000000),
    DEFINE_PROP_END_OF_LIST(),
};

static void ftpwmtmr010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftpwmtmr010;
    dc->props   = ftpwmtmr010_properties;
    dc->reset   = ftpwmtmr010_reset;
    dc->realize = ftpwmtmr010_realize;
    dc->no_user = 1;
}

static const TypeInfo ftpwmtmr010_info = {
    .name          = TYPE_FTPWMTMR010,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftpwmtmr010State),
    .class_init    = ftpwmtmr010_class_init,
};

static void ftpwmtmr010_register_types(void)
{
    type_register_static(&ftpwmtmr010_info);
}

type_init(ftpwmtmr010_register_types)
