/*
 * Faraday FTGPIO010 GPIO Controller.
 *
 * Copyright (c) 2013 by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/bitops.h"

typedef struct Ftgpio010Regs {
    uint32_t outr; /* data output register*/
    uint32_t inr; /* data input register */
    uint32_t dir; /* direction register */
    uint32_t rsvd0;
/* output */
    uint32_t set; /* data bit set register */ 
    uint32_t clr; /* data bit clear register */
/* input */
    uint32_t pen; /* pull enable register register */
    uint32_t pty; /* pull high or low register */
    uint32_t ien; /* interrupt enable register*/
    uint32_t irs; /* interrupt raw status register */
    uint32_t ims; /* interrupt masked status register */
    uint32_t im;  /* interrupt mask register */
    uint32_t ic;  /* interrupt clear */
    uint32_t it;  /* interrupt trigger register */
    uint32_t ib;  /* interrupt trigger by both */
    uint32_t irn; /* interrupt trigger by rising or falling edge */
    uint32_t ben; /* bounce enable */
    uint32_t bsc; /* bounce pre scale */
} Ftgpio010Regs;

#define FTGPIO010_REG_SIZE  sizeof(Ftgpio010Regs)

#define TYPE_FTGPIO010      "ftgpio010"

typedef struct Ftgpio010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    qemu_irq irq;

    qemu_irq handler[32];

    /* HW register caches */
    uint32_t regs[FTGPIO010_REG_SIZE >> 2];
} Ftgpio010State;

#define FTGPIO010(obj) \
    OBJECT_CHECK(Ftgpio010State, obj, TYPE_FTGPIO010)

/* Note: Here level means state of the signal on a pin */
static void ftgpio010_set_gpio(void *opaque, int line, int level)
{
    Ftgpio010State *s = FTGPIO010(opaque);
    Ftgpio010Regs *regs = (Ftgpio010Regs *)s->regs;
    uint32_t mask = BIT(line & 0x1f);
    uint32_t prev = regs->inr;

    if (level)
        regs->inr |= mask;
    else
        regs->inr &= ~mask;

    if (((regs->it & regs->inr & ~prev) | (~regs->it & ~regs->inr & prev)) &
                    mask & ~regs->dir/* & ~s->ims*/) {
        regs->irs |= mask;
        qemu_irq_raise(s->irq);
    }
}

static uint64_t ftgpio010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{

    Ftgpio010State *s = FTGPIO010(opaque);
    Ftgpio010Regs *regs = (Ftgpio010Regs *)s->regs;
    uint64_t ret = 0;

    if (addr >= FTGPIO010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftgpio010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftgpio010Regs, irs):
        ret = regs->irs & regs->ien;
        break;
    case offsetof(Ftgpio010Regs, ims):
        ret = (regs->irs & regs->ien) & ~regs->im;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    fprintf(stderr, "ftgpio010 r@0x%02" HWADDR_PRIx " val=0x%08" PRIx64 "\n", addr, ret);

    return ret;
}

static void ftgpio010_mmio_write(void *opaque, hwaddr addr,
        uint64_t val, unsigned size)
{
    Ftgpio010State *s = FTGPIO010(opaque);
    Ftgpio010Regs *regs = (Ftgpio010Regs *)s->regs;
    uint32_t diff, old;
    int ln;

    if (addr >= FTGPIO010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftgpio010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    fprintf(stderr, "ftgpio010 w@0x%02" HWADDR_PRIx " val=0x%08" PRIx64 "\n", addr, val);

    switch (addr) {
    case offsetof(Ftgpio010Regs, outr):
        old = regs->outr;
        regs->outr = val;
	regs->inr = (regs->inr ^ old) | val;
        diff = (old ^ regs->outr) & regs->dir;

        while ((ln = ffs(diff))) {
            ln--;
            if (s->handler[ln])
                qemu_set_irq(s->handler[ln], (regs->outr >> ln) & 1);
            diff &= ~(1 << ln);
        }
        break;
    case offsetof(Ftgpio010Regs, dir):
        diff = regs->outr & (regs->dir ^ val);

        regs->dir = val;

        val = regs->outr & regs->dir;
        while ((ln = ffs(diff))) {
            ln --;
            if (s->handler[ln])
                qemu_set_irq(s->handler[ln], (val >> ln) & 1);
            diff &= ~(1 << ln);
        }
        break;
    case offsetof(Ftgpio010Regs, set):
        old = regs->outr;
        regs->outr |= val;
	regs->inr = (regs->inr ^ old) | val;

        diff = (old ^ regs->outr) & regs->dir;

        while ((ln = ffs(diff))) {
            ln --;
            if (s->handler[ln])
                qemu_set_irq(s->handler[ln], (regs->outr >> ln) & 1);
            diff &= ~(1 << ln);
        }
        break;
    case offsetof(Ftgpio010Regs, clr):
        old = regs->outr;
        regs->outr &= ~val;
	regs->inr = (regs->inr ^ old) | val;

        diff = (old ^ regs->outr) & regs->dir;

        while ((ln = ffs(diff))) {
            ln --;
            if (s->handler[ln])
                qemu_set_irq(s->handler[ln], (regs->outr >> ln) & 1);
            diff &= ~(1 << ln);
        }
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftgpio010_mmio_read,
    .write = ftgpio010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static VMStateDescription vmstate_ftgpio010 = {
    .name = TYPE_FTGPIO010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Ftgpio010State, FTGPIO010_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    },
};

static void ftgpio010_realize(DeviceState *dev, Error **errp)
{
    Ftgpio010State *s = FTGPIO010(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_FTGPIO010,
                          FTGPIO010_REG_SIZE);

    sysbus_init_mmio(sbd, &s->iomem);

    qdev_init_gpio_in(&sbd->qdev, ftgpio010_set_gpio, 32);
    qdev_init_gpio_out(&sbd->qdev, s->handler, 32);
    sysbus_init_irq(sbd, &s->irq);
}

static void ftgpio010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftgpio010;
    dc->realize = ftgpio010_realize;
    dc->no_user = 1;
}

static const TypeInfo ftgpio010_info = {
    .name          = TYPE_FTGPIO010,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftgpio010State),
    .class_init    = ftgpio010_class_init,
};

static void ftgpio010_register_types(void)
{
    type_register_static(&ftgpio010_info);
}

type_init(ftgpio010_register_types)
