/*
 * Faraday FTTSC010 touchscreen emulator.
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/devices.h"
#include "hw/arm/faraday.h"
#include "ui/console.h"
#include "qemu/timer.h"
#include "qemu/bitops.h"

#define TYPE_FTTSC010       "fttsc010"
#define FTTSC010_REVISION   FARADAY_REVISION(1, 0, 0)

#define X_AXIS_DMAX         3470
#define X_AXIS_MIN          290
#define Y_AXIS_DMAX         3450
#define Y_AXIS_MIN          200

#define ADS_XPOS(x, y)  \
    (X_AXIS_MIN + ((X_AXIS_DMAX * (x)) >> 15))
#define ADS_YPOS(x, y)  \
    (Y_AXIS_MIN + ((Y_AXIS_DMAX * (y)) >> 15))
#define ADS_Z1POS(x, y) 8
#define ADS_Z2POS(x, y) \
    ((1600 + ADS_XPOS(x, y)) * ADS_Z1POS(x, y) / ADS_XPOS(x, y))

typedef struct Fttsc010Regs {
    uint32_t cr;    /* Control Register */
    uint32_t isr;   /* Interrupt Status Register */
    uint32_t imr;   /* Interrupt Mask Register */
    uint32_t revr;  /* Revision Register */
    uint32_t rsvd0[8];
    uint32_t csr;   /* Clock & Sample Rate Register */
    uint32_t pfr;   /* Panel Function Register */
    uint32_t dcr;   /* Delay Control Register */
    uint32_t xyr;   /* Touchscreen X,Y-Axis Register */
    uint32_t rsvd1[3];
    uint32_t pmr;   /* Pressure Measurement Register */
} Fttsc010Regs;

#define FTTSC010_REG_SIZE       sizeof(Fttsc010Regs)

/* Control Register */
#define FTTSC010_CR_AS          (1 << 31) /* enable auto-scan mode */
#define FTTSC010_CR_1SHOT       (1 << 30) /* enable 1-shot mode */

/* Interrupt Status Register */
#define FTTSC010_ISR_AS         (1 << 10) /* auto-scan */

/* Clock & Sample Rate Register */
#define FTTSC010_CSR_SDIV(x)    \
    (1 + extract32((uint32_t)(x), 8, 8)) /* sample clock divider */
#define FTTSC010_CSR_MDIV(x)    \
    (1 + extract32((uint32_t)(x), 0, 8)) /* main clock divider */

/* Delay Control Register */
#define FTTSC010_DCR_CDLY(x)     \
    (1 + extract32((uint32_t)(x), 0, 16))/* conversion delay */

#define FTTSC010_XYR(x, y)      \
    (((x) & 0xfff) | (((y) & 0xfff) << 16))

#define FTTSC010_PMR(z1, z2)      \
    (((z1) & 0xfff) | (((z2) & 0xfff) << 16))

typedef struct Fttsc010State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;
    qemu_irq irq;

    uint64_t period;
    QEMUTimer *qtimer;

    int x, y, z1, z2;
    uint32_t freq;

    /* HW registers */
    uint32_t regs[FTTSC010_REG_SIZE];
} Fttsc010State;

#define FTTSC010(obj) \
    OBJECT_CHECK(Fttsc010State, obj, TYPE_FTTSC010)

static void fttsc010_irq_update(Fttsc010State *s)
{
    Fttsc010Regs *regs = (Fttsc010Regs *)s->regs;

    qemu_set_irq(s->irq, !!(regs->imr & regs->isr));
}

static uint64_t fttsc010_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Fttsc010State *s = FTTSC010(opaque);
    uint32_t ret = 0;

    if (addr >= FTTSC010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "fttsc010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Fttsc010Regs, revr):
        ret = FTTSC010_REVISION;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void fttsc010_mmio_write(void *opaque, hwaddr addr, uint64_t val,
        unsigned size)
{
    uint32_t cdly, sdiv, mdiv;
    Fttsc010State *s = FTTSC010(opaque);
    Fttsc010Regs *regs = (Fttsc010Regs *)s->regs;

    if (addr >= FTTSC010_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "fttsc010: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Fttsc010Regs, cr):
        if ((val ^ regs->cr) & (FTTSC010_CR_AS | FTTSC010_CR_1SHOT)) {
            if (val & (FTTSC010_CR_AS | FTTSC010_CR_1SHOT)) {
                /* ADC conversion delay with frame number */
                cdly = FTTSC010_DCR_CDLY(regs->dcr);
                /* ADC sample clock divider */
                sdiv = FTTSC010_CSR_SDIV(regs->csr);
                /* ADC main clock divider */
                mdiv = FTTSC010_CSR_MDIV(regs->csr);
                /* Calculate sample rate/timer period */
                s->period = s->freq / (mdiv * sdiv * cdly * 64);
                qemu_mod_timer(s->qtimer,
                    MAX(1, s->period) + qemu_get_clock_ms(vm_clock));
            } else {
                qemu_del_timer(s->qtimer);
            }
        }
        regs->cr = (uint32_t)val;
        break;
    case offsetof(Fttsc010Regs, isr):
        regs->isr &= (uint32_t)(~val);
        fttsc010_irq_update(s);
        break;
    case offsetof(Fttsc010Regs, imr):
        regs->imr = (uint32_t)val;
        fttsc010_irq_update(s);
        break;
    /* read-only registers */
    case offsetof(Fttsc010Regs, revr):
    case offsetof(Fttsc010Regs, xyr):
    case offsetof(Fttsc010Regs, pmr):
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }
}

static const MemoryRegionOps mmio_ops = {
    .read  = fttsc010_mmio_read,
    .write = fttsc010_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void fttsc010_timer(void *opaque)
{
    Fttsc010State *s = FTTSC010(opaque);
    Fttsc010Regs *regs = (Fttsc010Regs *)s->regs;

    /* clear 1-shot read control */
    regs->cr &= ~FTTSC010_CR_1SHOT;

    /* update auto-scan interrupt status */
    if (regs->cr & FTTSC010_CR_AS) {
        regs->isr |= FTTSC010_ISR_AS;
    }

    /* update interrupt signal */
    fttsc010_irq_update(s);

    /* re-schedule */
    if (regs->cr & FTTSC010_CR_AS) {
        qemu_mod_timer(s->qtimer,
            MAX(1, s->period) + qemu_get_clock_ms(vm_clock));
    }
}

static void fttsc010_ts_event(void *opaque, int x, int y, int z, int bt)
{
    Fttsc010State *s = FTTSC010(opaque);
    Fttsc010Regs *regs = (Fttsc010Regs *)s->regs;

    if (bt) {
        /* button pressed */
        x = 0x7fff - x;
        s->x = ADS_XPOS(x, y);
        s->y = ADS_YPOS(x, y);
        s->z1 = ADS_Z1POS(x, y);
        s->z2 = ADS_Z2POS(x, y);
    } else {
        /* button released */
        s->z1 = 0;
        s->z2 = 0;
    }

    regs->xyr = FTTSC010_XYR(s->x, s->y);
    regs->pmr = FTTSC010_PMR(s->z1, s->z2);
}

static void fttsc010_reset(DeviceState *dev)
{
    Fttsc010State *s = FTTSC010(dev);

    s->x  = 0;
    s->y  = 0;
    s->z1 = 0;
    s->z2 = 0;
    memset(s->regs, 0, sizeof(s->regs));
    qemu_set_irq(s->irq, 0);
}

static void fttsc010_realize(DeviceState *dev, Error **errp)
{
    Fttsc010State *s = FTTSC010(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    s->qtimer = qemu_new_timer_ms(vm_clock, fttsc010_timer, s);

    memory_region_init_io(&s->iomem, &mmio_ops, s,
        TYPE_FTTSC010, FTTSC010_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);
    sysbus_init_irq(sbd, &s->irq);

    qemu_add_mouse_event_handler(fttsc010_ts_event, s, 1,
                    "QEMU FTTSC010-driven Touchscreen");
}

static const VMStateDescription vmstate_fttsc010 = {
    .name = TYPE_FTTSC010,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32_ARRAY(regs, Fttsc010State, FTTSC010_REG_SIZE),
        VMSTATE_END_OF_LIST(),
    }
};

static Property properties_fttsc010[] = {
    DEFINE_PROP_UINT32("freq", Fttsc010State, freq, 66000000),
    DEFINE_PROP_END_OF_LIST(),
};

static void fttsc010_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->reset   = fttsc010_reset;
    dc->realize = fttsc010_realize;
    dc->vmsd    = &vmstate_fttsc010;
    dc->props   = properties_fttsc010;
    dc->no_user = 1;
}

static const TypeInfo fttsc010_info = {
    .name          = TYPE_FTTSC010,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Fttsc010State),
    .class_init    = fttsc010_class_init,
};

static void fttsc010_register_types(void)
{
    type_register_static(&fttsc010_info);
}

type_init(fttsc010_register_types)
