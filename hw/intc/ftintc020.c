/*
 * Faraday FTINTC020 Interrupt Controller.
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Dante Su <dantesu@faraday-tech.com>
 *
 * This code is licensed under GNU GPL v2+.
 */

#include "hw/hw.h"
#include "hw/sysbus.h"
#include "hw/arm/faraday.h"
#include "qemu/bitops.h"

#define FTINTC020_REVISION  FARADAY_REVISION(1, 0, 0)
#define FTINTC020_FEATURE   0x4040  /* 64 IRQ/FIQ */

#ifdef CONFIG_MOXART
#undef FTINTC020_REVISION
#undef FTINTC020_FEATURE
#define FTINTC020_REVISION  FARADAY_REVISION(1, 5, 0)
#define FTINTC020_FEATURE   0x2020  /* 32 IRQ/FIQ */
#endif

typedef struct Ftintc020PicRegs {
    uint32_t src; /* source register */
    uint32_t ena; /* enable register */
    uint32_t scr; /* status clear register */
    uint32_t tmr; /* trigger mode register */
    uint32_t tlr; /* trigger level register */
    uint32_t sr;  /* status register */
    uint32_t rsvd[2];
} Ftintc020PicRegs;

typedef struct Ftintc020Regs {
    /* IRQ/FIQ:  0 ~ 31 */
    Ftintc020PicRegs irq32; /* 0x00 - 0x1C: IRQ 0 ~ 31 */
    Ftintc020PicRegs fiq32; /* 0x20 - 0x3C: FIQ 0 ~ 31 */
    uint32_t rsvd1[4];      /* 0x40 - 0x4C: Reserved */
    uint32_t revr;          /* 0x50: Revision Register */
    uint32_t fear;          /* 0x54: Feature Register */
    uint32_t rsvd2[2];      /* 0x58 - 0x5C: Reserved */
    /* IRQ/FIQ: 32 ~ 63 */
#if FTINTC020_FEATURE == 0x4040
    Ftintc020PicRegs irq64; /* 0x60 - 0x7C: IRQ 32 ~ 63 */
    Ftintc020PicRegs fiq64; /* 0x80 - 0x9C: FIQ 32 ~ 63 */
#endif
} Ftintc020Regs;

#define FTINTC020_REG_SIZE  sizeof(Ftintc020Regs)

#define TYPE_FTINTC020      "ftintc020"

typedef struct Ftintc020State {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    MemoryRegion iomem;

    qemu_irq irq;
    qemu_irq fiq;

#if FTINTC020_FEATURE == 0x4040
    uint64_t pin; /* device pin state */
#else
    uint32_t pin; /* device pin state */
#endif

    /* HW register caches */
    uint32_t regs[FTINTC020_REG_SIZE >> 2];
} Ftintc020State;

#define FTINTC020(obj) \
    OBJECT_CHECK(Ftintc020State, obj, TYPE_FTINTC020)

static Ftintc020PicRegs *ftintc020_get_pic(Ftintc020State *s, int nr, bool fiq)
{
    Ftintc020Regs *regs = (Ftintc020Regs *)s->regs;
    Ftintc020PicRegs *pic = NULL;

    if (fiq) {
#if FTINTC020_FEATURE == 0x4040
        pic = (nr > 31) ? &regs->fiq64 : &regs->fiq32;
#else
        pic = &regs->fiq32;
#endif
    } else {
#if FTINTC020_FEATURE == 0x4040
        pic = (nr > 31) ? &regs->irq64 : &regs->irq32;
#else
        pic = &regs->irq32;
#endif
    }

    return pic;
}

static void ftintc020_update(Ftintc020State *s)
{
    Ftintc020Regs *regs = (Ftintc020Regs *)s->regs;

    /* FIQ */
    regs->fiq32.sr = regs->fiq32.src & regs->fiq32.ena;
#if FTINTC020_FEATURE == 0x4040
    regs->fiq64.sr = regs->fiq64.src & regs->fiq64.ena;
    qemu_set_irq(s->fiq, !!(regs->fiq32.sr || regs->fiq64.sr));
#else
    qemu_set_irq(s->fiq, !!regs->fiq32.sr);
#endif

    /* IRQ */
    regs->irq32.sr = regs->irq32.src & regs->irq32.ena;
#if FTINTC020_FEATURE == 0x4040
    regs->irq64.sr = regs->irq64.src & regs->irq64.ena;
    qemu_set_irq(s->irq, !!(regs->irq32.sr || regs->irq64.sr));
#else
    qemu_set_irq(s->irq, !!regs->irq32.sr);
#endif
}

/* pin transition state */
typedef enum PTState {
    PTS_LOW = 0, /* BIT[1-0] = 00 */
    PTS_RISING,  /* BIT[1-0] = 01 */
    PTS_FALLING, /* BIT[1-0] = 10 */
    PTS_HIGH,    /* BIT[1-0] = 11 */
} PTState;

static void ftintc020_update_pic(Ftintc020PicRegs *pic, int irq, PTState pts)
{
    uint32_t mask = BIT(irq & 0x1f);

    if (pic->tmr & mask) {  /* edge triggered */
        switch (pts) {
        case PTS_RISING:
            pic->src |= (pic->tlr & mask) ? 0 : mask;
            break;
        case PTS_FALLING:
            pic->src |= (pic->tlr & mask) ? mask : 0;
            break;
        default:
            break;
        }
    } else {                /* level triggered */
        if (pic->tlr & mask) {
            if (!(pts & 1)) {
                pic->src |= mask;
            } else {
                pic->src &= ~mask;
            }
        } else {
            if (pts & 1) {
                pic->src |= mask;
            } else {
                pic->src &= ~mask;
            }
        }
    }
}

/* Note: Here level means state of the signal on a pin */
static void ftintc020_set_irq(void *opaque, int irq, int level)
{
    Ftintc020State *s = FTINTC020(opaque);
    Ftintc020PicRegs *pic;
#if FTINTC020_FEATURE == 0x4040
    uint64_t msk;
#else
    uint32_t msk;
#endif
    PTState pts;

#if FTINTC020_FEATURE == 0x4040
    if (irq < 0 || irq > 63) {
#else
    if (irq < 0 || irq > 31) {
#endif
        fprintf(stderr, "ftintc020: undefined irq=%d\n", irq);
        abort();
    }

#if FTINTC020_FEATURE == 0x4040
    msk = (uint64_t)1 << irq;
#else
    msk = BIT(irq);
#endif
    pts = ((s->pin & msk) ? 2 : 0) + (level ? 1 : 0);

    /* FIQ */
    pic = ftintc020_get_pic(s, irq, true);
    ftintc020_update_pic(pic, irq, pts);

    /* IRQ */
    pic = ftintc020_get_pic(s, irq, false);
    ftintc020_update_pic(pic, irq, pts);

    /* Update device pin state */
    if (level) {
        s->pin |= msk;
    } else {
        s->pin &= ~msk;
    }

    /* Update interrupt signals */
    ftintc020_update(s);
}

static uint64_t ftintc020_mmio_read(void *opaque, hwaddr addr, unsigned size)
{
    Ftintc020State *s = FTINTC020(opaque);
    uint32_t ret = 0;

    if (addr >= FTINTC020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftintc020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return 0;
    }

    switch (addr) {
    case offsetof(Ftintc020Regs, revr):
        ret = FTINTC020_REVISION;
        break;
    case offsetof(Ftintc020Regs, fear):
        ret = FTINTC020_FEATURE;
        break;
    default:
        ret = s->regs[addr >> 2];
        break;
    }

    return ret;
}

static void ftintc020_mmio_write(void *opaque, hwaddr addr,
        uint64_t val, unsigned size)
{
    Ftintc020State *s = FTINTC020(opaque);
    Ftintc020PicRegs *pic;

    if (addr >= FTINTC020_REG_SIZE) {
        qemu_log_mask(LOG_GUEST_ERROR,
            "ftintc020: undefined memory access@%#" HWADDR_PRIx "\n", addr);
        return;
    }

    switch (addr) {
    case offsetof(Ftintc020Regs, irq32) + offsetof(Ftintc020PicRegs, scr):
    case offsetof(Ftintc020Regs, fiq32) + offsetof(Ftintc020PicRegs, scr):
#if FTINTC020_FEATURE == 0x4040
    case offsetof(Ftintc020Regs, irq64) + offsetof(Ftintc020PicRegs, scr):
    case offsetof(Ftintc020Regs, fiq64) + offsetof(Ftintc020PicRegs, scr):
#endif
        addr -= offsetof(Ftintc020PicRegs, scr);
        pic = (Ftintc020PicRegs *)(s->regs + (addr >> 2));
        /* clear edge triggered interrupts only */
        val = ~(val & pic->tmr);
        pic->src &= (uint32_t)val;
        break;
    default:
        s->regs[addr >> 2] = (uint32_t)val;
        break;
    }

    ftintc020_update(s);
}

static const MemoryRegionOps mmio_ops = {
    .read  = ftintc020_mmio_read,
    .write = ftintc020_mmio_write,
    .endianness = DEVICE_LITTLE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void ftintc020_reset(DeviceState *dev)
{
    Ftintc020State *s = FTINTC020(dev);

    s->pin = 0;
    memset(s->regs, 0, sizeof(s->regs));

    ftintc020_update(s);
}

static VMStateDescription vmstate_ftintc020 = {
    .name = TYPE_FTINTC020,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT64(pin, Ftintc020State),
        VMSTATE_UINT32_ARRAY(regs, Ftintc020State, FTINTC020_REG_SIZE >> 2),
        VMSTATE_END_OF_LIST(),
    },
};

static void ftintc020_realize(DeviceState *dev, Error **errp)
{
    Ftintc020State *s = FTINTC020(dev);
    SysBusDevice *sbd = SYS_BUS_DEVICE(dev);

    memory_region_init_io(&s->iomem,
                          &mmio_ops,
                          s,
                          TYPE_FTINTC020,
                          FTINTC020_REG_SIZE);
    sysbus_init_mmio(sbd, &s->iomem);

    qdev_init_gpio_in(&sbd->qdev, ftintc020_set_irq, 64);
    sysbus_init_irq(sbd, &s->irq);
    sysbus_init_irq(sbd, &s->fiq);
}

static void ftintc020_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->vmsd    = &vmstate_ftintc020;
    dc->reset   = ftintc020_reset;
    dc->realize = ftintc020_realize;
    dc->no_user = 1;
}

static const TypeInfo ftintc020_info = {
    .name          = TYPE_FTINTC020,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(Ftintc020State),
    .class_init    = ftintc020_class_init,
};

static void ftintc020_register_types(void)
{
    type_register_static(&ftintc020_info);
}

type_init(ftintc020_register_types)
