/*
 * Faraday A360 SoC emulation
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2.
 */

#ifndef HW_ARM_FARADAY_A360_H
#define HW_ARM_FARADAY_A360_H

#define TYPE_FARADAY_A360   "faraday.a360"

typedef struct FaradayA360State {
    FaradaySocState common;

    ARMCPU *cpu;
    qemu_irq pic[32];

    MemoryRegion *as;       /* Address Space */
    MemoryRegion *ram;      /* SDRAM(DDR) */
} FaradayA360State;

#define FARADAY_A360(obj) \
    OBJECT_CHECK(FaradayA360State, obj, TYPE_FARADAY_A360)

FaradayA360State *faraday_a360_init(const char *cpu, uint64_t ram_size);

#endif  /* HW_ARM_FARADAY_A360_H */
