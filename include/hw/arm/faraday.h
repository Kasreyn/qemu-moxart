/*
 * Faraday Platform SoC emulation
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2.
 */

#ifndef HW_ARM_FARADAY_H
#define HW_ARM_FARADAY_H

typedef struct FaradaySocState {
    /*< private >*/
    SysBusDevice parent;

    /*< public >*/
    void (*ahbc_init)(void *opaque, uint32_t *regs);
    void (*ahbc_remap)(void *opaque, bool on);
} FaradaySocState;

/* revision = x.y.z */
#define FARADAY_REVISION(x, y, z)   \
    ((((x) & 0xff) << 16) | (((y) & 0xff) << 8) | ((z) & 0xff))

/* ftssp010.c */
void ftssp010_i2s_data_req(void *opaque, int tx, int rx);

/* ftgmac100.c */
void ftgmac100_init(NICInfo *nd, uint32_t base, qemu_irq irq);

/* ftmac110.c */
void ftmac110_init(NICInfo *nd, uint32_t base, qemu_irq irq);

/* ftmac100.c */
void ftmac100_init(NICInfo *nd, uint32_t base, qemu_irq irq);

/* soc headers */
#include "hw/arm/faraday_a360.h"
#include "hw/arm/faraday_a369.h"
#include "hw/arm/moxa_art.h"

#endif  /* HW_ARM_FARADAY_H */
