/*
 * Faraday A369 SoC emulation
 *
 * Copyright (c) 2013 Faraday Technology
 * Written by Kuo-Jung Su <dantesu@gmail.com>
 *
 * This code is licensed under the GNU GPL v2.
 */

#ifndef HW_ARM_FARADAY_A369_H
#define HW_ARM_FARADAY_A369_H

#define TYPE_FARADAY_A369   "faraday.a369"

typedef struct FaradayA369State {
    FaradaySocState common;

    ARMCPU *cpu;
    qemu_irq pic[64];

    MemoryRegion *as;       /* Address Space */
    MemoryRegion *ram;      /* SDRAM(DDR) */
    MemoryRegion *rom;      /* On-Chip ROM */
    MemoryRegion *sram;     /* On-Chip SRAM */

    DeviceState  *nandc;    /* NAND flash controller */
    DeviceState  *i2c[2];   /* I2C bus */
    DeviceState  *i2s[2];   /* I2S bus */
    DeviceState  *spi[2];   /* SPI bus */

    bool romboot;
} FaradayA369State;

#define FARADAY_A369(obj) \
    OBJECT_CHECK(FaradayA369State, obj, TYPE_FARADAY_A369)

FaradayA369State *faraday_a369_init(const char *cpu, uint64_t ram_size);

#endif  /* HW_ARM_FARADAY_A369_H */
