/*
 * MOXA ART SoC emulation
 *
 * Copyright (c) 2013 Eleven, Twenty-two
 * Written by Adam Jaremko <adam.jaremko@gmail.com>
 *
 * This code is licensed under the GNU GPL v2.
 */

#ifndef HW_ARM_MOXA_ART_H
#define HW_ARM_MOXA_ART_H

#define TYPE_MOXA_ART   "moxa.art"

typedef struct MoxaArtState {
    FaradaySocState common;

    ARMCPU *cpu;
    qemu_irq pic[64];

    MemoryRegion *as;       /* Address Space */
    MemoryRegion *ram;      /* SDRAM(DDR) */
    MemoryRegion *rom;      /* On-Chip ROM */
    MemoryRegion *sram;     /* On-Chip SRAM */

    DeviceState *gpio;
} MoxaArtState;

#define MOXA_ART(obj) \
    OBJECT_CHECK(MoxaArtState, obj, TYPE_MOXA_ART)

MoxaArtState *moxa_art_init(const char *cpu, uint64_t ram_size);

#endif  /* HW_ARM_MOXA_ART_H */
